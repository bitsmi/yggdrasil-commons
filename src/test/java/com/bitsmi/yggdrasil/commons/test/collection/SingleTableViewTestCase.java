package com.bitsmi.yggdrasil.commons.test.collection;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowable;

import org.junit.jupiter.api.Test;

import com.bitsmi.yggdrasil.commons.collection.OnMemoryTable;
import com.bitsmi.yggdrasil.commons.collection.QueryableCollectionFactory;
import com.bitsmi.yggdrasil.commons.collection.SingleTableView;
import com.bitsmi.yggdrasil.commons.collection.TableViewRecord;

public class SingleTableViewTestCase 
{
	/*--------------------------------------*
	 * ON MEMORY TABLE VIEW TESTS
	 *--------------------------------------*/
	@Test
	public void onMemoryTableViewCreationTest()
	{
		OnMemoryTable table = QueryableCollectionFactory.newOnMemoryTable()
				.withColumns("ID", "NAME", "VALUE1", "VALUE2")
				.build();
		
		table.addRecord(1L, "Row 1", "MATCH", "No match value")
				.addRecord(2L, "Row 2", "Non match value", "MATCH")
				.addRecord(3L, "Row 3", "MATCH", "MATCH");
		
		SingleTableView<OnMemoryTable, OnMemoryTable.TableRecord> view = QueryableCollectionFactory.newSingleTableView(OnMemoryTable.class)
				.with(table)
				.selectAll()
				.where()
					.isEqualsToValue("VALUE1", "MATCH")
					.and().isEqualsToValue("VALUE2", "MATCH")
				.build();
		
		long recordCount = view.size();
		TableViewRecord<OnMemoryTable.TableRecord> data = view.get(0);
		
		assertThat(recordCount).isEqualTo(1);
		assertThat(data).extracting(record -> record.getValue("ID"),
					record -> record.getValue("NAME"),
					record -> record.getValue("VALUE1"),
					record -> record.getValue("VALUE2"))
				.containsExactly(3L, "Row 3", "MATCH", "MATCH");
	}
	
	@Test
    public void onMemoryTableViewSelectColumnsTest()
    {
        OnMemoryTable table = QueryableCollectionFactory.newOnMemoryTable()
                .withColumns("ID", "NAME", "VALUE1", "VALUE2")
                .build();
        
        table.addRecord(1L, "Row 1", "MATCH", "No match value")
                .addRecord(2L, "Row 2", "Non match value", "MATCH")
                .addRecord(3L, "Row 3", "MATCH", "MATCH");
        
        SingleTableView<OnMemoryTable, OnMemoryTable.TableRecord> view = QueryableCollectionFactory.newSingleTableView(OnMemoryTable.class)
                .with(table)
                .select("ID", "VALUE1")
                .where()
                    .isEqualsToValue("VALUE1", "MATCH")
                    .and().isEqualsToValue("VALUE2", "MATCH")
                .build();
        
        long recordCount = view.size();
        TableViewRecord<OnMemoryTable.TableRecord> data = view.get(0);
        
        assertThat(recordCount).isEqualTo(1);
        assertThat(data).extracting(record -> record.getValue("ID"),
            record -> record.getValue("VALUE1"))
                .containsExactly(3L, "MATCH");
        
        Throwable thrown = catchThrowable(() -> {
            data.getValue("NAME");
        });
        assertThat(thrown).isInstanceOf(IllegalArgumentException.class);
    }
	
	@Test
    public void onMemoryTableViewVersionTest()
    {
        OnMemoryTable table = QueryableCollectionFactory.newOnMemoryTable()
                .withColumns("ID", "NAME", "VALUE1", "VALUE2")
                .build();
        
        table.addRecord(1L, "Row 1", "MATCH", "No match value")
                .addRecord(2L, "Row 2", "Non match value", "MATCH");
        
        SingleTableView<OnMemoryTable, OnMemoryTable.TableRecord> view = QueryableCollectionFactory.newSingleTableView(OnMemoryTable.class)
                .with(table)
                .selectAll()
                .where()
                    .isEqualsToValue("VALUE1", "MATCH")
                    .and().isEqualsToValue("VALUE2", "MATCH")
                .build();
        
        table.addRecord(3L, "Row 3", "MATCH", "MATCH");
        
        long recordCount = view.size();
        TableViewRecord<OnMemoryTable.TableRecord> data = view.get(0);
        long tableVersion = table.getVersion();
        long viewVersion = view.getVersion();
        
        assertThat(recordCount).isEqualTo(1);
        assertThat(data).extracting(record -> record.getValue("ID"),
                    record -> record.getValue("NAME"),
                    record -> record.getValue("VALUE1"),
                    record -> record.getValue("VALUE2"))
                .containsExactly(3L, "Row 3", "MATCH", "MATCH");
        assertThat(tableVersion).isEqualTo(3);
        assertThat(viewVersion).isEqualTo(tableVersion);
    }
}
