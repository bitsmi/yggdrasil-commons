package com.bitsmi.yggdrasil.commons.test.exception;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowable;

import java.util.Optional;
import java.util.concurrent.atomic.AtomicBoolean;

import org.junit.jupiter.api.Test;

import com.bitsmi.yggdrasil.commons.exception.CodedException;
import com.bitsmi.yggdrasil.commons.exception.CodedRuntimeException;
import com.bitsmi.yggdrasil.commons.exception.ExceptionCatcher;
import com.bitsmi.yggdrasil.commons.util.UncheckedFunctionals;

public class ExceptionCatcherTestCase 
{
	@Test
	public void catcherHasExceptionTest()
	{
		ExceptionCatcher<?> catcher = ExceptionCatcher.execute(() -> {
			throw new CodedException("TEST");
		});
		
		assertThat(catcher.hasException()).isTrue();
	}
	
	@Test
	public void catcherHasNoExceptionTest()
	{
		ExceptionCatcher<?> catcher = ExceptionCatcher.execute(() -> {
			// Noop
			return;
		});
		
		assertThat(catcher.hasException()).isFalse();
	}
	
	/*---------------------------*
	 * RETHROW EXCEPTIONS
	 *---------------------------*/
	@Test
	public void catcherNoExceptionRethrowTest()
	{
		Throwable thrown = catchThrowable(() -> {
			ExceptionCatcher.execute(() -> {
				// Noop
				return;
			}).ifExceptionRethrow();
		});
		
		assertThat(thrown).isNull();
	}
	
	@Test
	public void catcherExceptionRethrowTest()
	{
		Throwable thrown = catchThrowable(() -> {
			ExceptionCatcher.execute(() -> {
				throw new CodedException("TEST");
			}).ifExceptionRethrow();
		});
		
		assertThat(thrown).isInstanceOf(CodedException.class);
	}
	
	@Test
	public void catcherSpecificExceptionRethrowTest()
	{
		Throwable thrown = catchThrowable(() -> {
			ExceptionCatcher.execute(() -> {
				throw new CodedException("TEST");
			}).ifExceptionRethrow(CodedException.class);
		});
		
		assertThat(thrown).isInstanceOf(CodedException.class);
	}
	
	@Test
	public void catcherNoSpecificExceptionRethrowTest()
	{
		Throwable thrown = catchThrowable(() -> {
			ExceptionCatcher.execute(() -> {
				throw new IllegalArgumentException();
			}).ifExceptionRethrow(CodedException.class);
		});
		
		assertThat(thrown).isNull();
	}
	
	@Test
	public void catcherMapExceptionRethrowTest()
	{
		Throwable thrown = catchThrowable(() -> {
			ExceptionCatcher.execute(() -> {
				throw new IllegalArgumentException();
			}).ifExceptionRethrow(e -> new CodedException("TEST", e));
		});
		
		assertThat(thrown).isInstanceOf(CodedException.class);
		assertThat(thrown.getCause())
				.isNotNull()
				.isInstanceOf(IllegalArgumentException.class);
	}
	
	@Test
	public void catcherNoMapExceptionRethrowTest()
	{
		Throwable thrown = catchThrowable(() -> {
			ExceptionCatcher.execute(() -> {
				// Noop
				return;
			}).ifExceptionRethrow(e -> new CodedException("TEST", e));
		});
		
		assertThat(thrown).isNull();
	}
	
	@Test
	public void catcherExceptionUncheckedRethrowTest()
	{
		Throwable thrown = catchThrowable(() -> {
			ExceptionCatcher.execute(() -> {
				throw new CodedException("TEST");
			}).ifExceptionRethrowAsUnchecked();
		});
		
		assertThat(thrown).isInstanceOf(CodedException.class);
	}
	
	@Test
	public void catcherExceptionUncheckedRethrow2Test()
	{
		Throwable thrown = catchThrowable(() -> {
			ExceptionCatcher.execute(() -> {
				throw new CodedRuntimeException("TEST");
			}).ifExceptionRethrowAsUnchecked();
		});
		
		assertThat(thrown).isInstanceOf(CodedRuntimeException.class);
		assertThat(thrown.getCause())
				.isNull();
	}
	
	@Test
	public void catcherNoExceptionUncheckedRethrowTest()
	{
		Throwable thrown = catchThrowable(() -> {
			ExceptionCatcher.execute(() -> {
				// Noop
				return;
			}).ifExceptionRethrowAsUnchecked();
		});
		
		assertThat(thrown).isNull();
	}
	
	/*---------------------------*
	 * EXCEPTION CONSUMERS
	 *---------------------------*/
	@Test
	public void catcherConsumerExceptionTest()
	{
		AtomicBoolean result = new AtomicBoolean(true);
		ExceptionCatcher.execute(() -> {
			throw new CodedRuntimeException("TEST");
		}).ifException(e -> {
			result.set(false);
		});
		
		assertThat(result.get()).isFalse();
	}
	
	@Test
	public void catcherConsumerNoExceptionTest()
	{
		AtomicBoolean result = new AtomicBoolean(true);
		ExceptionCatcher.execute(() -> {
			// Noop
			return;
		}).ifException(e -> {
			result.set(false);
		});
		
		assertThat(result.get()).isTrue();
	}
	
	@Test
	public void catcherConsumerSpecificExceptionTest()
	{
		AtomicBoolean result = new AtomicBoolean(true);
		ExceptionCatcher.execute(() -> {
			throw new CodedException("TEST");
		}).ifException(CodedException.class, e -> {
			result.set(false);
		});
		
		assertThat(result.get()).isFalse();
	}
	
	@Test
	public void catcherConsumerSpecificException2Test()
	{
		AtomicBoolean result = new AtomicBoolean(true);
		ExceptionCatcher.execute(() -> {
			throw new CodedRuntimeException("TEST");
		}).ifException(CodedException.class, e -> {
			result.set(false);
		});
		
		assertThat(result.get()).isTrue();
	}
	
	@Test
	public void catcherConsumerNoSpecificExceptionTest()
	{
		AtomicBoolean result = new AtomicBoolean(true);
		ExceptionCatcher.execute(() -> {
			// Noop
			return;
		}).ifException(CodedException.class, e -> {
			result.set(false);
		});
		
		assertThat(result.get()).isTrue();
	}
	
	/*---------------------------*
	 * RESULT SUPPLIER
	 *---------------------------*/
	@Test
	public void catcherSupplierTest()
	{
		Throwable throwable = catchThrowable(() -> {
			Optional<Integer> result = ExceptionCatcher.execute(() -> Integer.valueOf(1))
					.ifExceptionRethrowAsUnchecked()
					.getResult();
			
			assertThat(result).isPresent().contains(1);
		});
		assertThat(throwable).isNull();
	}
	
	@Test
	public void catcherSupplierExceptionTest()
	{
		AtomicBoolean error = new AtomicBoolean(false);
		Optional<Integer> result = ExceptionCatcher.execute((UncheckedFunctionals.ThrowingSupplier<Integer, Exception>)() -> { throw new CodedRuntimeException("TEST"); })
				.ifException(e -> error.set(true))
				.getResult();
		
		assertThat(error).isTrue();
		assertThat(result).isNotPresent();
	}
}
