package com.bitsmi.yggdrasil.commons.test.util;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;
import java.util.function.Consumer;

import org.junit.jupiter.api.Test;

import com.bitsmi.yggdrasil.commons.test.util.support.HierarchyPojo;
import com.bitsmi.yggdrasil.commons.util.RecursiveFunctionals;

public class RecursiveFunctionalsTestCase 
{
	/*------------------------* 
	 * CONSUMER 
	 *------------------------*/
	@Test
	public void consumerRecursivelyTest()
	{
		/* 1 -> 2 -> 4
		 *   -> 3 -> 5 -> 7
		 *        -> 6
		 */
		HierarchyPojo<Integer> root = new HierarchyPojo<Integer>().setValue(Integer.valueOf(1))
				.addChild(new HierarchyPojo<Integer>().setValue(Integer.valueOf(2))
						.addChild(new HierarchyPojo<Integer>().setValue(Integer.valueOf(4)))
				)
				.addChild(new HierarchyPojo<Integer>().setValue(Integer.valueOf(3))
						.addChild(new HierarchyPojo<Integer>().setValue(Integer.valueOf(5))
								.addChild(new HierarchyPojo<Integer>().setValue(Integer.valueOf(7)))
						)
						.addChild(new HierarchyPojo<Integer>().setValue(Integer.valueOf(6)))
				);

		List<Integer> result = new LinkedList<>();
		Consumer<HierarchyPojo<Integer>> consumer = RecursiveFunctionals.<HierarchyPojo<Integer>>consumer(elem -> result.add(elem.getValue()), 
				elem -> elem.getChildren());
		
		consumer.accept(root);
		
		assertThat(result).hasSize(7);
		assertThat(result).contains(1, 2, 3, 4, 5, 6, 7);
	}
	
	@Test
	public void consumerSortTest()
	{
		/* 1 -> 3 -> 6
		 *   -> 2 -> 5 -> 7
		 *        -> 4
		 */
		HierarchyPojo<Integer> root = new HierarchyPojo<Integer>().setValue(Integer.valueOf(1))
				.addChild(new HierarchyPojo<Integer>().setValue(Integer.valueOf(3))
						.addChild(new HierarchyPojo<Integer>().setValue(Integer.valueOf(6)))
				)
				.addChild(new HierarchyPojo<Integer>().setValue(Integer.valueOf(2))
						.addChild(new HierarchyPojo<Integer>().setValue(Integer.valueOf(5))
								.addChild(new HierarchyPojo<Integer>().setValue(Integer.valueOf(7)))
						)
						.addChild(new HierarchyPojo<Integer>().setValue(Integer.valueOf(4)))
				);
		
		Comparator<HierarchyPojo<Integer>> comparator = (o1, o2) -> o1.getValue().compareTo(o2.getValue());
		
		Consumer<HierarchyPojo<Integer>> consumer = RecursiveFunctionals.<HierarchyPojo<Integer>>consumer(elem -> {
					if(elem.getChildren()!=null) {
						elem.getChildren().sort(comparator);
					}
				},
				elem -> elem.getChildren());
		
		consumer.accept(root);
		
		/* 1 -> 2 -> 4
		 *        -> 5 -> 7
		 *   -> 3 -> 6
		 */
		assertThat(root.getChildren()).isSortedAccordingTo(comparator);
		
		/* Node (2) */
		HierarchyPojo<Integer> node2 = root.getChildren().get(0);
		// Nodes (4), (5) ordered
		assertThat(node2.getChildren()).isSortedAccordingTo(comparator);
		
		/* Node (3) */
		HierarchyPojo<Integer> node3 = root.getChildren().get(1);
		assertThat(node3.getValue()).isEqualTo(3);
		// Node (3) only has 1 child (6)
		assertThat(node3.getChildren())
				.singleElement()
				.satisfies(elem -> assertThat(elem.getValue()).isEqualTo(6));
		
		/* Node (5) */
		HierarchyPojo<Integer> node5 = node2.getChildren().get(1);
		assertThat(node5.getValue()).isEqualTo(5);
		// Node (5) only has 1 child (7)
		assertThat(node5.getChildren())
				.singleElement()
				.satisfies(elem -> assertThat(elem.getValue()).isEqualTo(7));
	}
	
	@Test
	public void sortElementHierarchyTest()
	{
		/* 1 -> 3 -> 6
		 *   -> 2 -> 5 -> 7
		 *        -> 4
		 */
		HierarchyPojo<Integer> root = new HierarchyPojo<Integer>().setValue(Integer.valueOf(1))
				.addChild(new HierarchyPojo<Integer>().setValue(Integer.valueOf(3))
						.addChild(new HierarchyPojo<Integer>().setValue(Integer.valueOf(6)))
				)
				.addChild(new HierarchyPojo<Integer>().setValue(Integer.valueOf(2))
						.addChild(new HierarchyPojo<Integer>().setValue(Integer.valueOf(5))
								.addChild(new HierarchyPojo<Integer>().setValue(Integer.valueOf(7)))
						)
						.addChild(new HierarchyPojo<Integer>().setValue(Integer.valueOf(4)))
				);
		
		Comparator<HierarchyPojo<Integer>> comparator = (o1, o2) -> o1.getValue().compareTo(o2.getValue());
		
		RecursiveFunctionals.sortElementHierarchy(root, comparator, elem -> elem.getChildren());
		
		/* 1 -> 2 -> 4
		 *        -> 5 -> 7
		 *   -> 3 -> 6
		 */
		assertThat(root.getChildren()).isSortedAccordingTo(comparator);
		
		/* Node (2) */
		HierarchyPojo<Integer> node2 = root.getChildren().get(0);
		// Nodes (4), (5) ordered
		assertThat(node2.getChildren()).isSortedAccordingTo(comparator);
		
		/* Node (3) */
		HierarchyPojo<Integer> node3 = root.getChildren().get(1);
		assertThat(node3.getValue()).isEqualTo(3);
		// Node (3) only has 1 child (6)
		assertThat(node3.getChildren())
				.singleElement()
				.satisfies(elem -> assertThat(elem.getValue()).isEqualTo(6));
		
		/* Node (5) */
		HierarchyPojo<Integer> node5 = node2.getChildren().get(1);
		assertThat(node5.getValue()).isEqualTo(5);
		// Node (5) only has 1 child (7)
		assertThat(node5.getChildren())
				.singleElement()
				.satisfies(elem -> assertThat(elem.getValue()).isEqualTo(7));
	}
	
	@Test
	public void sortElementListHierarchyTest()
	{
		List<HierarchyPojo<Integer>> collection = new ArrayList<>(2);
		
		/* 1 -> 3 -> 6
		 *   -> 2 -> 5 -> 7
		 *        -> 4
		 */
		collection.add(new HierarchyPojo<Integer>().setValue(Integer.valueOf(1))
				.addChild(new HierarchyPojo<Integer>().setValue(Integer.valueOf(3))
						.addChild(new HierarchyPojo<Integer>().setValue(Integer.valueOf(6)))
				)
				.addChild(new HierarchyPojo<Integer>().setValue(Integer.valueOf(2))
						.addChild(new HierarchyPojo<Integer>().setValue(Integer.valueOf(5))
								.addChild(new HierarchyPojo<Integer>().setValue(Integer.valueOf(7)))
						)
						.addChild(new HierarchyPojo<Integer>().setValue(Integer.valueOf(4)))
				)
		);
		
		/* 11 -> 13 -> 16
		 *    -> 12 -> 15 -> 17
		 *          -> 14
		 */
		collection.add(new HierarchyPojo<Integer>().setValue(Integer.valueOf(11))
				.addChild(new HierarchyPojo<Integer>().setValue(Integer.valueOf(13))
						.addChild(new HierarchyPojo<Integer>().setValue(Integer.valueOf(16)))
				)
				.addChild(new HierarchyPojo<Integer>().setValue(Integer.valueOf(12))
						.addChild(new HierarchyPojo<Integer>().setValue(Integer.valueOf(15))
								.addChild(new HierarchyPojo<Integer>().setValue(Integer.valueOf(17)))
						)
						.addChild(new HierarchyPojo<Integer>().setValue(Integer.valueOf(14)))
				)
		);
		
		Comparator<HierarchyPojo<Integer>> comparator = (o1, o2) -> o1.getValue().compareTo(o2.getValue());
		
		RecursiveFunctionals.sortElementListHierarchy(collection, comparator, elem -> elem.getChildren());
		
		HierarchyPojo<Integer> root1 = collection.get(0);
		HierarchyPojo<Integer> root2 = collection.get(1);
		
		/* 1 -> 2 -> 4
		 *        -> 5 -> 7
		 *   -> 3 -> 6
		 */
		assertThat(root1.getChildren()).isSortedAccordingTo(comparator);

		/* Node (2) */
		HierarchyPojo<Integer> node2 = collection.get(0).getChildren().get(0);
		// Nodes (4), (5) ordered
		assertThat(node2.getChildren()).isSortedAccordingTo(comparator);
		
		/* Node (3) */
		HierarchyPojo<Integer> node3 = root1.getChildren().get(1);
		assertThat(node3.getValue()).isEqualTo(3);
		// Node (3) only has 1 child (6)
		assertThat(node3.getChildren())
				.singleElement()
				.satisfies(elem -> assertThat(elem.getValue()).isEqualTo(6));
		
		/* Node (5) */
		HierarchyPojo<Integer> node5 = node2.getChildren().get(1);
		assertThat(node5.getValue()).isEqualTo(5);
		// Node (5) only has 1 child (7)
		assertThat(node5.getChildren())
				.singleElement()
				.satisfies(elem -> assertThat(elem.getValue()).isEqualTo(7));
		
		/* 11 -> 12 -> 14
		 *          -> 15 -> 17
		 *    -> 13 -> 16
		 */
		assertThat(root2.getChildren()).isSortedAccordingTo(comparator);

		/* Node (12) */
		HierarchyPojo<Integer> node12 = root2.getChildren().get(0);
		// Nodes (14), (15) ordered
		assertThat(node12.getChildren()).isSortedAccordingTo(comparator);
		
		/* Node (13) */
		HierarchyPojo<Integer> node13 = root2.getChildren().get(1);
		assertThat(node13.getValue()).isEqualTo(13);
		// Node (13) only has 1 child (16)
		assertThat(node13.getChildren())
				.singleElement()
				.satisfies(elem -> assertThat(elem.getValue()).isEqualTo(16));
		
		/* Node (15) */
		HierarchyPojo<Integer> node15 = node12.getChildren().get(1);
		assertThat(node15.getValue()).isEqualTo(15);
		// Node (15) only has 1 child (17)
		assertThat(node15.getChildren())
				.singleElement()
				.satisfies(elem -> assertThat(elem.getValue()).isEqualTo(17));
	}
}
