package com.bitsmi.yggdrasil.commons.test.util;

import static com.bitsmi.yggdrasil.commons.util.Expressions.DIGIT;
import static com.bitsmi.yggdrasil.commons.util.Expressions.END;
import static com.bitsmi.yggdrasil.commons.util.Expressions.WHITESPACE;
import static com.bitsmi.yggdrasil.commons.util.Expressions.capture;
import static com.bitsmi.yggdrasil.commons.util.Expressions.charsBetween;
import static com.bitsmi.yggdrasil.commons.util.Expressions.charsIn;
import static com.bitsmi.yggdrasil.commons.util.Expressions.exactly;
import static com.bitsmi.yggdrasil.commons.util.Expressions.greedy;
import static com.bitsmi.yggdrasil.commons.util.Expressions.lookahead;
import static com.bitsmi.yggdrasil.commons.util.Expressions.named;
import static com.bitsmi.yggdrasil.commons.util.Expressions.not;
import static com.bitsmi.yggdrasil.commons.util.Expressions.optional;
import static com.bitsmi.yggdrasil.commons.util.Expressions.sequence;
import static com.bitsmi.yggdrasil.commons.util.Expressions.singleChar;
import static com.bitsmi.yggdrasil.commons.util.Expressions.singleCharBetween;
import static com.bitsmi.yggdrasil.commons.util.Expressions.singleCharIn;
import static org.assertj.core.api.Assertions.assertThat;

import java.util.HashMap;
import java.util.Map;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import com.bitsmi.yggdrasil.commons.util.ExpressionParser;
import com.bitsmi.yggdrasil.commons.util.ExpressionParser.ParseResult;
import com.bitsmi.yggdrasil.commons.util.Expressions;
import com.bitsmi.yggdrasil.commons.util.Expressions.Expression;

public class ExpressionParserTestCase 
{
    @Test
    @DisplayName("Exact string expression incomplete Test")
    public void plainExpressionsTest1()
    {
        ExpressionParser parser = ExpressionParser.builder()
            .withExpressions(Expressions.sequence(
                exactly("ABC"),
                END))
            .build();
        ParseResult result = parser.parse("ABCD");
        
        assertThat(result.isInputFullyMatched()).isFalse();
        assertThat(result.getMatchedParsedExpressions()).hasSize(0);
    }
    
    @Test
    @DisplayName("Characters in expression no match Test")
    public void plainExpressionsTest2()
    {
        ExpressionParser parser = ExpressionParser.builder()
            .withExpressions(Expressions.sequence(
                    charsBetween('1', '6'),
                    END))
            .build();
        ParseResult result = parser.parse("123456A");
        
        assertThat(result.getMatchedParsedExpressions()).hasSize(0);
        assertThat(result.isInputFullyMatched()).isFalse();
    }
    
    @Test
    @DisplayName("Sequence composed operation Test")
    public void sequenceExpressionsTest1()
    {
        ExpressionParser parser = ExpressionParser.builder()
            .withExpressions(Expressions.sequence(
                singleChar('A'),
                singleChar('1')))
            .build();
        ParseResult result = parser.parse("A1");
        
        assertThat(result.isInputFullyMatched()).isTrue();
        assertThat(result.getMatchedParsedExpressions())
                .hasSize(1)
                .element(0)
                    .satisfies(exprResult -> {
                        assertThat(exprResult.getMatchingValue()).isEqualTo("A1");
                        assertThat(exprResult.getEndPosition()).isEqualTo(2);
                    });
    }
    
    @Test
    @DisplayName("Choice composed operation Test")
    public void choiceExpressionsTest1()
    {
        ExpressionParser parser = ExpressionParser.builder()
            .withExpressions(Expressions.choice(
                singleChar('A'),
                singleChar('1')))
            .build();
        ParseResult result = parser.parse("1");

        assertThat(result.isInputFullyMatched()).isTrue();
        assertThat(result.getMatchedParsedExpressions())
                .hasSize(1)
                .element(0)
                    .satisfies(exprResult -> {
                        assertThat(exprResult.getMatchingValue()).isEqualTo("1");
                        assertThat(exprResult.getEndPosition()).isEqualTo(1);
                    });
    }
    
    @Test
    @DisplayName("Not operation single character Test")
    public void wrappedExpressionTest1()
    {
        ExpressionParser parser = ExpressionParser.builder()
            .withExpressions(Expressions.not(singleCharIn('1', '2', '3', '4', '5', '6')))
            .build();
        ParseResult result = parser.parse("A");
        
        assertThat(result.isInputFullyMatched()).isTrue();
        assertThat(result.getMatchedParsedExpressions())
                .hasSize(1)
                .element(0)
                    .satisfies(exprResult -> {
                        assertThat(exprResult.getMatchingValue()).isEqualTo("A");
                        assertThat(exprResult.getEndPosition()).isEqualTo(1);
                    });
    }
    
    @Test
    @DisplayName("Capture operation Test")
    public void wrappedExpressionTest2()
    {
        ExpressionParser parser = ExpressionParser.builder()
            .withExpressions(capture("TEST", charsIn('1', '2', '3', '4', '5', '6')))
            .build();
        ParseResult result = parser.parse("1");
        
        assertThat(result.isInputFullyMatched()).isTrue();
        assertThat(result.getMatchedParsedExpressions())
                .hasSize(1)
                .element(0)
                    .satisfies(exprResult -> {
                        assertThat(exprResult.getMatchingValue()).isEqualTo("1");
                        assertThat(exprResult.getEndPosition()).isEqualTo(1);
                    });
        assertThat(result.getCapturedValues("TEST")).containsExactlyInAnyOrder("1");
    }
    
    @Test
    @DisplayName("Capture operation multiple values Test")
    public void wrappedExpressionTest3()
    {
        ExpressionParser parser = ExpressionParser.builder()
            .withExpressions(greedy(capture("TEST", singleCharBetween('1', '6'))))
            .build();
        ParseResult result = parser.parse("12");
        
        assertThat(result.isInputFullyMatched()).isTrue();
        assertThat(result.getMatchedParsedExpressions())
                .hasSize(1)
                .element(0)
                    .satisfies(exprResult -> {
                        assertThat(exprResult.getMatchingValue()).isEqualTo("12");
                        assertThat(exprResult.getEndPosition()).isEqualTo(2);
                    });
        assertThat(result.getCapturedValues("TEST")).containsExactlyInAnyOrder("1", "2");
    }
    
    @Test
    @DisplayName("Named operation Test")
    public void wrappedExpressionTest4()
    {
        ExpressionParser parser = ExpressionParser.builder()
            .withExpressions(named("TEST", charsIn('1', '2', '3', '4', '5', '6')))
            .build();
        ParseResult result = parser.parse("1");
        
        assertThat(result.isInputFullyMatched()).isTrue();
        assertThat(result.getMatchedParsedExpressions())
                .hasSize(1)
                .element(0)
                    .satisfies(exprResult -> {
                        assertThat(exprResult.getMatchingValue()).isEqualTo("1");
                        assertThat(exprResult.getEndPosition()).isEqualTo(1);
                        assertThat(exprResult.getExpression().getName()).isEqualTo("TEST");
                    });
    }
    
    @Test
    @DisplayName("Optional operation Test")
    public void wrappedExpressionTest5()
    {
        ExpressionParser parser = ExpressionParser.builder()
            .withExpressions(sequence(
                optional(singleChar('1')),
                singleChar('A')
            ))
            .build();
        ParseResult result1 = parser.parse("1A");
        ParseResult result2 = parser.parse("A");
        
        assertThat(result1.isInputFullyMatched()).isTrue();
        assertThat(result1.getMatchedParsedExpressions())
                .hasSize(1)
                .element(0)
                .satisfies(exprResult -> {
                    assertThat(exprResult.getMatchingValue()).isEqualTo("1A");
                    assertThat(exprResult.getEndPosition()).isEqualTo(2);
                });
        assertThat(result2.isInputFullyMatched()).isTrue();
        assertThat(result2.getMatchedParsedExpressions())
                .hasSize(1)
                .element(0)
                    .satisfies(exprResult -> {
                        assertThat(exprResult.getMatchingValue()).isEqualTo("A");
                        assertThat(exprResult.getEndPosition()).isEqualTo(1);
                    });
    }
    
    @Test
    @DisplayName("Greedy expression Test")
    public void greedyExpressionTest1()
    {
        ExpressionParser parser = ExpressionParser.builder()
            .withExpressions(greedy(
                exactly("AB")
            ))
            .build();
        ParseResult result = parser.parse("ABABABAB");
        
        assertThat(result.isInputFullyMatched()).isTrue();
        assertThat(result.getMatchedParsedExpressions())
                .hasSize(1)
                .element(0)
                    .satisfies(exprResult -> {
                        assertThat(exprResult.getMatchingValue()).isEqualTo("ABABABAB");
                        assertThat(exprResult.getEndPosition()).isEqualTo(8);
                    });
    }
    
    @Test
    @DisplayName("Lookahead expression Test")
    public void lookaheadExpressionTest1()
    {
        ExpressionParser parser = ExpressionParser.builder()
            .withExpressions(greedy(
                sequence(
                    exactly("AB"),
                    lookahead(exactly("AB"))
                )
            ))
            .build();
        ParseResult result = parser.parse("ABABABAB");
        
        assertThat(result.isInputFullyMatched()).isFalse();
        assertThat(result.getMatchedParsedExpressions())
                .hasSize(1)
                .element(0)
                    .satisfies(exprResult -> {
                        // The last "AB" pair doesn't match lookahead expression
                        assertThat(exprResult.getMatchingValue()).isEqualTo("ABABAB");
                        assertThat(exprResult.getEndPosition()).isEqualTo(6);
                    });
    }
    
    @Test
    @DisplayName("Negative lookahead expression Test")
    public void lookaheadExpressionTest2()
    {
        ExpressionParser parser = ExpressionParser.builder()
            .withExpressions(greedy(
                sequence(
                    exactly("AB"),
                    lookahead(not(exactly("AB")))
                )
            ))
            .build();
        ParseResult result = parser.parse("ABCDABAB");
        
        assertThat(result.isInputFullyMatched()).isFalse();
        assertThat(result.getMatchedParsedExpressions())
                .hasSize(1)
                .element(0)
                    .satisfies(exprResult -> {
                        assertThat(exprResult.getMatchingValue()).isEqualTo("AB");
                        assertThat(exprResult.getEndPosition()).isEqualTo(2);
                    });
    }
    
    @Test
    @DisplayName("Multiple expression match Test")
    public void mutipleExpressionTest1()
    {
        Expression expr1 = greedy(
            exactly("AB")
        );
        Expression expr2 = greedy(
            exactly("CD")
        );
        
        ExpressionParser parser = ExpressionParser.builder()
            .withExpressions(expr1, expr2)
            .build();
        ParseResult result = parser.parse("ABABABCDCDCD");
        
        assertThat(result.isInputFullyMatched()).isTrue();
        assertThat(result.getMatchedParsedExpressions())
                .hasSize(2)
                .satisfies(expressions -> {
                    assertThat(expressions.get(0))
                            .satisfies(exprResult -> {
                                assertThat(exprResult.getMatchingValue()).isEqualTo("ABABAB");
                                assertThat(exprResult.getEndPosition()).isEqualTo(6);
                            });
                    assertThat(expressions.get(1))
                            .satisfies(exprResult -> {
                                assertThat(exprResult.getMatchingValue()).isEqualTo("CDCDCD");
                                assertThat(exprResult.getEndPosition()).isEqualTo(12);
                            });
                });
    }
    
    @Test
    @DisplayName("Multiple expression not match Test")
    public void mutipleExpressionTest2()
    {
        Expression expr1 = greedy(
            exactly("AB")
        );
        Expression expr2 = greedy(
            exactly("CD")
        );
        
        ExpressionParser parser = ExpressionParser.builder()
            .withExpressions(expr1, expr2)
            .build();
        ParseResult result = parser.parse("WXWXWXYZYZYZ");
        
        assertThat(result.isInputFullyMatched()).isFalse();
        assertThat(result.getMatchedParsedExpressions())
                .hasSize(0);
    }
    
    @Test
    @DisplayName("Multiple expression partial match Test")
    public void mutipleExpressionTest3()
    {
        Expression expr1 = greedy(
            exactly("AB")
        );
        Expression expr2 = greedy(
            exactly("WX")
        );
        
        ExpressionParser parser = ExpressionParser.builder()
            .withExpressions(expr1, expr2)
            .build();
        ParseResult result = parser.parse("WXWXWXYZYZYZ");
        
        assertThat(result.isInputFullyMatched()).isFalse();
        assertThat(result.getMatchedParsedExpressions())
                .hasSize(1)
                .element(0)
                    .satisfies(exprResult -> {
                        assertThat(exprResult.getMatchingValue()).isEqualTo("WXWXWX");
                        assertThat(exprResult.getEndPosition()).isEqualTo(6);
                    });
    }
    
    @Test
    @DisplayName("Parsed expression visitor test")
    public void parsedExpressionVisitorTest1()
    {
        Expression expression = sequence(
            named("OPERAND", greedy(DIGIT)),
            optional(greedy(WHITESPACE)),
            named("OPERATOR", singleCharIn('+', '-', '*', '/')),
            optional(greedy(WHITESPACE)),
            named("OPERAND", greedy(DIGIT))
        );
        
        ExpressionParser parser = ExpressionParser.builder()
            .withExpressions(expression)
            .build();
        ParseResult result = parser.parse("100 + 200");
        
        Map<String, String> values = new HashMap<>();
        result.visitParsedExpressions((path, parsedExpr) -> {
            if(path.matches("/.+/OPERAND\\[0\\]")) {
            	values.put("OPERAND1", parsedExpr.getMatchingValue());
            }
            else if(path.matches("/.+/OPERATOR")) {
            	values.put("OPERATOR", parsedExpr.getMatchingValue());
            }
            else if(path.matches("/.+/OPERAND\\[1\\]")) {
            	values.put("OPERAND2", parsedExpr.getMatchingValue());
            }
            
            return true;
        });

        assertThat(values).containsOnly(Assertions.entry("OPERAND1", "100"),
        		Assertions.entry("OPERATOR", "+"),
        		Assertions.entry("OPERAND2", "200"));
    }
    
    @Test
    @DisplayName("Parsed expression visitor partial walk test")
    public void parsedExpressionVisitorTest2()
    {
        Expression expression = sequence(
            named("OPERAND", greedy(DIGIT)),
            optional(greedy(WHITESPACE)),
            named("OPERATOR", singleCharIn('+', '-', '*', '/')),
            optional(greedy(WHITESPACE)),
            named("OPERAND", greedy(DIGIT))
        );
        
        ExpressionParser parser = ExpressionParser.builder()
            .withExpressions(expression)
            .build();
        ParseResult result = parser.parse("100 + 200");
        
        Map<String, String> visitedValues = new HashMap<>();
        result.visitParsedExpressions((path, parsedExpr) -> {
        	boolean follow = true;
            if(path.matches("/.+/OPERAND\\[0\\]")) {
            	visitedValues.put("OPERAND1", parsedExpr.getMatchingValue());
            }
            else if(path.matches("/.+/OPERATOR")) {
            	visitedValues.put("OPERATOR", parsedExpr.getMatchingValue());
            	follow = false;
            }
            else if(path.matches("/.+/OPERAND\\[1\\]")) {
            	visitedValues.put("OPERAND2", parsedExpr.getMatchingValue());
            }
            
            return follow;
        });

        assertThat(visitedValues).containsOnly(Assertions.entry("OPERAND1", "100"),
        		Assertions.entry("OPERATOR", "+"));
    }
    
	@Test
	@DisplayName("Parse range expression OK test")
	public void parseRangeExpressionTest1()
	{
		Expression NUMBER = Expressions.sequence(
				optional(singleChar('-')),
				greedy(DIGIT));
		
		// [-10, 100)
		Expression genericExpression = Expressions.named("GENERIC_EXPR", 
				sequence(
					named("LOWER_BOUND", capture("LOWER_BOUND", singleCharIn('[', '('))),
					optional(greedy(WHITESPACE)),
					named("LOWER_VALUE", capture("LOWER_VALUE", greedy(NUMBER))),
					optional(greedy(WHITESPACE)),
					singleChar(','),
					optional(greedy(WHITESPACE)),
					named("UPPER_VALUE", capture("UPPER_VALUE", greedy(NUMBER))),
					optional(greedy(WHITESPACE)),
					named("UPPER_BOUND", capture("UPPER_BOUND", singleCharIn(']', ')')))
				)
		);
		// -10..100
		Expression simplifiedExpression = Expressions.named("SIMPLIFIED_EXPR", 
				sequence(
					capture("LOWER_VALUE", greedy(NUMBER)),
					optional(greedy(WHITESPACE)),
					exactly(".."),
					optional(greedy(WHITESPACE)),
					capture("UPPER_VALUE", greedy(NUMBER))
				)
		);
		// Operation
		Expression operandExpression = Expressions.choice(
				genericExpression, 
				simplifiedExpression, 
				named("NUMBER_OP", NUMBER));
		Expression operationExpression = Expressions.named("OPERATION_EXPR",
				sequence(
					capture("OPERAND_1", operandExpression),
					optional(greedy(WHITESPACE)),
					// Union, Intersection
					capture("OPERATOR", singleCharIn('\u222A', '\u2229')),
					optional(greedy(WHITESPACE)),
					capture("OPERAND_2", operandExpression)
				)
		);
	
		ExpressionParser parser = ExpressionParser.builder()
				.withExpressions(operationExpression,
				    genericExpression,  
				    simplifiedExpression,
				    named("NUMBER_EXPR", NUMBER)
				)
				.build();
		
		ParseResult result = parser.parse("[-10, 100)");
		Map<String, String> visitedValues = new HashMap<>();
        result.visitParsedExpressions((path, parsedExpr) -> {
            if(path.matches("/.+/LOWER_BOUND")) {
            	visitedValues.put("LOWER_BOUND", parsedExpr.getMatchingValue());
            }
            else if(path.matches("/.+/LOWER_VALUE")) {
            	visitedValues.put("LOWER_VALUE", parsedExpr.getMatchingValue());
            }
            else if(path.matches("/.+/UPPER_VALUE")) {
            	visitedValues.put("UPPER_VALUE", parsedExpr.getMatchingValue());
            }
            else if(path.matches("/.+/UPPER_BOUND")) {
            	visitedValues.put("UPPER_BOUND", parsedExpr.getMatchingValue());
            }
            
            return true;
        });

		assertThat(result.isInputFullyMatched()).isTrue();
		assertThat(result.getParsedExpressions("GENERIC_EXPR"))
                .hasSize(1)
                .element(0)
                    .satisfies(exprResult -> {
                        assertThat(exprResult.getExpression().getName()).isEqualTo("GENERIC_EXPR");
                        assertThat(exprResult.getMatchingValue()).isEqualTo("[-10, 100)");
                    });
		assertThat(result.getCapturedValues("LOWER_BOUND"))
		        .hasSize(1)
		        .element(0)
		        .isEqualTo("[");
		assertThat(result.getCapturedValues("LOWER_VALUE"))
                .hasSize(1)
                .element(0)
                .isEqualTo("-10");
		assertThat(result.getCapturedValues("UPPER_VALUE"))
                .hasSize(1)
                .element(0)
                .isEqualTo("100");
		assertThat(result.getCapturedValues("UPPER_BOUND"))
                .hasSize(1)
                .element(0)
                .isEqualTo(")");	
		
		assertThat(visitedValues).containsOnly(Assertions.entry("LOWER_BOUND", "["),
        		Assertions.entry("LOWER_VALUE", "-10"),
        		Assertions.entry("UPPER_VALUE", "100"),
        		Assertions.entry("UPPER_BOUND", ")"));
	}
}
