package com.bitsmi.yggdrasil.commons.test.util;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;

import com.bitsmi.yggdrasil.commons.util.IProgressTrackerListener;
import com.bitsmi.yggdrasil.commons.util.IProgressTrackerTask;
import com.bitsmi.yggdrasil.commons.util.ProgressTracker;

public class ProgressTrackerTestCase 
{
	@Test
	public void simpleTasksTest()
	{
		ProgressTracker tracker = ProgressTracker.newTracker();
		
		// Task1 -> 20%; Task2 -> 80%
		List<? extends IProgressTrackerTask> tasks = tracker.split(20, 80);
		// Task 1 (100%)
		tracker.getCurrentSubtask().complete();
		
		Assert.assertEquals(20, tracker.computeProgress());
		
		// Task 2 (50%)
		tracker.getCurrentSubtask().advance(50);
		
		Assert.assertEquals(60, tracker.computeProgress());
		Assert.assertEquals(100, tasks.get(0).computeProgress());
		Assert.assertEquals(50, tasks.get(1).computeProgress());
	}
	
	@Test
	public void trackerOverflowTest()
	{
		ProgressTracker tracker = ProgressTracker.newTracker();
		
		// Task1 -> 20%; Task2 -> 80%
		List<? extends IProgressTrackerTask> tasks = tracker.split(20, 80);
		
		tracker.advance(60);
		
		// Task1 (100%) + Task2 (50%) => 60%
		Assert.assertEquals(60, tracker.computeProgress());
		Assert.assertEquals(100, tasks.get(0).computeProgress());
		Assert.assertEquals(50, tasks.get(1).computeProgress());
	}
	
	@Test
	public void implicitSplitTest()
	{
		ProgressTracker tracker = ProgressTracker.newTracker();
		
		// Task1 -> 20%; Task2 -> 80%
		List<? extends IProgressTrackerTask> tasks = tracker.split(20);
		
		tracker.advance(60);
		
		// Task1 (100%) + Task2 (50%) => 60%
		Assert.assertEquals(60, tracker.computeProgress());
		Assert.assertEquals(100, tasks.get(0).computeProgress());
		Assert.assertEquals(50, tasks.get(1).computeProgress());
	}
	
	@Test
	public void fullSplitTest()
	{
		ProgressTracker tracker = ProgressTracker.newTracker();
		
		// Task1 -> 100%
		tracker.split(100);
		// Task1 (complete)
		tracker.getCurrentSubtask().complete();
		
		Assert.assertEquals(1, tracker.getSubtaskCount());
		Assert.assertEquals(100, tracker.computeProgress());
		Assert.assertEquals(true, tracker.isComplete());
	}
	
	@Test
	public void completeSubtasksTest()
	{
		ProgressTracker tracker = ProgressTracker.newTracker();
		
		// Task1 -> 20%; Task2 -> 80%
		List<? extends IProgressTrackerTask> tasks = tracker.split(20, 80);
		
		tasks.get(0).complete();
		tasks.get(1).complete();
		
		// Task1 (100%) + Task2 (100%) => 100%
		Assert.assertEquals(100, tracker.computeProgress());
		Assert.assertEquals(100, tasks.get(0).computeProgress());
		Assert.assertEquals(100, tasks.get(1).computeProgress());
		Assert.assertEquals(true, tasks.get(0).isComplete());
		Assert.assertEquals(true, tasks.get(1).isComplete());
	}
	
	@Test
	public void subtaskTreeTest()
	{
		ProgressTracker tracker = ProgressTracker.newTracker();
		
		// Task1 -> 20%; Task2 -> 80%
		List<? extends IProgressTrackerTask> tasks = tracker.split(20, 80);
		List<? extends IProgressTrackerTask> t2Subtasks = tasks.get(1).split(20, 30, 50);
		
		tracker.getCurrentSubtask().complete();
		t2Subtasks.get(0).complete();
		t2Subtasks.get(1).advance(50);
		
		// Task1 (100%) + Task2 (35%) => 48%
		Assert.assertEquals(48, tracker.computeProgress());
		Assert.assertEquals(100, tasks.get(0).computeProgress());
		Assert.assertEquals(35, tasks.get(1).computeProgress());
	}
	
	@Test(expected = IllegalStateException.class)
	public void alreadyUsedTaskTest()
	{
		ProgressTracker tracker = ProgressTracker.newTracker();
		
		// Task1 -> 20%; Task2 -> 80%
		List<? extends IProgressTrackerTask> tasks = tracker.split(20, 80);
		
		// Complete Task2
		tasks.get(1).complete();
		// Task1 Overflow -> ERROR
		tracker.advance(50);
	}
	
	/* LISTENER */
	@Test
	public void trackerSimpleListenerTest()
	{
		// Create as array to make it editable from inside lambda expressions
		final int[] actualNotifications = new int[] {0};		
		
		ProgressTracker tracker = ProgressTracker.newTracker()
				.addProgressListener((task, info) -> {
					int currentValue = actualNotifications[0]; 
					actualNotifications[0] = currentValue + 1;
				});
		
		// Task1 -> 20%; Task2 -> 80%
		List<? extends IProgressTrackerTask> tasks = tracker.split(20, 80);
		// Task1 (completa)
		tasks.get(0).complete();
		// Task 2 (al 50%)
		tasks.get(1).advance(50);
		
		Assert.assertEquals(60, tracker.computeProgress());
		Assert.assertEquals(2, actualNotifications[0]);
	}
	
	@Test
	public void trackerChainedListenersTest()
	{
		// Create as array to make it editable from inside lambda expressions
		final int[] actualNotifications1 = new int[] {0};		
		final int[] actualNotifications2 = new int[] {0};
		
		ProgressTracker tracker = ProgressTracker.newTracker()
				.addProgressListener((task, info) -> {
					int currentValue = actualNotifications1[0]; 
					actualNotifications1[0] = currentValue + 1;
				})
				.addProgressListener((task, info) -> {
					int currentValue = actualNotifications2[0]; 
					actualNotifications2[0] = currentValue + 1;
				});
		
		// Task1 -> 20%; Task2 -> 80%
		List<? extends IProgressTrackerTask> tasks = tracker.split(20, 80);
		// Task1 (completa)
		tasks.get(0).complete();
		// Task 2 (al 50%)
		tasks.get(1).advance(50);
		
		Assert.assertEquals(60, tracker.computeProgress());
		Assert.assertEquals(2, actualNotifications1[0]);
		Assert.assertEquals(2, actualNotifications2[0]);
	}
	
	@Test
	public void subtaskListenerAdvanceTest()
	{
		// Create as array to make it editable from inside lambda expressions
		final int[] actualNotificationsMain = new int[] {0};		
		final int[] actualNotificationsSubtask = new int[] {0};
		
		ProgressTracker tracker = ProgressTracker.newTracker()
				.addProgressListener((task, info) -> {
					int currentValue = actualNotificationsMain[0]; 
					actualNotificationsMain[0] = currentValue + 1;
				});
		
		// Task1 -> 20%; Task2 -> 80%
		List<? extends IProgressTrackerTask> tasks = tracker.split(20, 80);
		tasks.get(1)
			.addProgressListener((task, info) -> {
				int currentValue = actualNotificationsSubtask[0]; 
				actualNotificationsSubtask[0] = currentValue + 1;
			});
		
		// Task1 (completa)
		tasks.get(0).complete();
		// Task 2 (al 50%)
		tasks.get(1).advance(50);
		
		Assert.assertEquals(60, tracker.computeProgress());
		Assert.assertEquals(2, actualNotificationsMain[0]);
		Assert.assertEquals(1, actualNotificationsSubtask[0]);
	}
	
	@Test
	public void subtaskListenerCompleteTest()
	{
		// Create as array to make it editable from inside lambda expressions
		final int[] actualNotificationsMain = new int[] {0};	
		final int[] actualNotificationsSubtask1 = new int[] {0};
		final int[] actualNotificationsSubtask2 = new int[] {0};
		
		ProgressTracker tracker = ProgressTracker.newTracker()
				.addProgressListener((task, info) -> {
					int currentValue = actualNotificationsMain[0]; 
					actualNotificationsMain[0] = currentValue + 1;
				});
		
		// Task1 -> 20%; Task2 -> 80%
		List<? extends IProgressTrackerTask> tasks = tracker.split(20, 80);
		tasks.get(1)
			.addProgressListener((task, info) -> {
				int currentValue = actualNotificationsSubtask2[0]; 
				actualNotificationsSubtask1[0] = currentValue + 1;
			});
		tasks.get(1)
			.addProgressListener((task, info) -> {
				int currentValue = actualNotificationsSubtask2[0]; 
				actualNotificationsSubtask2[0] = currentValue + 1;
			});
		
		// Task1 (complete)
		tasks.get(0).complete();
		// Task 2 (complete)
		tasks.get(1).complete();
		
		Assert.assertEquals(100, tracker.computeProgress());
		Assert.assertEquals(2, actualNotificationsMain[0]);
		Assert.assertEquals(1, actualNotificationsSubtask1[0]);
		Assert.assertEquals(1, actualNotificationsSubtask2[0]);
	}
	
	/* LISTENER WITH INFO */
	@Test
	public void trackerSimpleListenerWithInfoTest()
	{
		// Create as array to make it editable from inside lambda expressions
		final int[] actualNotifications = new int[] {0};		
		final List<IProgressTrackerListener.ProgressInfo> actualInfo = new ArrayList<>();
		
		ProgressTracker tracker = ProgressTracker.newTracker()
				.addProgressListener((task, info) -> {
					int currentValue = actualNotifications[0]; 
					actualNotifications[0] = currentValue + 1;
					actualInfo.add(info);
				});
		
		// Task1 -> 20%; Task2 -> 80%
		List<? extends IProgressTrackerTask> tasks = tracker.split(20, 80);
		// Task1 (completa)
		tasks.get(0).complete(new IProgressTrackerListener.ProgressInfo()
				.setMessage("INFO")
				.putAttribute("PARAM", 1));
		// Task 2 (al 50%)
		tasks.get(1).advance(50, new IProgressTrackerListener.ProgressInfo()
				.setMessage("INFO")
				.putAttribute("PARAM", 2));
		
		Assert.assertEquals(60, tracker.computeProgress());
		Assert.assertEquals(2, actualNotifications[0]);
		Assert.assertEquals(2, actualInfo.size());
		// Expected values
		int[] paramMain = new int[] {1, 2};
		for(int i=0; i<actualInfo.size(); i++) {
			IProgressTrackerListener.ProgressInfo info = actualInfo.get(i);
			Assert.assertEquals("INFO", info.getMessage());
			Assert.assertEquals(1, info.getAttributes().size());
			Assert.assertEquals(paramMain[i], info.getAttributes().get("PARAM"));
		}
	}
	
	@Test
	public void trackerChainedListenersWithInfoTest()
	{
		// Create as array to make it editable from inside lambda expressions
		final int[] actualNotifications1 = new int[] {0};		
		final int[] actualNotifications2 = new int[] {0};
		final List<IProgressTrackerListener.ProgressInfo> actualInfo1 = new ArrayList<>();
		final List<IProgressTrackerListener.ProgressInfo> actualInfo2 = new ArrayList<>();
		
		ProgressTracker tracker = ProgressTracker.newTracker()
				.addProgressListener((task, info) -> {
					int currentValue = actualNotifications1[0]; 
					actualNotifications1[0] = currentValue + 1;
					actualInfo1.add(info);
				})
				.addProgressListener((task, info) -> {
					int currentValue = actualNotifications2[0]; 
					actualNotifications2[0] = currentValue + 1;
					actualInfo2.add(info);
				});
		
		// Task1 -> 20%; Task2 -> 80%
		List<? extends IProgressTrackerTask> tasks = tracker.split(20, 80);
		// Task1 (completa)
		tasks.get(0).complete(new IProgressTrackerListener.ProgressInfo()
				.setMessage("INFO")
				.putAttribute("PARAM", 1));
		// Task 2 (al 50%)
		tasks.get(1).advance(50, new IProgressTrackerListener.ProgressInfo()
				.setMessage("INFO")
				.putAttribute("PARAM", 2));
		
		Assert.assertEquals(60, tracker.computeProgress());
		Assert.assertEquals(2, actualNotifications1[0]);
		Assert.assertEquals(2, actualNotifications2[0]);
		// Expected values
		int[] param1 = new int[] {1, 2};
		for(int i=0; i<actualInfo1.size(); i++) {
			IProgressTrackerListener.ProgressInfo info = actualInfo1.get(i);
			Assert.assertEquals("INFO", info.getMessage());
			Assert.assertEquals(1, info.getAttributes().size());
			Assert.assertEquals(param1[i], info.getAttributes().get("PARAM"));
		}
		// Expected values
		int[] param2 = new int[] {1, 2};
		for(int i=0; i<actualInfo2.size(); i++) {
			IProgressTrackerListener.ProgressInfo info = actualInfo2.get(i);
			Assert.assertEquals("INFO", info.getMessage());
			Assert.assertEquals(1, info.getAttributes().size());
			Assert.assertEquals(param2[i], info.getAttributes().get("PARAM"));
		}
	}
	
	@Test
	public void subtaskListenerAdvanceWithInfoTest()
	{
		// Create as array to make it editable from inside lambda expressions
		final int[] actualNotificationsMain = new int[] {0};		
		final int[] actualNotificationsSubtask = new int[] {0};
		final List<IProgressTrackerListener.ProgressInfo> actualInfoMain = new ArrayList<>();
		final List<IProgressTrackerListener.ProgressInfo> actualInfoSubtask = new ArrayList<>();
		
		ProgressTracker tracker = ProgressTracker.newTracker()
				.addProgressListener((task, info) -> {
					int currentValue = actualNotificationsMain[0]; 
					actualNotificationsMain[0] = currentValue + 1;
					actualInfoMain.add(info);
				});
		
		// Task1 -> 20%; Task2 -> 80%
		List<? extends IProgressTrackerTask> tasks = tracker.split(20, 80);
		tasks.get(1)
			.addProgressListener((task, info) -> {
				int currentValue = actualNotificationsSubtask[0]; 
				actualNotificationsSubtask[0] = currentValue + 1;
				actualInfoSubtask.add(info);
			});
		
		// Task1 (completa)
		tasks.get(0).complete(new IProgressTrackerListener.ProgressInfo()
				.setMessage("INFO")
				.putAttribute("PARAM", 1));
		// Task 2 (al 50%)
		tasks.get(1).advance(50, new IProgressTrackerListener.ProgressInfo()
				.setMessage("INFO")
				.putAttribute("PARAM", 2));
		
		Assert.assertEquals(60, tracker.computeProgress());
		Assert.assertEquals(2, actualNotificationsMain[0]);
		Assert.assertEquals(1, actualNotificationsSubtask[0]);
		// Expected values
		int[] param1 = new int[] {1, 2};
		for(int i=0; i<actualInfoMain.size(); i++) {
			IProgressTrackerListener.ProgressInfo info = actualInfoMain.get(i);
			Assert.assertEquals("INFO", info.getMessage());
			Assert.assertEquals(1, info.getAttributes().size());
			Assert.assertEquals(param1[i], info.getAttributes().get("PARAM"));
		}
		// Expected values
		int[] param2 = new int[] {2};
		for(int i=0; i<actualInfoSubtask.size(); i++) {
			IProgressTrackerListener.ProgressInfo info = actualInfoSubtask.get(i);
			Assert.assertEquals("INFO", info.getMessage());
			Assert.assertEquals(1, info.getAttributes().size());
			Assert.assertEquals(param2[i], info.getAttributes().get("PARAM"));
		}
	}
	
	@Test
	public void subtaskListenerCompleteWithInfoTest()
	{
		// Create as array to make it editable from inside lambda expressions
		final int[] actualNotificationsMain = new int[] {0};	
		final int[] actualNotificationsSubtask1 = new int[] {0};
		final int[] actualNotificationsSubtask2 = new int[] {0};
		final List<IProgressTrackerListener.ProgressInfo> actualInfoMain = new ArrayList<>();
		final List<IProgressTrackerListener.ProgressInfo> actualInfoSubtask1 = new ArrayList<>();
		final List<IProgressTrackerListener.ProgressInfo> actualInfoSubtask2 = new ArrayList<>();
		
		ProgressTracker tracker = ProgressTracker.newTracker()
				.addProgressListener((task, info) -> {
					int currentValue = actualNotificationsMain[0]; 
					actualNotificationsMain[0] = currentValue + 1;
					actualInfoMain.add(info);
				});
		
		// Task1 -> 20%; Task2 -> 80%
		List<? extends IProgressTrackerTask> tasks = tracker.split(20, 80);
		tasks.get(1)
			.addProgressListener((task, info) -> {
				int currentValue = actualNotificationsSubtask2[0]; 
				actualNotificationsSubtask1[0] = currentValue + 1;
				actualInfoSubtask1.add(info);
			});
		tasks.get(1)
			.addProgressListener((task, info) -> {
				int currentValue = actualNotificationsSubtask2[0]; 
				actualNotificationsSubtask2[0] = currentValue + 1;
				actualInfoSubtask2.add(info);
			});
		
		// Task1 (complete)
		tasks.get(0).complete(new IProgressTrackerListener.ProgressInfo()
				.setMessage("INFO")
				.putAttribute("PARAM", 1));
		// Task 2 (complete)
		tasks.get(1).complete(new IProgressTrackerListener.ProgressInfo()
				.setMessage("INFO")
				.putAttribute("PARAM", 2));
		
		Assert.assertEquals(100, tracker.computeProgress());
		Assert.assertEquals(2, actualNotificationsMain[0]);
		Assert.assertEquals(1, actualNotificationsSubtask1[0]);
		Assert.assertEquals(1, actualNotificationsSubtask2[0]);
		// Expected values
		int[] param1 = new int[] {1, 2};
		for(int i=0; i<actualInfoMain.size(); i++) {
			IProgressTrackerListener.ProgressInfo info = actualInfoMain.get(i);
			Assert.assertEquals("INFO", info.getMessage());
			Assert.assertEquals(1, info.getAttributes().size());
			Assert.assertEquals(param1[i], info.getAttributes().get("PARAM"));
		}
		// Expected values
		int[] param2 = new int[] {2};
		for(int i=0; i<actualInfoSubtask1.size(); i++) {
			IProgressTrackerListener.ProgressInfo info = actualInfoSubtask1.get(i);
			Assert.assertEquals("INFO", info.getMessage());
			Assert.assertEquals(1, info.getAttributes().size());
			Assert.assertEquals(param2[i], info.getAttributes().get("PARAM"));
		}
		// Expected values
		int[] param3 = new int[] {2};
		for(int i=0; i<actualInfoSubtask2.size(); i++) {
			IProgressTrackerListener.ProgressInfo info = actualInfoSubtask2.get(i);
			Assert.assertEquals("INFO", info.getMessage());
			Assert.assertEquals(1, info.getAttributes().size());
			Assert.assertEquals(param3[i], info.getAttributes().get("PARAM"));
		}
	}
}
