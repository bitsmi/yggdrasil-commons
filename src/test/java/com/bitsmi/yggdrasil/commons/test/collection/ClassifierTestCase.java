package com.bitsmi.yggdrasil.commons.test.collection;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.Test;

import com.bitsmi.yggdrasil.commons.collection.Classification;
import com.bitsmi.yggdrasil.commons.collection.ClassifierCollector;
import com.bitsmi.yggdrasil.commons.collection.ClassifierMatcher;

public class ClassifierTestCase 
{
	@Test
	public void classificationTest()
	{
		ClassifierMatcher<String> classifier = new ClassifierMatcher<String>()
				.matcher("LENGHT_3", entry -> entry.length()==3)
				.matcher("LENGHT_4", entry -> entry.length()==4)
				.matcher("CONTENT_A", entry -> entry.contains("a"))
				.includeNonMatchResults();
		
		ClassifierCollector<String> collector = new ClassifierCollector<>(classifier);
		
		List<String> data = Arrays.asList("aaa", "bbb", "ccc", "aaaa", "bbbb", "cccc", "55555", "aaaaaaaaa");
		
		Classification<String> classification = data.stream()
				.collect(collector);
		
		assertThat(classification.getEntries()).hasSize(4);
		assertThat(classification.getEntry("LENGHT_3")).isPresent()
				.hasValueSatisfying(value -> {
					assertThat(value.getResults()).containsOnly("aaa", "bbb", "ccc");
				});
		assertThat(classification.getEntry("LENGHT_4")).isPresent()
				.hasValueSatisfying(value -> {
					assertThat(value.getResults()).containsOnly("aaaa", "bbbb", "cccc");
				});
		assertThat(classification.getEntry("CONTENT_A")).isPresent()
				.hasValueSatisfying(value -> {
					assertThat(value.getResults()).containsOnly("aaa", "aaaa", "aaaaaaaaa");
				});
		assertThat(classification.getNoneMatchEntry()).isPresent()
				.hasValueSatisfying(value -> {
					assertThat(value.getResults()).containsOnly("55555");
				});
	}
	
	@Test
    public void classificationNoNonMatchTest()
    {
	    ClassifierMatcher<String> classifier = new ClassifierMatcher<String>()
            .matcher("LENGHT_3", entry -> entry.length()==3)
            .matcher("LENGHT_4", entry -> entry.length()==4)
            .matcher("CONTENT_A", entry -> entry.contains("a"));
    
        ClassifierCollector<String> collector = new ClassifierCollector<>(classifier);
        
        List<String> data = Arrays.asList("aaa", "bbb", "ccc", "aaaa", "bbbb", "cccc", "55555", "aaaaaaaaa");
        
        Classification<String> classification = data.stream()
                .collect(collector);
        
        assertThat(classification.getEntries()).hasSize(3);
        assertThat(classification.getEntry("LENGHT_3")).isPresent()
                .hasValueSatisfying(value -> {
                    assertThat(value.getResults()).containsOnly("aaa", "bbb", "ccc");
                });
        assertThat(classification.getEntry("LENGHT_4")).isPresent()
                .hasValueSatisfying(value -> {
                    assertThat(value.getResults()).containsOnly("aaaa", "bbbb", "cccc");
                });
        assertThat(classification.getEntry("CONTENT_A")).isPresent()
                .hasValueSatisfying(value -> {
                    assertThat(value.getResults()).containsOnly("aaa", "aaaa", "aaaaaaaaa");
                });
        assertThat(classification.getNoneMatchEntry()).isNotPresent();
    }
	

    @Test
    public void classificationNoConfTest()
    {
        ClassifierMatcher<String> classifier = new ClassifierMatcher<String>();
    
        ClassifierCollector<String> collector = new ClassifierCollector<>(classifier);
        
        List<String> data = Arrays.asList("aaa", "bbb", "ccc", "aaaa", "bbbb", "cccc", "55555", "aaaaaaaaa");
        
        Classification<String> classification = data.stream()
            .collect(collector);
        
        assertThat(classification.getEntries()).isEmpty();
    }
}
