package com.bitsmi.yggdrasil.commons.test.util;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowable;

import java.io.File;
import java.nio.file.Paths;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Stream;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestTemplate;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.api.extension.Extension;
import org.junit.jupiter.api.extension.ExtensionContext;
import org.junit.jupiter.api.extension.ParameterContext;
import org.junit.jupiter.api.extension.ParameterResolver;
import org.junit.jupiter.api.extension.TestTemplateInvocationContext;
import org.junit.jupiter.api.extension.TestTemplateInvocationContextProvider;

import com.bitsmi.yggdrasil.commons.test.util.support.PojoWithValues;
import com.bitsmi.yggdrasil.commons.util.PropertiesEvaluator;
import com.bitsmi.yggdrasil.commons.util.Value;

public class PropertiesEvaluatorTestCase 
{
	@TestTemplate
	@ExtendWith(PropertiesEvaluatorTestContextProvider.class)
	public void injectValuesOkTest(PropertiesEvaluator evaluator) throws ReflectiveOperationException
	{
		PojoWithValues obj = new PojoWithValues();
		evaluator.injectValues(obj);
		
		assertThat(obj.getParam1()).isEqualTo("key4");
		assertThat(obj.getParam2()).isEqualTo("value2");
		assertThat(obj.getParam3()).isEqualTo("value5");
		assertThat(obj.getParam4()).isEqualTo("value4");
		assertThat(obj.getParam5()).isEqualTo("value5");
		assertThat(obj.getParam6()).isEqualTo("value6");
		assertThat(obj.getParam7()).isEqualTo("value7");
		assertThat(obj.getParam8()).isEqualTo("value8");
		assertThat(obj.getParam9()).isEqualTo("value9");
	}
	
	@Test
	public void defaultValueTest() throws ReflectiveOperationException
	{
		// Map properties
		Map<String, String> propertiesMap = PropertiesEvaluatorTestContextProvider.generateBasePropertyMap();
		// Remove "key2" in order to get de default value
		propertiesMap.remove("key2");
		
		PropertiesEvaluator evaluator = new PropertiesEvaluator()
				.addProperties(propertiesMap);
		
		PojoWithValues obj = new PojoWithValues();
		evaluator.injectValues(obj);
		
		assertNonNullValues(obj);
		assertThat(obj.getParam2()).isEqualTo("default value");
	}
	
	@Test
	public void resultSubstitutionTest() throws ReflectiveOperationException
	{
		// Map properties
		Map<String, String> propertiesMap = PropertiesEvaluatorTestContextProvider.generateBasePropertyMap();
		propertiesMap.put("replacement", "replacement_key");
		propertiesMap.put("key3", "prefix_${replacement}_suffix");
		propertiesMap.put("prefix_replacement_key_suffix", "replaced_value");

		PropertiesEvaluator evaluator = new PropertiesEvaluator()
				.addProperties(propertiesMap);
		
		PojoWithValues obj = new PojoWithValues();
		evaluator.injectValues(obj);
		
		assertNonNullValues(obj);
		assertThat(obj.getParam3()).isEqualTo("replaced_value");		
	}
	
	@Test
	public void propertyNotFoundOnPropertyTest()
	{
		// Map properties
		Map<String, String> propertiesMap = PropertiesEvaluatorTestContextProvider.generateBasePropertyMap();
		// Remove mandatory "key1"
		propertiesMap.remove("key1");
		
		PropertiesEvaluator evaluator = new PropertiesEvaluator()
				.addProperties(propertiesMap);
		
		PojoWithValues obj = new PojoWithValues();
		Throwable thrown = catchThrowable(() -> { 
			evaluator.injectValues(obj);
		});
		
		assertThat(thrown).isInstanceOf(IllegalArgumentException.class);
	}
	
	@Test
	public void propertyNotFoundOnValueTest()
	{
		// Map properties
		Map<String, String> propertiesMap = PropertiesEvaluatorTestContextProvider.generateBasePropertyMap();
		propertiesMap.put("key3", "${key999}_value");
		PropertiesEvaluator evaluator = new PropertiesEvaluator()
				.addProperties(propertiesMap);
		
		PojoWithValues obj = new PojoWithValues();
		Throwable thrown = catchThrowable(() -> { 
			evaluator.injectValues(obj);
		});
		
		assertThat(thrown).isInstanceOf(IllegalArgumentException.class);
	}
	
	/**
	 * Tests value injection on non string members
	 */
	@Test
	public void nonAssignebleValuesTest()
	{
		// Map properties
		Map<String, String> propertiesMap = PropertiesEvaluatorTestContextProvider.generateBasePropertyMap();
		
		PropertiesEvaluator evaluator = new PropertiesEvaluator()
				.addProperties(propertiesMap);
		
		PojoWithNonAssignableValues obj1 = new PojoWithNonAssignableValues();
		Throwable thrown1 = catchThrowable(() -> { 
			evaluator.injectValues(obj1);
		});
		
		// Error testing: All parameters must be compatible with String type
		PojoWithNonAssignableMethodParameters obj2 = new PojoWithNonAssignableMethodParameters();
		Throwable thrown2 = catchThrowable(() -> { 
			evaluator.injectValues(obj2);
		});
		
		assertThat(thrown1).isInstanceOf(IllegalArgumentException.class);
		assertThat(thrown2).isInstanceOf(IllegalArgumentException.class);
	}
	
	@Test
	public void invalidMethodAnnotationsTest()
	{
		// Map properties
		Map<String, String> propertiesMap = PropertiesEvaluatorTestContextProvider.generateBasePropertyMap();
		
		PropertiesEvaluator evaluator = new PropertiesEvaluator()
				.addProperties(propertiesMap);
		
		// Error testing: Methods with only 1 parameter, must be annotated at parameter or method level, but only one annotation is allowed
		PojoWithIncorrectAnnotations1 obj1 = new PojoWithIncorrectAnnotations1();
		Throwable thrown1 = catchThrowable(() -> { 
			evaluator.injectValues(obj1);
		});
		
		// Error testing: Methods annotated at parameter level, must have all it's parameters annotated
		PojoWithIncorrectAnnotations2 obj2 = new PojoWithIncorrectAnnotations2();
		Throwable thrown2 = catchThrowable(() -> { 
			evaluator.injectValues(obj2);
		});
		
		// Error testing: Methods must have at least 1 parameter
		PojoWithIncorrectAnnotations3 obj3 = new PojoWithIncorrectAnnotations3();
		Throwable thrown3 = catchThrowable(() -> { 
			evaluator.injectValues(obj3);
		});
		
		assertThat(thrown1).isInstanceOf(IllegalArgumentException.class);
		assertThat(thrown2).isInstanceOf(IllegalArgumentException.class);
		assertThat(thrown3).isInstanceOf(IllegalArgumentException.class);
	}
	
	/*--------------------------------------*
	 * TEST HELPERS
	 *--------------------------------------*/
	public void assertNonNullValues(PojoWithValues obj)
	{
		assertThat(obj.getParam1()).isNotNull();
		assertThat(obj.getParam2()).isNotNull();
		assertThat(obj.getParam3()).isNotNull();
		assertThat(obj.getParam4()).isNotNull();
		assertThat(obj.getParam5()).isNotNull();
		assertThat(obj.getParam6()).isNotNull();
		assertThat(obj.getParam7()).isNotNull();
		assertThat(obj.getParam8()).isNotNull();
		assertThat(obj.getParam9()).isNotNull();
	}
	
	public static class PropertiesEvaluatorTestContextProvider implements TestTemplateInvocationContextProvider
	{
		@Override
		public Stream<TestTemplateInvocationContext> provideTestTemplateInvocationContexts(ExtensionContext context) 
		{
			// Map properties
			Map<String, String> propertiesMap = generateBasePropertyMap();
			PropertiesEvaluator mapEvaluator = new PropertiesEvaluator()
					.addProperties(propertiesMap);
			
			// Classpath resources properties
			PropertiesEvaluator urlEvaluator = new PropertiesEvaluator();
			try {
				urlEvaluator.addFilePropertiesSourceFromClasspath("/com/bitsmi/yggdrasil/commons/test/util/support/evaluator.properties");
			}
			catch(Exception e) {
			    // Ignore
			}
			
			// File path properties
			PropertiesEvaluator filePathEvaluator = new PropertiesEvaluator();
			try {
			    File file = Paths.get("test-data", "evaluator.properties").toFile();
				filePathEvaluator.addFilePropertiesSource(file);
			}
			catch(Exception e) {
			    // Ignore
			}
			
			return Stream.of(invocationContext("Map evaluator", mapEvaluator),
					invocationContext("URL evaluator", urlEvaluator),
					invocationContext("File path evaluator", filePathEvaluator));
		}

		@Override
		public boolean supportsTestTemplate(ExtensionContext context) 
		{
			return true;
		}
		
		private TestTemplateInvocationContext invocationContext(String testName, PropertiesEvaluator parameter) 
		{
	        return new TestTemplateInvocationContext() 
	        {
	            @Override
	            public String getDisplayName(int invocationIndex) 
	            {
	                return testName;
	            }

	            @Override
	            public List<Extension> getAdditionalExtensions() 
	            {
	                return Collections.singletonList(new ParameterResolver() 
	                {
	                    @Override
	                    public boolean supportsParameter(ParameterContext parameterContext, ExtensionContext extensionContext) 
	                    {
	                        return parameterContext.getParameter().getType().equals(PropertiesEvaluator.class);
	                    }

	                    @Override
	                    public Object resolveParameter(ParameterContext parameterContext, ExtensionContext extensionContext) 
	                    {
	                        return parameter;
	                    }
	                });
	            }
	        };
	    }
		
		static Map<String, String> generateBasePropertyMap()
		{
			Map<String, String> propertiesMap = new HashMap<>();
			propertiesMap.put("key1", "key4");
			propertiesMap.put("key2", "value2");
			propertiesMap.put("key3", "key5");
			propertiesMap.put("pre_key4_ext", "value4");
			propertiesMap.put("key5", "value5");
			propertiesMap.put("key6", "value6");
			propertiesMap.put("key7", "value7");
			propertiesMap.put("key8", "value8");
			propertiesMap.put("key9", "value9");
			
			return propertiesMap;
		}
	}
	
	/*--------------------------------------*
	 * LOCAL SUPPORT CLASSES & METHODS
	 *--------------------------------------*/
	class PojoWithNonAssignableValues 
	{
		@Value("key1")
		private Date param1;
		
		public Date getParam1() 
		{
			return param1;
		}
	}
	
	/**
	 * Error testing: All parameters must be compatible with String type
	 */
	class PojoWithNonAssignableMethodParameters
	{
		private Date param1;
		
		public Date getParam1() 
		{
			return param1;
		}
		
		@Value("key1")
		public void setParam1(Date param1)
		{
			this.param1 = param1;
		}
	}
	
	/**
	 * Error testing: Methods with only 1 parameter, must be annotated at parameter or method level, but only one annotation is allowed
	 */
	class PojoWithIncorrectAnnotations1
	{
		private String param1;
		
		public String getParam1()
		{
			return param1;
		}
		
		@Value("key1")
		public void setParam1(@Value("key1")String param1)
		{
			this.param1 = param1;
		}
	}
	
	/**
	 * Error testing: Methods annotated at parameter level, must have all it's parameters annotated
	 */
	class PojoWithIncorrectAnnotations2
	{
		private String param1;
		private String param2;
		
		public String getParam1()
		{
			return param1;
		}
		
		public String getParam2()
		{
			return param2;
		}
		
		public void setParam1And2(@Value("key1")String param1, String param2)
		{
			this.param1 = param1;
			this.param2 = param2;
		}
	}
	
	/**
	 * Error testing: Methods must have at least 1 parameter
	 */
	class PojoWithIncorrectAnnotations3
	{
		private String param1;
		
		public String getParam1()
		{
			return param1;
		}
		
		@Value("key1")
		public void setParam1()
		{
			// Nothing to do...
		}
	}
}
