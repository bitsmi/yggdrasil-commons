package com.bitsmi.yggdrasil.commons.test.util;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowable;
import static org.junit.Assert.fail;

import java.util.stream.Stream;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import com.bitsmi.yggdrasil.commons.util.ReflectiveCallHelper;
import com.bitsmi.yggdrasil.commons.util.ReflectiveCallHelper.ReflectiveCallInstance;

public class ReflectiveCallHelperTestCase 
{
	/*------------------------------*
	 * METHODS WITH NO PARAMETERS TEST
	 *------------------------------*/
	@ParameterizedTest(name = "Values {0}, {1}, {2}, {3}")
	@MethodSource("createTestValues")
	public void reflectiveCallNoParamsTest(String arg1, String arg2, String arg3, String arg4) throws ReflectiveOperationException
	{
		Boolean result = ReflectiveCallHelper.of(ReflectiveCallTestPojo.class)
				.withConstructor(String.class, String.class)
				.instantiateWith(arg1, arg2)
				.call("allDefaultValuesEquals");
		
		ReflectiveCallTestPojo controlPojo = new ReflectiveCallTestPojo(arg1, arg2);
		
		assertThat(result)
				.isNotNull()
				.isEqualTo(controlPojo.allDefaultValuesEquals());
	}
	
	@ParameterizedTest(name = "Values {0}, {1}, {2}, {3}")
	@MethodSource("createTestValues")
	public void reflectiveCallDefaultConstructorTest(String arg1, String arg2, String arg3, String arg4) throws ReflectiveOperationException
	{
		Boolean result = ReflectiveCallHelper.of(ReflectiveCallTestPojo.class)				
				.withDefaultConstructor()
				.call("allDefaultValuesEquals");
		
		ReflectiveCallTestPojo controlPojo = new ReflectiveCallTestPojo();
		
		assertThat(result)
				.isNotNull()
				.isEqualTo(controlPojo.allDefaultValuesEquals());
	}
	
	@ParameterizedTest(name = "Values {0}, {1}, {2}, {3}")
	@MethodSource("createTestValues")
	public void reflectiveCallProvidedInstanceTest(String arg1, String arg2, String arg3, String arg4) throws ReflectiveOperationException
	{
		ReflectiveCallTestPojo providedPojo = new ReflectiveCallTestPojo();
		Boolean result = ReflectiveCallHelper.of(ReflectiveCallTestPojo.class)				
				.withInstance(providedPojo)
				.call("allDefaultValuesEquals");
		
		ReflectiveCallTestPojo controlPojo = new ReflectiveCallTestPojo();
		
		assertThat(result)
				.isNotNull()
				.isEqualTo(controlPojo.allDefaultValuesEquals());
	}
	
	/**
	 * Test instance immutability after multiple calls on same ReflectiveCallInstance
	 * Parameter constructor case
	 */
	@Test
	public void reflectiveCallNoParamsInstanceImmutabilityTest() throws ReflectiveOperationException
	{
		ReflectiveCallInstance<ReflectiveCallTestPojo> reflectiveInstance = ReflectiveCallHelper.of(ReflectiveCallTestPojo.class)
				.withConstructor(String.class, String.class)
				.instantiateWith("TestString1", "TestString2");
		
		reflectiveInstance.call("allDefaultValuesEquals");
		ReflectiveCallTestPojo instance1 = reflectiveInstance.getCurrentInstance();
		reflectiveInstance.call("allDefaultValuesEquals");
		ReflectiveCallTestPojo instance2 = reflectiveInstance.getCurrentInstance();
		
		assertThat(instance1==instance2);
	}
	
	/**
	 * Test instance immutability after multiple calls on same ReflectiveCallInstance
	 * Default constructor case 
	 */
	@Test
	public void reflectiveCallDefaultConstructorInstanceImmutabilityTest() throws ReflectiveOperationException
	{
		ReflectiveCallInstance<ReflectiveCallTestPojo> reflectiveInstance = ReflectiveCallHelper.of(ReflectiveCallTestPojo.class)				
				.withDefaultConstructor();
		
		reflectiveInstance.call("allDefaultValuesEquals");
		ReflectiveCallTestPojo instance1 = reflectiveInstance.getCurrentInstance();
		reflectiveInstance.call("allDefaultValuesEquals");
		ReflectiveCallTestPojo instance2 = reflectiveInstance.getCurrentInstance();
		
		assertThat(instance1==instance2);
	}
	
	/**
	 * Test instance immutability after multiple calls on same ReflectiveCallInstance
	 * Provided instance case
	 */
	@Test
	public void reflectiveCallProvidedInstanceImmutabilityTest() throws ReflectiveOperationException
	{
		ReflectiveCallTestPojo providedPojo = new ReflectiveCallTestPojo();
		ReflectiveCallInstance<ReflectiveCallTestPojo> reflectiveInstance = ReflectiveCallHelper.of(ReflectiveCallTestPojo.class)
				.withInstance(providedPojo);
		
		reflectiveInstance.call("allDefaultValuesEquals");
		ReflectiveCallTestPojo instance1 = reflectiveInstance.getCurrentInstance();
		reflectiveInstance.call("allDefaultValuesEquals");
		ReflectiveCallTestPojo instance2 = reflectiveInstance.getCurrentInstance();
		
		assertThat(instance1==instance2);
	}
	
	/**
	 * Test that object instance change every time that newInstance method is called
	 * Parameter constructor case
	 */
	@Test
	public void reflectiveCallNoParamsMultipleInstantiationTest() throws ReflectiveOperationException
	{
		ReflectiveCallInstance<ReflectiveCallTestPojo> reflectiveInstance = ReflectiveCallHelper.of(ReflectiveCallTestPojo.class)
				.withConstructor(String.class, String.class)
				.instantiateWith("TestString1", "TestString2");

		ReflectiveCallTestPojo instance0 = reflectiveInstance.getCurrentInstance();
		
		ReflectiveCallTestPojo instance1 = reflectiveInstance.newInstance();
		ReflectiveCallTestPojo instance2 = reflectiveInstance.newInstance();
		reflectiveInstance.call("allDefaultValuesEquals");
		ReflectiveCallTestPojo instance3 = reflectiveInstance.getCurrentInstance();
		
		assertThat(instance0).isNull();
		assertThat(instance1!=instance2);
		assertThat(instance2==instance3);
	}
	
	/**
	 * Test instance immutability after multiple calls on same ReflectiveCallInstance
	 * Default constructor case
	 */
	@Test
	public void reflectiveCallDefaultConstructorMultipleInstantiationTest() throws ReflectiveOperationException
	{
		ReflectiveCallInstance<ReflectiveCallTestPojo> reflectiveInstance = ReflectiveCallHelper.of(ReflectiveCallTestPojo.class)				
				.withDefaultConstructor();
		
		ReflectiveCallTestPojo instance0 = reflectiveInstance.getCurrentInstance();
		
		ReflectiveCallTestPojo instance1 = reflectiveInstance.newInstance();
		ReflectiveCallTestPojo instance2 = reflectiveInstance.newInstance();
		reflectiveInstance.call("allDefaultValuesEquals");
		ReflectiveCallTestPojo instance3 = reflectiveInstance.getCurrentInstance();
		
		assertThat(instance0).isNull();
		assertThat(instance1!=instance2);
		assertThat(instance2==instance3);
	}
	
	/**
	 * Test instance immutability after multiple calls on same ReflectiveCallInstance
	 * Provided instance case
	 */
	@Test
	public void reflectiveCallProvidedInstanceMultipleInstantiationTest() throws ReflectiveOperationException
	{
		ReflectiveCallTestPojo providedInstance = new ReflectiveCallTestPojo();
		ReflectiveCallInstance<ReflectiveCallTestPojo> reflectiveInstance = ReflectiveCallHelper.of(ReflectiveCallTestPojo.class)
				.withInstance(providedInstance);
		
		ReflectiveCallTestPojo instance0 = reflectiveInstance.getCurrentInstance();
		
		ReflectiveCallTestPojo instance1 = reflectiveInstance.newInstance();
		ReflectiveCallTestPojo instance2 = reflectiveInstance.newInstance();
		reflectiveInstance.call("allDefaultValuesEquals");
		ReflectiveCallTestPojo instance3 = reflectiveInstance.getCurrentInstance();
		
		assertThat(providedInstance==instance0);
		assertThat(instance1!=instance2);
		assertThat(instance2==instance3);
	}
	
	/*------------------------------*
	 * METHODS WITH PARAMETERS TEST
	 *------------------------------*/
	@ParameterizedTest(name = "Values {0}, {1}, {2}, {3}")
	@MethodSource("createTestValues")
	public void reflectiveCallWithParamsTest(String arg1, String arg2, String arg3, String arg4) throws ReflectiveOperationException
	{
		Boolean result = ReflectiveCallHelper.of(ReflectiveCallTestPojo.class)
				.withConstructor(String.class, String.class)
				.instantiateWith(arg1, arg2)
				.call("allValuesEquals", 
						new Class[] {String.class, String.class}, 
						arg3, 
						arg4);
		
		ReflectiveCallTestPojo controlPojo = new ReflectiveCallTestPojo(arg1, arg2);
		
		assertThat(result)
				.isNotNull()
				.isEqualTo(controlPojo.allValuesEquals(arg3, arg4));
	}
	
	@ParameterizedTest(name = "Values {0}, {1}, {2}, {3}")
	@MethodSource("createTestValues")
	public void reflectiveCallDefaultConstructorWithParamsTest(String arg1, String arg2, String arg3, String arg4) throws ReflectiveOperationException
	{
		Boolean result = ReflectiveCallHelper.of(ReflectiveCallTestPojo.class)				
				.withDefaultConstructor()
				.call("allValuesEquals", 
						new Class[] {String.class, String.class}, 
						arg3, 
						arg4);
		
		ReflectiveCallTestPojo controlPojo = new ReflectiveCallTestPojo();
		
		assertThat(result)
				.isNotNull()
				.isEqualTo(controlPojo.allValuesEquals(arg3, arg4));
	}
	
	@ParameterizedTest(name = "Values {0}, {1}, {2}, {3}")
	@MethodSource("createTestValues")
	public void reflectiveCallWithParamsProvidedInstanceTest(String arg1, String arg2, String arg3, String arg4) throws ReflectiveOperationException
	{
		ReflectiveCallTestPojo providedPojo = new ReflectiveCallTestPojo();
		Boolean result = ReflectiveCallHelper.of(ReflectiveCallTestPojo.class)				
				.withInstance(providedPojo)
				.call("allValuesEquals", 
						new Class[] {String.class, String.class}, 
						arg3, 
						arg4);
		
		ReflectiveCallTestPojo controlPojo = new ReflectiveCallTestPojo();
		
		assertThat(result)
				.isNotNull()
				.isEqualTo(controlPojo.allValuesEquals(arg3, arg4));
	}
	
	@Test
	public void reflectiveCallWithParamsInstanceImmutabilityTest() throws ReflectiveOperationException
	{
		ReflectiveCallInstance<ReflectiveCallTestPojo> reflectiveInstance = ReflectiveCallHelper.of(ReflectiveCallTestPojo.class)
				.withConstructor(String.class, String.class)
				.instantiateWith("TestString1", "TestString2");
		
		reflectiveInstance.call("allValuesEquals", new Class[] {String.class, String.class}, "TestString1", "TestString2");
		ReflectiveCallTestPojo instance1 = reflectiveInstance.getCurrentInstance();
		reflectiveInstance.call("allValuesEquals", new Class[] {String.class, String.class}, "TestString1", "TestString2");
		ReflectiveCallTestPojo instance2 = reflectiveInstance.getCurrentInstance();
		
		assertThat(instance1==instance2);
	}
	
	@Test
	public void reflectiveCallWithParamsDefaultConstructorImmutabilityTest() throws ReflectiveOperationException
	{
		ReflectiveCallInstance<ReflectiveCallTestPojo> reflectiveInstance = ReflectiveCallHelper.of(ReflectiveCallTestPojo.class)				
				.withDefaultConstructor();
		
		reflectiveInstance.call("allValuesEquals", new Class[] {String.class, String.class}, "TestString1", "TestString2");
		ReflectiveCallTestPojo instance1 = reflectiveInstance.getCurrentInstance();
		reflectiveInstance.call("allValuesEquals", new Class[] {String.class, String.class}, "TestString1", "TestString2");
		ReflectiveCallTestPojo instance2 = reflectiveInstance.getCurrentInstance();
		
		assertThat(instance1==instance2);
	}
	
	@Test
	public void reflectiveCallWithParamsProvidedInstanceImmutabilityTest() throws ReflectiveOperationException
	{
		ReflectiveCallTestPojo providedPojo = new ReflectiveCallTestPojo();
		ReflectiveCallInstance<ReflectiveCallTestPojo> reflectiveInstance = ReflectiveCallHelper.of(ReflectiveCallTestPojo.class)
				.withInstance(providedPojo);
		
		reflectiveInstance.call("allValuesEquals", new Class[] {String.class, String.class}, "TestString1", "TestString2");
		ReflectiveCallTestPojo instance1 = reflectiveInstance.getCurrentInstance();
		reflectiveInstance.call("allValuesEquals", new Class[] {String.class, String.class}, "TestString1", "TestString2");
		ReflectiveCallTestPojo instance2 = reflectiveInstance.getCurrentInstance();
		
		assertThat(instance1==instance2);
	}
	
	/**
	 * Test instance immutability after multiple calls on same ReflectiveCallInstance
	 */
	@Test
	public void reflectiveCallWithParamsMultipleInstantiationTest() throws ReflectiveOperationException
	{
		ReflectiveCallInstance<ReflectiveCallTestPojo> reflectiveInstance = ReflectiveCallHelper.of(ReflectiveCallTestPojo.class)
				.withConstructor(String.class, String.class)
				.instantiateWith("TestString1", "TestString2");
		
		ReflectiveCallTestPojo instance0 = reflectiveInstance.getCurrentInstance();
		
		ReflectiveCallTestPojo instance1 = reflectiveInstance.newInstance();
		ReflectiveCallTestPojo instance2 = reflectiveInstance.newInstance();
		reflectiveInstance.call("allValuesEquals", new Class[] {String.class, String.class}, "TestString1", "TestString2");
		ReflectiveCallTestPojo instance3 = reflectiveInstance.getCurrentInstance();
		
		assertThat(instance0).isNull();
		assertThat(instance1!=instance2);
		assertThat(instance2==instance3);
	}
	
	@Test
	public void reflectiveCallWithParamsDefaultConstructorMultipleInstantiationTest() throws ReflectiveOperationException
	{
		ReflectiveCallInstance<ReflectiveCallTestPojo> reflectiveInstance = ReflectiveCallHelper.of(ReflectiveCallTestPojo.class)				
				.withDefaultConstructor();
		
		ReflectiveCallTestPojo instance0 = reflectiveInstance.getCurrentInstance();
		
		ReflectiveCallTestPojo instance1 = reflectiveInstance.newInstance();
		ReflectiveCallTestPojo instance2 = reflectiveInstance.newInstance();
		reflectiveInstance.call("allValuesEquals", new Class[] {String.class, String.class}, "TestString1", "TestString2");
		ReflectiveCallTestPojo instance3 = reflectiveInstance.getCurrentInstance();
		
		assertThat(instance0).isNull();
		assertThat(instance1!=instance2);
		assertThat(instance2==instance3);
	}
	
	@Test
	public void reflectiveCallWithParamsProvidedInstanceMultipleInstantiationTest() throws ReflectiveOperationException
	{
		ReflectiveCallTestPojo providedInstance = new ReflectiveCallTestPojo();
		ReflectiveCallInstance<ReflectiveCallTestPojo> reflectiveInstance = ReflectiveCallHelper.of(ReflectiveCallTestPojo.class)				
				.withInstance(providedInstance);
		
		ReflectiveCallTestPojo instance0 = reflectiveInstance.getCurrentInstance();
		
		ReflectiveCallTestPojo instance1 = reflectiveInstance.newInstance();
		ReflectiveCallTestPojo instance2 = reflectiveInstance.newInstance();
		reflectiveInstance.call("allValuesEquals", new Class[] {String.class, String.class}, "TestString1", "TestString2");
		ReflectiveCallTestPojo instance3 = reflectiveInstance.getCurrentInstance();
		
		assertThat(providedInstance==instance0);
		assertThat(instance1!=instance2);
		assertThat(instance2==instance3);
	}
	
	/*------------------------------*
	 * STATIC METHODS TEST
	 *------------------------------*/
	@ParameterizedTest(name = "Values {0}, {1}, {2}, {3}")
	@MethodSource("createTestValues")
	public void reflectiveCallStaticWithParamsTest(String arg1, String arg2, String arg3, String arg4) throws ReflectiveOperationException
	{
		Boolean result = ReflectiveCallHelper.of(ReflectiveCallTestPojo.class)
				.callStatic("staticAllDefaultValuesEquals", 
						new Class[] {String.class, String.class, String.class, String.class},
						arg1,
						arg2,
						arg3, 
						arg4);
		
		assertThat(result)
				.isNotNull()
				.isEqualTo(ReflectiveCallTestPojo.staticAllDefaultValuesEquals(arg1, arg2, arg3, arg4));
	}
	
	@Test
	public void reflectiveCallStaticNoParamsTest() throws ReflectiveOperationException
	{
		String result = ReflectiveCallHelper.of(ReflectiveCallTestPojo.class)
				.callStatic("staticDefaultString");
			
		assertThat(result).isEqualTo(ReflectiveCallTestPojo.staticDefaultString());
	}
	
	/*------------------------------*
	 * DEFAULTS CHECK
	 *------------------------------*/
	@Test
	public void reflectiveCallIllegalDefaultConstructorTest() throws ReflectiveOperationException
	{
		Throwable throwable = catchThrowable(() -> {
			ReflectiveCallHelper.of(ReflectiveCallTestPojo.class)
					.withConstructor(String.class, String.class)
					.newDefaultInstance();
			
			fail();
		});
		
		assertThat(throwable).isInstanceOf(IllegalStateException.class);
	}
	
	@Test
	public void reflectiveCallIllegalInstanceTest() throws ReflectiveOperationException
	{
		Throwable throwable = catchThrowable(() -> {
			ReflectiveCallHelper.of(ReflectiveCallTestPojo.class)
					.withDefaultConstructor()
					.newInstance("TestString1", "TestString2");
			
			fail();
		});
		
		assertThat(throwable).isInstanceOf(IllegalStateException.class);
	}
	
	@Test
	public void reflectiveCallIllegalParamTypesTest() throws ReflectiveOperationException
	{
		Throwable throwable = catchThrowable(() -> {
			ReflectiveCallHelper.of(ReflectiveCallTestPojo.class)
					.withDefaultConstructor()
					.call("allValuesEquals", 
							new Class[] {String.class, String.class}, 
							"TestString1", 
							Boolean.TRUE);
			
			fail();
		});
		
		assertThat(throwable).isInstanceOf(IllegalStateException.class);
	}
	
	/*------------------------------*
	 * SUPPORT METHODS AND CLASSES
	 *------------------------------*/
	public static Stream<Object> createTestValues()
	{
		return Stream.of(new Object[] {"TestString1", "TestString1", "TestString1", "TestString1"},
				new Object[] {"TestString1", "TestString2", "TestString1", "TestString2"},
				new Object[] {"TestString1", "TestString1", "TestString2", "TestString2"},
				// Nulls test
				new Object[4]);
	}
	
	public static class ReflectiveCallTestPojo
	{
		private String arg1;
		private String arg2;
		
		public ReflectiveCallTestPojo()
		{
			
		}
		
		public ReflectiveCallTestPojo(String arg1, String arg2)
		{
			this.arg1 = arg1;
			this.arg2 = arg2;
		}
		
		public boolean allDefaultValuesEquals()
		{
			return this.arg1!=null 
					&& this.arg1.equals(this.arg2);
		}
		
		public boolean allValuesEquals(String arg1, String arg2)
		{
			return this.arg1!=null 
					&& this.arg1.equals(this.arg2)
					&& this.arg1.equals(arg1)
					&& this.arg1.equals(arg2);
		}
		
		public static boolean staticAllDefaultValuesEquals(String arg1, String arg2, String arg3, String arg4)
		{
			return arg1!=null 
					&& arg1.equals(arg2)
					&& arg1.equals(arg3)
					&& arg1.equals(arg4);
		}
		
		public static String staticDefaultString()
		{
			return "TestDefaultString";
		}
	}
}
