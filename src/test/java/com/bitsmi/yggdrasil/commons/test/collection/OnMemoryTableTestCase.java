package com.bitsmi.yggdrasil.commons.test.collection;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowable;

import java.util.HashMap;
import java.util.Map;

import org.junit.jupiter.api.Test;

import com.bitsmi.yggdrasil.commons.collection.OnMemoryTable;
import com.bitsmi.yggdrasil.commons.collection.OnMemoryTable.TableRecord;
import com.bitsmi.yggdrasil.commons.collection.QueryableCollectionFactory;

public class OnMemoryTableTestCase
{
	/*--------------------------------------*
	 * CREATION TESTS
	 *--------------------------------------*/
	@Test
	public void creationTest()
	{
		OnMemoryTable table = QueryableCollectionFactory.newOnMemoryTable()
				.withColumns("ID", "NAME", "VALUE")
				.build();
		
		table.addRecord(1L, "Row 1", "Value 1")
				.addRecord(2L, "Row 2", "Value 2")
				.addRecord(3L, "Row 3", "Value 3");

		Long id2 = (Long)table.getValue(1, "ID");
		String name2 = (String)table.getValue(1, "NAME");
		String value2 = (String)table.getValue(1, "VALUE");
		
		assertThat(table.size()).isEqualTo(3);
		assertThat(id2).isEqualTo(2L);
		assertThat(name2).isEqualTo("Row 2");
		assertThat(value2).isEqualTo("Value 2");
	}
	
	/*--------------------------------------*
	 * CREATION ERROR TESTS
	 *--------------------------------------*/
	@Test
	public void creationRepeatedColumnNamesTest()
	{
	    Throwable thrown = catchThrowable(() -> {
	        QueryableCollectionFactory.newOnMemoryTable()
	        		.withColumns("ID", "NAME", "VALUE", "ID")
	        		.build();
	    });
	    
	    assertThat(thrown).isNotNull().isInstanceOf(IllegalStateException.class);
	}
	
	@Test
	public void creationMismatchColumnsTest()
	{
		OnMemoryTable table = QueryableCollectionFactory.newOnMemoryTable()
				.withColumns("ID", "NAME", "VALUE")
				.build();
		
		Throwable thrown = catchThrowable(() -> {
			table.addRecord(1L, "Row 1");
		});
		
		assertThat(thrown).isNotNull().isInstanceOf(IllegalArgumentException.class);
	}
	
	/*--------------------------------------*
	 * ADD ERROR TESTS
	 *--------------------------------------*/
	@Test
	public void addMapMismatchColumnTest()
	{
		OnMemoryTable table = QueryableCollectionFactory.newOnMemoryTable()
				.withColumns("ID", "NAME", "VALUE")
				.build();
		
		Map<String, Object> data = new HashMap<>();
		data.put("ID", 1L);
		data.put("MISMATCH", "Mismatch Value");
		data.put("VALUE", "Value 1");
		
		Throwable thrown = catchThrowable(() -> {
			table.addRecord(data);
		});
		
		assertThat(thrown).isNotNull().isInstanceOf(IllegalArgumentException.class);
	}
	
	@Test
	public void addRecordMismatchTableTest()
	{
		OnMemoryTable table1 = QueryableCollectionFactory.newOnMemoryTable()
				.withColumns("ID", "NAME", "VALUE")
				.build();
		OnMemoryTable table2 = QueryableCollectionFactory.newOnMemoryTable()
				.withColumns("COL1", "COl2", "COL3")
				.build();
		
		TableRecord regTable2 = table2.buildRecord(1L, "Row 1", "Value1");
		
		Throwable thrown = catchThrowable(() -> {
			table1.add(regTable2);
		});
		
		assertThat(thrown).isNotNull().isInstanceOf(IllegalArgumentException.class);
	}
	
	/*--------------------------------------*
     * REMOVE TESTS
     *--------------------------------------*/
	@Test
    public void removeRecordTest()
    {
        OnMemoryTable table = QueryableCollectionFactory.newOnMemoryTable()
                .withColumns("ID", "NAME", "VALUE")
                .build();
        
        table.addRecord(1L, "Row 1", "Value 1")
                .addRecord(2L, "Row 2", "Value 2")
                .addRecord(3L, "Row 3", "Value 3");

        TableRecord record = table.get(1);
        boolean removed = table.remove(record);
        
        assertThat(table.size()).isEqualTo(2);
        assertThat(removed).isTrue();
        assertThat(table.get(0)).extracting(elem -> elem.getValue("ID"),
                elem -> elem.getValue("NAME"),
                elem -> elem.getValue("VALUE"))
            .containsExactly(1L, "Row 1", "Value 1");
        assertThat(table.get(1)).extracting(elem -> elem.getValue("ID"),
                elem -> elem.getValue("NAME"),
                elem -> elem.getValue("VALUE"))
            .containsExactly(3L, "Row 3", "Value 3");
    }
	
	@Test
    public void removeRecordsByColumnValueTest()
    {
        OnMemoryTable table = QueryableCollectionFactory.newOnMemoryTable()
                .withColumns("ID", "NAME", "VALUE", "REMOVE")
                .build();
        
        table.addRecord(1L, "Row 1", "Value 1", Boolean.TRUE)
                .addRecord(2L, "Row 2", "Value 2", Boolean.FALSE)
                .addRecord(3L, "Row 3", "Value 3", Boolean.TRUE);

        long removedCount = table.removeRecords("REMOVE", Boolean.TRUE);
        
        Long id = (Long)table.getValue(0, "ID");
        String name = (String)table.getValue(0, "NAME");
        String value = (String)table.getValue(0, "VALUE");
        
        assertThat(table.size()).isEqualTo(1);
        assertThat(removedCount).isEqualTo(2);
        assertThat(id).isEqualTo(2L);
        assertThat(name).isEqualTo("Row 2");
        assertThat(value).isEqualTo("Value 2");
    }
	
	@Test
	public void clearTest()
	{
	    OnMemoryTable table = QueryableCollectionFactory.newOnMemoryTable()
            .withColumns("ID", "NAME", "VALUE", "REMOVE")
            .build();
    
        table.addRecord(1L, "Row 1", "Value 1", Boolean.TRUE)
                .addRecord(2L, "Row 2", "Value 2", Boolean.FALSE)
                .addRecord(3L, "Row 3", "Value 3", Boolean.TRUE);
        
        table.clear();
        
        long size = table.size();
        Throwable thrown = catchThrowable(() -> {
            table.get(0);
        });
        
        assertThat(size).isZero();
        assertThat(thrown).isNotNull().isInstanceOf(IndexOutOfBoundsException.class);
	}
}
