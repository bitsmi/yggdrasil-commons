package com.bitsmi.yggdrasil.commons.test.util;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowable;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.junit.jupiter.api.Test;

import com.bitsmi.yggdrasil.commons.exception.CodedException;
import com.bitsmi.yggdrasil.commons.util.UncheckedFunctionals;

public class UncheckedFunctionalsTestCase 
{
	/*------------------------* 
	 * CONSUMER 
	 *------------------------*/
	@Test
	public void consumerTest()
	{
		AtomicInteger counter = new AtomicInteger(0);
		
		Stream.of("1", "2", "3")
			.forEach(UncheckedFunctionals.uncheckedConsumer(i -> {
				counter.incrementAndGet();
			}));
		
		assertThat(counter.get()).isEqualTo(3);
	}
	
	@Test
	public void consumerExceptionTest()
	{
		Throwable thrown = catchThrowable(() -> { 
			Stream.of("1", "2", "3")
			.forEach(UncheckedFunctionals.uncheckedConsumer(i -> {
				throw new CodedException("TEST");
			}));
		});
		
		assertThat(thrown).isInstanceOf(CodedException.class);
	}
	
	/*------------------------* 
	 * SUPPLIER 
	 *------------------------*/
	@Test
	public void supplierTest()
	{
		String a = UncheckedFunctionals.uncheckedSupplier(() -> "A").get();
		assertThat(a).isEqualTo("A");
	}
	
	@Test
	public void supplierExceptionTest()
	{
		Throwable thrown = catchThrowable(() -> { 
			UncheckedFunctionals.uncheckedSupplier(() -> {
				throw new CodedException("TEST");
			}).get();
		});
		
		assertThat(thrown).isInstanceOf(CodedException.class);
	}
	
	/*------------------------* 
	 * FUNCTION 
	 *------------------------*/
	@Test
	public void functionTest()
	{
		List<String> results = Stream.of("1", "2", "3")
				.map(UncheckedFunctionals.uncheckedFunction(i -> {
					return "MAP-" + i;
				}))
				.collect(Collectors.toList());
		
		assertThat(results)
				.hasSize(3)
				.containsOnly("MAP-1", "MAP-2", "MAP-3");
	}
	
	@Test
	public void functionExceptionTest()
	{
		Throwable thrown = catchThrowable(() -> { 
			Stream.of("1", "2", "3")
				.map(UncheckedFunctionals.uncheckedFunction(i -> {
					throw new CodedException("TEST");
				}))
				.collect(Collectors.toList());
		});
		
		assertThat(thrown).isInstanceOf(CodedException.class);
	}
	
	/*------------------------* 
	 * BIFUNCTION 
	 *------------------------*/
	@Test
	public void biFunctionTest()
	{
		Map<String, Integer> data = new HashMap<>();
		data.put("KEY1", 1);
		data.put("KEY2", 2);
		data.put("KEY3", 3);
		
		data.computeIfPresent("KEY2", UncheckedFunctionals.uncheckedBiFunction((key, value) -> value*1000));
		
		assertThat(data.values())
				.hasSize(3)
				.containsOnly(1, 2000, 3);
	}
	
	@Test
	public void biFunctionExceptionTest()
	{
		Throwable thrown = catchThrowable(() -> {
			Map<String, Integer> data = new HashMap<>();
			data.put("KEY1", 1);
			data.put("KEY2", 2);
			data.put("KEY3", 3);
			
			data.computeIfPresent("KEY2", UncheckedFunctionals.uncheckedBiFunction((key, value) -> {
				throw new CodedException("TEST");
			}));
		});
		
		assertThat(thrown).isInstanceOf(CodedException.class);
	}
}
