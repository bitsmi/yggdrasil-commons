package com.bitsmi.yggdrasil.commons.test.collection;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.TreeSet;
import java.util.stream.Stream;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import com.bitsmi.yggdrasil.commons.collection.FluentCollection;

public class FluentCollectionTestCase
{
    /*-------------------------------*
     * FLUENT COLLECTION
     *-------------------------------*/
    @ParameterizedTest(name = "Test {0}")
    @MethodSource("createCollectionValues")
    public void addTest(String testName, boolean ordered, Collection<String> collection)
    {
        Collection<String> resultCollection = FluentCollection.of(collection)
                .clear()
                .add("VALUE1")
                .add("VALUE2")
                .add("VALUE3")
                .addAll(Arrays.asList("VALUE4", "VALUE5"))
                .get();
        
        if(ordered) {
            assertThat(resultCollection).hasSize(5)
                    .containsExactly("VALUE1", "VALUE2", "VALUE3", "VALUE4", "VALUE5");
        }
        else {
            assertThat(resultCollection).hasSize(5)
                    .contains("VALUE1", "VALUE2", "VALUE3", "VALUE4", "VALUE5");
        }
    }
    
    @ParameterizedTest(name = "Test {0}")
    @MethodSource("createCollectionValues")
    public void retainAllTest(String testName, boolean ordered, Collection<String> collection)
    {
        Collection<String> resultCollection = FluentCollection.of(collection)
                .clear()
                .add("VALUE1")
                .add("VALUE2")
                .add("VALUE3")
                .retainAll(Arrays.asList("VALUE1", "VALUE3"))
                .get();
        
        if(ordered) {
            assertThat(resultCollection).hasSize(2)
                    .containsExactly("VALUE1", "VALUE3");
        }
        else {
            assertThat(resultCollection).hasSize(2)
                    .contains("VALUE1", "VALUE3");
        }
    }
    
    @ParameterizedTest(name = "Test {0}")
    @MethodSource("createCollectionValues")
    public void iteratorTest(String testName, boolean ordered, Collection<String> collection)
    {
        Collection<String> resultCollection = FluentCollection.of(collection)
                .clear()
                .add("VALUE1")
                .add("VALUE2")
                .add("VALUE3")
                .get();
        
        if(ordered) {
            assertThat(resultCollection.stream()).hasSize(3)
                    .containsExactly("VALUE1", "VALUE2", "VALUE3");
        }
        else {
            assertThat(resultCollection.stream()).hasSize(3)
                    .contains("VALUE1", "VALUE2", "VALUE3");
        }
    }
    
    @ParameterizedTest(name = "Test {0}")
    @MethodSource("createCollectionValues")
    public void streamTest(String testName, boolean ordered, Collection<String> collection)
    {
        Collection<String> resultCollection = FluentCollection.of(collection)
                .clear()
                .add("VALUE1")
                .add("VALUE2")
                .add("VALUE3")
                .get();
        
        if(ordered) {
            assertThat(resultCollection.stream()).hasSize(3)
                    .containsExactly("VALUE1", "VALUE2", "VALUE3");
        }
        else {
            assertThat(resultCollection.stream()).hasSize(3)
                    .contains("VALUE1", "VALUE2", "VALUE3");
        }
    }
    
    /*--------------------------------*
     * SUPPORT METHODS AND CLASSES
     * ParameterizedTest's method sources must be public to be accessible by java module system
     *--------------------------------*/
    public static Stream<Object> createCollectionValues()
    {
        final HashSet<String> nonEmptySet = new HashSet<>();
        nonEmptySet.add("INITIAL VALUE");
        
        final ArrayList<String> nonEmptyList = new ArrayList<>();
        nonEmptyList.add("INITIAL VALUE");
        
        return Stream.of(
                /* SETS */
                new Object[] {"HashSet", Boolean.FALSE,
                    new HashSet<String>()
                },
                new Object[] {"LinkedHashSet", Boolean.TRUE, 
                    new LinkedHashSet<String>()
                },
                new Object[] {"TreeSet", Boolean.TRUE,
                    new TreeSet<String>()
                },
                new Object[] {"Non Empty Set", Boolean.FALSE,
                    nonEmptySet
                },
                /* LISTS */
                new Object[] {"ArrayList", Boolean.TRUE,
                    new ArrayList<String>()
                },
                new Object[] {"LinkedList", Boolean.TRUE, 
                    new LinkedList<String>()
                },
                new Object[] {"Non Empty List", Boolean.TRUE, 
                    nonEmptyList
                },
                /* QUEUES */
                new Object[] {"ArrayDeque", Boolean.TRUE,
                    new ArrayDeque<String>()
                });
    }
}
