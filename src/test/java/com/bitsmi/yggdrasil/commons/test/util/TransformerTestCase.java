package com.bitsmi.yggdrasil.commons.test.util;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;

import com.bitsmi.yggdrasil.commons.test.util.support.TransformerTestPojos.SourcePojoTransformerFactory;
import com.bitsmi.yggdrasil.commons.test.util.support.TransformerTestPojos.TargetPojo;
import com.bitsmi.yggdrasil.commons.test.util.support.TransformerTestPojos.TransformableSourcePojo;

public class TransformerTestCase
{
    @Test
    public void transformedTest()
    {
        TransformableSourcePojo srcPojo = new TransformableSourcePojo()
                .setSourceValue1("Value 1")
                .setSourceValue2("Value 2");
        
        TargetPojo targetPojo = srcPojo.transformed()
                .by(SourcePojoTransformerFactory.toTargetPojo());
        
        assertThat(targetPojo.getTargetValue1()).isEqualTo(srcPojo.getSourceValue1());
        assertThat(targetPojo.getTargetValue2()).isEqualTo(srcPojo.getSourceValue2());
    }
}
