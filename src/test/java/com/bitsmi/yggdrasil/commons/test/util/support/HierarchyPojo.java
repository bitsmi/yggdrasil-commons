package com.bitsmi.yggdrasil.commons.test.util.support;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

public class HierarchyPojo <T> 
{
	private T value;
	private List<HierarchyPojo<T>> children;
	
	public T getValue() 
	{
		return value;
	}
	
	public HierarchyPojo<T> setValue(T value) 
	{
		this.value = value;
		return this;
	}
	
	public List<HierarchyPojo<T>> getChildren() 
	{
		if(children==null) {
			return Collections.emptyList();
		}
		return children;
	}
	
	public HierarchyPojo<T> setChildren(List<HierarchyPojo<T>> children) 
	{
		this.children = children;
		return this;
	}
	
	public HierarchyPojo<T> addChild(HierarchyPojo<T> child)
	{
		if(children==null) {
			children = new LinkedList<>();
		}
		children.add(child);
		
		return this;
	}
}
