package com.bitsmi.yggdrasil.commons.test.collection;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.entry;

import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.TreeMap;
import java.util.stream.Stream;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import com.bitsmi.yggdrasil.commons.collection.FluentMap;

public class FluentMapTestCase
{
    @Test
    public void newHashMapPutTest()
    {
        HashMap<String, String> map = FluentMap.<String, String>newHashMap()
                .put("KEY1", "VALUE1")
                .put("KEY2", "VALUE2")
                .put("KEY3", "VALUE3")
                .get();
        
        assertThat(map).hasSize(3)
                .contains(entry("KEY1", "VALUE1"), entry("KEY2", "VALUE2"), entry("KEY3", "VALUE3"));
    }
    
    @Test
    public void newLinkedHashSetAddTest()
    {
        LinkedHashMap<String, String> map = FluentMap.<String, String>newLinkedHashMap()
                .put("KEY1", "VALUE1")
                .put("KEY2", "VALUE2")
                .put("KEY3", "VALUE3")
                .get();
        
        assertThat(map).hasSize(3)
                .contains(entry("KEY1", "VALUE1"), entry("KEY2", "VALUE2"), entry("KEY3", "VALUE3"));
    }
    
    @Test
    public void newTreeSetAddTest()
    {
        TreeMap<String, String> map = FluentMap.<String, String>newTreeMap()
                .put("KEY1", "VALUE1")
                .put("KEY3", "VALUE3")
                .put("KEY2", "VALUE2")
                .get();
        
        assertThat(map).hasSize(3)
                // Ordered
                .containsExactly(entry("KEY1", "VALUE1"), entry("KEY2", "VALUE2"), entry("KEY3", "VALUE3"));
    }
    
    @Test
    public void newLinkedTreeSetComparatorTest()
    {
        TreeMap<String, String> map = FluentMap.<String, String>newTreeMap(Comparator.reverseOrder())
                .put("KEY1", "VALUE1")
                .put("KEY3", "VALUE3")
                .put("KEY2", "VALUE2")
                .get();
        
        assertThat(map).hasSize(3)
                // Reverse order
                .containsExactly(entry("KEY3", "VALUE3"), entry("KEY2", "VALUE2"), entry("KEY1", "VALUE1"));
    }
    
    @ParameterizedTest(name = "Test {0}")
    @MethodSource("createMapValues")
    public void ofMapPutTest(String testName, Map<String, String> map)
    {
        Map<String, String> additionalData = new HashMap<>();
        additionalData.put("KEY4", "VALUE4");
        additionalData.put("KEY5", "VALUE5");
        additionalData.put("KEY6", "VALUE6");
        additionalData.put("KEY7", "VALUE7");
        
        Map<String, String> resultMap = FluentMap.of(map)
                .clear()
                .put("KEY1", "VALUE1")
                .put("KEY2", "VALUE2")
                .put("KEY3", "VALUE3")
                .putAll(additionalData)
                .putIfAbsent("KEY8", "VALUE8")
                // Not put
                .putIfAbsent("KEY1", "VALUE1 MOD")
                .compute("KEY2", (k, v) -> v.concat(" MOD"))
                .compute("KEY9", (k, v) -> "VALUE9")
                // Not put
                .compute("KEY99", (k, v) -> null)
                // Removed
                .computeIfPresent("KEY3", (k, v) -> null)
                .computeIfAbsent("KEY10", k -> "VALUE10")
                // Not put
                .computeIfAbsent("KEY4", k -> "VALUE4 MOD")
                .merge("KEY11", "VALUE11", (v1, v2) -> v1.concat(" REPLACED BY ").concat(v2))
                .merge("KEY7", "VALUE7.1", (v1, v2) -> v1.concat(" REPLACED BY ").concat(v2))
                .get();
        
        assertThat(resultMap).hasSize(10)
                .contains(
                    entry("KEY1", "VALUE1"), 
                    entry("KEY2", "VALUE2 MOD"), 
                    // KEY3 removed
                    entry("KEY4", "VALUE4"),
                    entry("KEY5", "VALUE5"),
                    entry("KEY6", "VALUE6"),
                    entry("KEY7", "VALUE7 REPLACED BY VALUE7.1"),
                    entry("KEY8", "VALUE8"),
                    entry("KEY9", "VALUE9"),
                    entry("KEY10", "VALUE10"),
                    entry("KEY11", "VALUE11"));
    }
    
    @Test
    public void replaceTest()
    {
        Map<String, String> map = FluentMap.<String, String>newHashMap()
                .put("KEY1", "VALUE1")
                .put("KEY2", "VALUE2")
                .put("KEY3", "VALUE3")
                .replace("KEY1", "VALUE1.1")
                .replace("KEY2", "ND", "VALUE2.1")
                .replace("KEY3", "VALUE3", "VALUE3.1")
                .get();
        
        assertThat(map).hasSize(3)
                .contains(
                    entry("KEY1", "VALUE1.1"), 
                    entry("KEY2", "VALUE2"), 
                    entry("KEY3", "VALUE3.1"));
    }
    
    @Test
    public void replaceAllTest()
    {
        Map<String, String> map = FluentMap.<String, String>newHashMap()
                .put("KEY1", "VALUE1")
                .put("KEY2", "VALUE2")
                .put("KEY3", "VALUE3")
                .replaceAll((k, v) -> v.concat(" REPLACED"))
                .get();
        
        assertThat(map).hasSize(3)
                .contains(
                    entry("KEY1", "VALUE1 REPLACED"), 
                    entry("KEY2", "VALUE2 REPLACED"), 
                    entry("KEY3", "VALUE3 REPLACED"));
    }
    
    /*--------------------------------*
     * SUPPORT METHODS AND CLASSES
     * ParameterizedTest's method sources must be public to be accessible by java module system
     *--------------------------------*/
    public static Stream<Object> createMapValues()
    {
        final HashMap<String, String> nonEmptyMap = new HashMap<>();
        nonEmptyMap.put("INITIAL KEY", "INITIAL VALUE");
        
        return Stream.of(new Object[] {"HashMap", 
                    new HashMap<String, String>()
                },
                new Object[] {"LinkedHashMap", 
                    new LinkedHashMap<String, String>()
                },
                new Object[] {"TreeMap", 
                    new TreeMap<String, String>()
                },
                new Object[] {"Non Empty Map", 
                    nonEmptyMap
                });
    }
}
