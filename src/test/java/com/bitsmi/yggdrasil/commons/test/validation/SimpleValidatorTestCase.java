package com.bitsmi.yggdrasil.commons.test.validation;

import static com.bitsmi.yggdrasil.commons.validation.Constraints.isGreaterThan;
import static com.bitsmi.yggdrasil.commons.validation.Constraints.isLessThan;
import static org.assertj.core.api.Assertions.assertThat;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import com.bitsmi.yggdrasil.commons.test.validation.support.TestBean;
import com.bitsmi.yggdrasil.commons.validation.ConstraintViolation;
import com.bitsmi.yggdrasil.commons.validation.Constraints;
import com.bitsmi.yggdrasil.commons.validation.IValidator;
import com.bitsmi.yggdrasil.commons.validation.Validation;
import com.bitsmi.yggdrasil.commons.validation.Validation.AbstractConstraintExecution;
import com.bitsmi.yggdrasil.commons.validation.Validation.ValidationExecution;

public class SimpleValidatorTestCase
{
    @Test
    @DisplayName("Simple validation success Test")
    public void simpleValidationTest1()
    {
        TestBean instance = new TestBean()
            .setName("john.random@foo.bar")
            .setAge(18);
        
        Validation validation = new TestBeanValidator().validate(instance);
        
        ValidationExecution execution = validation.getExecution();
        assertThat(execution.getGroupExecution(Validation.EXECUTION_GROUP_DEFAULT).getStatementExecutions())
                .hasSize(2)
                .extracting(Validation.ConstraintStatementExecution::getResult)
                .containsExactly(Validation.Result.SUCCEEDED, Validation.Result.SUCCEEDED);
        
        assertThat(validation.isSuccess()).isTrue();
        assertThat(validation.isFailed()).isFalse();
        assertThat(validation.isSkipped()).isFalse();
        assertThat(validation.getConstraintViolations())
                .isEmpty();
        
        assertThat(execution.getGroupExecution("Optional validations 1").isSuccess())
                .isTrue();
        assertThat(execution.getGroupExecution("Optional validations 1").getStatementExecutions())
                .hasSize(1)
                .extracting(Validation.ConstraintStatementExecution::getResult)
                .containsExactly(Validation.Result.SUCCEEDED);
        
        assertThat(execution.getGroupExecution("Optional validations 2").isSkipped())
                .isTrue();
        
    }
    
    @Test
    @DisplayName("Simple validation fail Test")
    public void simpleValidationTest2()
    {
        TestBean instance = new TestBean()
            .setName("john.random@foo.bar")
            .setAge(30);
        
        Validation validation = new TestBeanValidator().validate(instance);
        
        ValidationExecution execution = validation.getExecution();
        assertThat(execution.getGroupExecution(Validation.EXECUTION_GROUP_DEFAULT).getStatementExecutions())
                .hasSize(2)
                .extracting(Validation.ConstraintStatementExecution::getResult)
                .containsExactly(Validation.Result.SUCCEEDED, Validation.Result.FAILED);
        
        assertThat(validation.isSuccess()).isFalse();
        assertThat(validation.isFailed()).isTrue();
        assertThat(validation.isSkipped()).isFalse();
        assertThat(validation.getConstraintViolations())
                .hasSize(1)
                .extracting(ConstraintViolation::getConstraintName, ConstraintViolation::getMessage)
                .containsOnly(Assertions.tuple("Valid user age", "Value (30) is not a valid user age"));
        
        assertThat(execution.getGroupExecution("Optional validations 1").isSuccess())
                .isTrue();
        assertThat(execution.getGroupExecution("Optional validations 1").getStatementExecutions())
                .hasSize(1)
                .extracting(Validation.ConstraintStatementExecution::getResult)
                .containsExactly(Validation.Result.SUCCEEDED);
        
        assertThat(execution.getGroupExecution("Optional validations 2").isSkipped())
                .isTrue();
    }
    
    @Test
    @DisplayName("Simple validation skip Test")
    public void simpleValidationTest3()
    {
    	Validation validation = Validation.validationWithDefaultExecutionGroup();
    	validation.skip();
    	
    	assertThat(validation.isSuccess()).isFalse();
        assertThat(validation.isFailed()).isFalse();
        assertThat(validation.isSkipped()).isTrue();
    }

    /*----------------------------------*
     * HELPER METHODS AND CLASSES
     *----------------------------------*/
    private static class TestBeanValidator implements IValidator<TestBean>
    {
        @Override
        public Validation validate(TestBean instance)
        {
            Validation validation = Validation.validationWithDefaultExecutionGroup();
        
            /* Validate name */
            AbstractConstraintExecution userNameConstraintExecution = Constraints.isNotNull(String.class)
                    .and(Constraints.isEmail())
                    .test(instance.getName());
            Validation.ConstraintStatementExecution userNameValidationStatement = Validation.executedStatement("Valid user name", instance, userNameConstraintExecution);
            if(userNameValidationStatement.isFail()) {
                validation.addConstraintViolation(new ConstraintViolation(userNameValidationStatement.getName(), 
                        "Value (" + instance.getName() + ") is not a valid user name"));
            }
            validation.getExecution().addStatementExecution(userNameValidationStatement);
            
            /* Validate age */
            AbstractConstraintExecution ageConstraintExecution = Constraints.isNotNull(Integer.class)
                    .and(Constraints.isLessThan(20)
                        .or(Constraints.isGreaterThan(50)))
                    .test(instance.getAge());
            Validation.ConstraintStatementExecution ageValidationStatement = Validation.executedStatement("Valid user age", instance, ageConstraintExecution);
            if(ageConstraintExecution.isFail()) {
                validation.addConstraintViolation(new ConstraintViolation(ageValidationStatement.getName(), 
                        "Value (" + instance.getAge() + ") is not a valid user age"));
            }
            validation.getExecution().addStatementExecution(ageValidationStatement);
            if(userNameConstraintExecution.isSuccess() && ageConstraintExecution.isSuccess()) {
                validation
                    .getExecution()
                    .selectedGroup()
                    .success();
            }
            else {
                validation
                    .getExecution()
                    .selectedGroup()
                    .fail();
            }
            
            /* ValidationMode == ALL */

            /* Optional validations 1 */
            validation.getExecution().selectGroup("Optional validations 1");
            if(!instance.getName().endsWith("@foo.bar")) {
                validation
                    .getExecution()
                    .selectedGroup()
                    .skip();
            }
            else {
                AbstractConstraintExecution optionalConstraintExecution = Constraints.map(String.class, 
                        name -> name==null ? 0 : name.length(), 
                            isGreaterThan(10)
                                .and(isLessThan(100)))                    
                        .test(instance.getName());
                Validation.ConstraintStatementExecution optionalStatement = Validation.executedStatement("Valid user name", instance, optionalConstraintExecution);
                if(optionalConstraintExecution.isFail()) {
                    validation.addConstraintViolation(new ConstraintViolation(ageValidationStatement.getName(), 
                            "Value (" + instance.getName() + ") is not a valid user name"));
                }
                validation.getExecution().addStatementExecution(optionalStatement);
                if(optionalConstraintExecution.isSuccess()) {
                    validation
                        .getExecution()
                        .selectedGroup()
                        .success();
                }
                else {
                    validation
                        .getExecution()
                        .selectedGroup()
                        .fail();
                }
            }
            
            /* Optional validations 2 */
            validation.getExecution().selectGroup("Optional validations 2");
            if(!instance.getName().endsWith("@baz.qux")) {
                validation
                    .getExecution()
                    .selectedGroup()
                    .skip();
            }
            else {
                AbstractConstraintExecution optionalConstraintExecution = Constraints.map(String.class, 
                        name -> name==null ? 0 : name.length(), 
                            isGreaterThan(20)
                                .and(isLessThan(30)))                    
                        .test(instance.getName());
                Validation.ConstraintStatementExecution optionalStatement = Validation.executedStatement("Valid user name", instance, optionalConstraintExecution);
                if(optionalConstraintExecution.isFail()) {
                    validation.addConstraintViolation(new ConstraintViolation(ageValidationStatement.getName(), 
                            "Value (" + instance.getName() + ") is not a valid user name"));
                }
                validation.getExecution().addStatementExecution(optionalStatement);
                if(optionalConstraintExecution.isSuccess()) {
                    validation
                        .getExecution()
                        .selectedGroup()
                        .success();
                }
                else {
                    validation
                        .getExecution()
                        .selectedGroup()
                        .fail();
                }
            }
            
            if(validation.getConstraintViolations().isEmpty()) {
                validation.success();
            }
            else {
                validation.fail();
            }
            
            return validation;
        }
    }
}
