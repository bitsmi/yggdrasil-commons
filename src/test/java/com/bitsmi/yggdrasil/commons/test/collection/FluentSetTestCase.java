package com.bitsmi.yggdrasil.commons.test.collection;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.Comparator;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.TreeSet;

import org.junit.jupiter.api.Test;

import com.bitsmi.yggdrasil.commons.collection.FluentCollection;
import com.bitsmi.yggdrasil.commons.collection.FluentSet;

/**
 * {@link FluentSet} specific operations tests
 * For common {@link FluentCollection} operations see below
 * @author Antonio Archilla
 * @see FluentCollectionTestCase
 */
public class FluentSetTestCase
{
    @Test
    public void newHashSetAddTest()
    {
        HashSet<String> set = FluentSet.<String>newHashSet()
                .add("VALUE1")
                .add("VALUE2")
                .add("VALUE3")
                .get();
        
        assertThat(set).hasSize(3)
                .contains("VALUE1", "VALUE2", "VALUE3");
    }
    
    @Test
    public void newLinkedHashSetAddTest()
    {
        LinkedHashSet<String> set = FluentSet.<String>newLinkedHashSet()
                .add("VALUE1")
                .add("VALUE2")
                .add("VALUE3")
                .get();
        
        assertThat(set).hasSize(3)
                .containsExactly("VALUE1", "VALUE2", "VALUE3");
    }
    
    @Test
    public void newTreeSetAddTest()
    {
        TreeSet<String> set = FluentSet.<String>newTreeSet()
                .add("VALUE1")
                .add("VALUE3")
                .add("VALUE2")
                .get();
        
        assertThat(set).hasSize(3)
                // Ordered
                .containsExactly("VALUE1", "VALUE2", "VALUE3");
    }
    
    @Test
    public void newLinkedTreeSetComparatorTest()
    {
        TreeSet<String> set = FluentSet.<String>newTreeSet(Comparator.reverseOrder())
                .add("VALUE1")
                .add("VALUE3")
                .add("VALUE2")
                .get();
        
        assertThat(set).hasSize(3)
                // Reverse order
                .containsExactly("VALUE3", "VALUE2", "VALUE1");
    }
}
