package com.bitsmi.yggdrasil.commons.test.util;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowable;

import java.util.UUID;
import java.util.concurrent.atomic.AtomicInteger;

import org.assertj.core.api.Condition;
import org.junit.jupiter.api.Test;

import com.bitsmi.yggdrasil.commons.exception.CodedException;
import com.bitsmi.yggdrasil.commons.exception.CodedMessage;
import com.bitsmi.yggdrasil.commons.util.Lazy;
import com.bitsmi.yggdrasil.commons.util.UncheckedFunctionals;

public class LazyTestCase 
{
	@Test
	public void lazyTest()
	{
		Lazy<String> lazy = Lazy.of(() -> UUID.randomUUID().toString());
		
		assertThat(lazy).is(new Condition<>(e -> !e.isEvaluated(), "Lazy value is already evaluated"));
		String value = lazy.get();
		assertThat(value).isNotNull();
		assertThat(lazy)
			.is(new Condition<>(e -> value.equals(e.get()), "Lazy value is not equals to previous value"))
			.is(new Condition<>(e -> e.isEvaluated(), "Lazy value is not evaluated"));
	}
	
	@Test
	public void lazyOptionalValueTest()
	{
		Lazy<String> lazy = Lazy.of(() -> "Value");
		
		assertThat(lazy).is(new Condition<>(e -> !e.isEvaluated(), "Lazy value is already evaluated"));
		String value = lazy.get();
		assertThat(value).isNotNull();
		assertThat(lazy)
			.is(new Condition<>(e -> e.optional().orElse("Alternative").equals("Value"), "Lazy value is null"))
			.is(new Condition<>(e -> e.isEvaluated(), "Lazy value is not evaluated"));
	}
	
	@Test
	public void lazyOptionalNullValueTest()
	{
		Lazy<String> lazy = Lazy.of(() -> null);
		
		assertThat(lazy).is(new Condition<>(e -> !e.isEvaluated(), "Lazy value is already evaluated"));
		String value = lazy.get();
		assertThat(value).isNull();
		assertThat(lazy)
			.is(new Condition<>(e -> e.optional().orElse("Alternative").equals("Alternative"), "Lazy value is not null"))
			.is(new Condition<>(e -> e.isEvaluated(), "Lazy value is not evaluated"));
	}
	
	@Test
	public void lazyMapTest()
	{
	    Lazy<String> lazy = Lazy.of(() -> "Value");
	    Lazy<Integer> mappedLazy = lazy.map(val -> val.length());
	    
	    assertThat(lazy.isEvaluated()).isFalse();
	    assertThat(lazy.get()).isEqualTo("Value");
	    assertThat(lazy.isEvaluated()).isTrue();
	    
	    assertThat(mappedLazy.isEvaluated()).isFalse();
        assertThat(mappedLazy.get()).isEqualTo(5);
        assertThat(mappedLazy.isEvaluated()).isTrue();
	}
	
	@Test
    public void lazyMapRefreshTest()
    {
	    AtomicInteger counter = new AtomicInteger(1);
        Lazy<Integer> lazy = Lazy.of(() -> 1_000 + counter.getAndAdd(1));
        Lazy<Integer> mappedLazy = lazy.map(val -> val + 100);
        
        assertThat(lazy.isEvaluated()).isFalse();
        
        Integer initialValue = lazy.get();
        Integer refreshedValue = lazy.refresh().get();
                
        assertThat(refreshedValue).isNotEqualTo(initialValue);
        assertThat(lazy.isEvaluated()).isTrue();
        
        assertThat(mappedLazy.isEvaluated()).isFalse();
        assertThat(mappedLazy.get()).isEqualTo(refreshedValue + 100);
        assertThat(mappedLazy.isEvaluated()).isTrue();
    }
	
	@Test
	public void lazyExceptionHandlingTest()
	{
	    Lazy<String> lazy = Lazy.of(UncheckedFunctionals.uncheckedSupplier(() -> { 
	            throw new CodedException(new CodedMessage()
	                    .setCode("CODE-1")
	                    .setMessage("Error message")); 
	    }));
	    
	    Throwable t = catchThrowable(() -> {
	        lazy.get();
	    });
	    
	    assertThat(t)
	            .isInstanceOf(CodedException.class)
	            .extracting(e -> ((CodedException)e).getErrorCode(), e -> e.getMessage())
	            .containsExactly("CODE-1", "Error message"); 
	}
	
	@Test
    public void lazyExceptionHandlingTest2()
    {
        Lazy<String> lazy = Lazy.ofThrowing(() -> { 
                throw new CodedException(new CodedMessage()
                        .setCode("CODE-1")
                        .setMessage("Error message")); 
        });
        
        Throwable t = catchThrowable(() -> {
            lazy.get();
        });
        
        assertThat(t)
                .isInstanceOf(CodedException.class)
                .extracting(e -> ((CodedException)e).getErrorCode(), e -> e.getMessage())
                .containsExactly("CODE-1", "Error message"); 
    }
}
