package com.bitsmi.yggdrasil.commons.test.util;

import static org.assertj.core.api.Assertions.assertThat;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Parameter;
import java.util.List;
import java.util.Map;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import com.bitsmi.yggdrasil.commons.test.util.support.PojoWithValues;
import com.bitsmi.yggdrasil.commons.util.ReflectionUtils;
import com.bitsmi.yggdrasil.commons.util.Value;

public class ReflectionUtilsTestCase 
{
	/*-------------------------------*
	 * GENERICS INTROSPECTION
	 *-------------------------------*/
	@Test
	@DisplayName("Parameterized method parameter test")
	public void genericsIntrostectionTest1() throws Exception
	{
		Method method = GenericsTestClass.class.getMethod("methodWithGenericParam", List.class);
		boolean parameterized = ReflectionUtils.isParameterizedMethodParameter(method.getParameters()[0]);
		List<Class<?>> types = ReflectionUtils.extractMethodParameterTypeArguments(method.getParameters()[0]);
		
		assertThat(parameterized).isTrue();
		assertThat(types).hasSize(1).contains(String.class);
	}
	
	@Test
	@DisplayName("Parameterized method parameter multiple generics test")
	public void genericsIntrostectionTest2() throws Exception
	{
		Method method = GenericsTestClass.class.getMethod("methodWithMultipleGenericParams", Map.class);
		boolean parameterized = ReflectionUtils.isParameterizedMethodParameter(method.getParameters()[0]);
		List<Class<?>> types = ReflectionUtils.extractMethodParameterTypeArguments(method.getParameters()[0]);
		
		assertThat(parameterized).isTrue();
		assertThat(types).hasSize(2).contains(String.class, Long.class);
	}
	
	@Test
	@DisplayName("Non parameterized method parameter test")
	public void genericsIntrostectionTest3() throws Exception
	{
		Method method = GenericsTestClass.class.getMethod("methodWithSimpleParams", String.class);
		boolean parameterized = ReflectionUtils.isParameterizedMethodParameter(method.getParameters()[0]);
		List<Class<?>> types = ReflectionUtils.extractMethodParameterTypeArguments(method.getParameters()[0]);
		
		assertThat(parameterized).isFalse();
		assertThat(types).isEmpty();
	}
	
	@Test
	@DisplayName("Non defined parameterized method parameter test")
	public void genericsIntrostectionTest4() throws Exception
	{
		Method method = GenericsTestClass.class.getMethod("methodWithNonDefinedParameterizedParam", List.class);
		boolean parameterized = ReflectionUtils.isParameterizedMethodParameter(method.getParameters()[0]);
		List<Class<?>> types = ReflectionUtils.extractMethodParameterTypeArguments(method.getParameters()[0]);
		
		assertThat(parameterized).isTrue();
		assertThat(types).isEmpty();
	}
	
	@Test
	@DisplayName("Non defined generic on parameterized method parameter test")
	public void genericsIntrostectionTest5() throws Exception
	{
		Method method = GenericsTestClass.class.getMethod("methodWithNonDefinedParameterizedParam", List.class);
		boolean parameterized = ReflectionUtils.isParameterizedMethodParameter(method.getParameters()[0]);
		List<Class<?>> types = ReflectionUtils.extractMethodParameterTypeArguments(method.getParameters()[0]);
		
		assertThat(parameterized).isTrue();
		assertThat(types).isEmpty();
	}
	
	@Test
	@DisplayName("Non defined generic method parameter test")
	public void genericsIntrostectionTest6() throws Exception
	{
		Method method = GenericsTestClass.class.getMethod("methodWithNonDefinedGenericParam", Object.class);
		boolean parameterized = ReflectionUtils.isParameterizedMethodParameter(method.getParameters()[0]);
		boolean nonDefined = ReflectionUtils.isGenericMethodParameter(method.getParameters()[0]);
		List<Class<?>> types = ReflectionUtils.extractMethodParameterTypeArguments(method.getParameters()[0]);
		
		assertThat(parameterized).isFalse();
		assertThat(nonDefined).isTrue();
		assertThat(types).isEmpty();
	}
	
	@Test
	@DisplayName("Generic classes test")
	public void genericsIntrospectionTest7() throws Exception
	{
		boolean parameterizedGenericClass = ReflectionUtils.isParameterizedClass(GenericsTestClass.class);
		List<Class<?>> parameterizedGenericClassTypes = ReflectionUtils.extractClassTypeArguments(GenericsTestClass.class);
		boolean parameterizedConcreteClass = ReflectionUtils.isParameterizedClass(ConcreteTestClass.class);
		List<Class<?>> parameterizedConcreteClassTypes = ReflectionUtils.extractClassTypeArguments(ConcreteTestClass.class);
		boolean parameterizedSimpleClass = ReflectionUtils.isParameterizedClass(SimpleTestClass.class);
		List<Class<?>> parameterizedSimpleClassTypes = ReflectionUtils.extractClassTypeArguments(SimpleTestClass.class);
		
		assertThat(parameterizedGenericClass).isTrue();
		assertThat(parameterizedGenericClassTypes).hasSize(0);
		assertThat(parameterizedConcreteClass).isTrue();
		assertThat(parameterizedConcreteClassTypes).hasSize(1);
		assertThat(parameterizedSimpleClass).isFalse();
		assertThat(parameterizedSimpleClassTypes).hasSize(0);
	}
	
	/*-------------------------------*
	 * ANNOTATION UTILS
	 *-------------------------------*/
	@Test
	public void scanAnnotatedFieldsWithValuesTest()
	{
		List<Field> fields = ReflectionUtils.scanAnnotatedFields(PojoWithValues.class, Value.class);
		assertThat(fields)
				.hasSize(4)
				.allSatisfy(f -> {
					assertThat(f.isAnnotationPresent(Value.class)).isTrue();
				});
	}
	
	@Test
	public void scanAnnotatedFieldsWithoutValuesTest()
	{
		List<Field> fields = ReflectionUtils.scanAnnotatedFields(PojoWithoutValues.class, Value.class);
		assertThat(fields)
				.isEmpty();
	}
	
	@Test
	public void scannAnnotatedParametersTest() throws Exception
	{
	    Method method = PojoWithSomeAnnotatedMethodParameters.class.getMethod("setParam34", String.class, String.class);
	    List<Parameter> parameters = ReflectionUtils.scanAnnotatedParameters(method, Value.class);
	    
	    assertThat(parameters).hasSize(1);
	    assertThat(parameters.get(0)).satisfies(p -> {
	        Value value = p.getAnnotation(Value.class);
	        assertThat(value).isNotNull();
	        assertThat(value.value()).isEqualTo("key3");
	        assertThat(p.getType()).isEqualTo(String.class);
	    });
	}
	
	@Test
	public void checkAllMethodParametersAnnotatedTest()
	{
		List<Method> methods = ReflectionUtils.scanMethodsWithAnnotatedParameters(PojoWithAnnotatedMethodParameters.class, Value.class);
		
		assertThat(methods)
				.hasSize(3)
				.allSatisfy(m -> {
					assertThat(m.getName()).isIn("setParam1", "setParam2", "setParam34");
					assertThat(ReflectionUtils.assertAllParametersAnnotated(m, Value.class)).isTrue();
				});    	
	}
	
	@Test
	public void checkSomeMethodParametersAnnotatedTest()
	{
		List<Method> methods = ReflectionUtils.scanMethodsWithAnnotatedParameters(PojoWithSomeAnnotatedMethodParameters.class, Value.class);
		
		assertThat(methods)
				.hasSize(3)
				.allSatisfy(m -> {
					assertThat(m.getName()).isIn("setParam1", "setParam2", "setParam34");
					if("setParam34".equals(m.getName())) {
						assertThat(ReflectionUtils.assertAllParametersAnnotated(m, Value.class)).isFalse();
					}
					else {
						assertThat(ReflectionUtils.assertAllParametersAnnotated(m, Value.class)).isTrue();
					}
				});    	
	}
	
	/*--------------------------------------*
	 * LOCAL SUPPORT CLASSES
	 *--------------------------------------*/
	class GenericsTestClass <T>
	{
		public void methodWithGenericParam(List<String> param)
		{
			
		}
		
		public void methodWithMultipleGenericParams(Map<String, Long> param)
		{
			
		}
		
		public void methodWithSimpleParams(String param)
		{
			
		}
		
		public void methodWithNonDefinedParameterizedParam(List<T> param)
		{
			
		}
		
		public void methodWithNonDefinedGenericParam(T param)
		{
			
		}
	}
	
	class ConcreteTestClass extends GenericsTestClass<String>
	{
		
	}
	
	class SimpleTestClass
	{
		public void methodWithSimpleParams(String param)
		{
			
		}
	}
	
	class PojoWithoutValues 
	{
		private String param1;
		private String param2;
		private String param3;
		private String param4;

		public String getParam1() 
		{
			return param1;
		}

		public String getParam2() 
		{
			return param2;
		}

		public String getParam3() 
		{
			return param3;
		}
		
		public String getParam4() 
		{
			return param4;
		}
	}

	class PojoWithAnnotatedMethodParameters 
	{
		private String param1;
		private String param2;
		private String param3;
		private String param4;
		
		public String getParam1() 
		{
			return param1;
		}
		
		public void setParam1(@Value("key1") String param1) 
		{
			this.param1 = param1;
		}
		
		public String getParam2() 
		{
			return param2;
		}
		
		public void setParam2(@Value("key2") String param2) 
		{
			this.param2 = param2;
		}
		
		public String getParam3() 
		{
			return param3;
		}
		
		public String getParam4() 
		{
			return param4;
		}
		
		public void setParam34(@Value("key3") String param3, @Value("key4") String param4) 
		{
			this.param3 = param3;
			this.param4 = param4;
		}
	}

	class PojoWithSomeAnnotatedMethodParameters 
	{
		private String param1;
		private String param2;
		private String param3;
		private String param4;
		
		public String getParam1() 
		{
			return param1;
		}
		
		public void setParam1(@Value("key1") String param1) 
		{
			this.param1 = param1;
		}
		
		public String getParam2() 
		{
			return param2;
		}
		
		public void setParam2(@Value("key2") String param2) 
		{
			this.param2 = param2;
		}
		
		public String getParam3() 
		{
			return param3;
		}
		
		public String getParam4() 
		{
			return param4;
		}
		
		public void setParam34(@Value("key3") String param3, String param4) 
		{
			this.param3 = param3;
			this.param4 = param4;
		}
	}
}
