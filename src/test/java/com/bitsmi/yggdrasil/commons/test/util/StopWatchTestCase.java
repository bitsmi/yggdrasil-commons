package com.bitsmi.yggdrasil.commons.test.util;

import static org.assertj.core.api.Assertions.assertThat;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

import com.bitsmi.yggdrasil.commons.util.StopWatch;

public class StopWatchTestCase 
{
    @Test
    public void oneTaskTest() throws Exception
    {
        StopWatch sw = new StopWatch();
        sw.start("TASK_1");
        Thread.sleep(1000);
        sw.stop();
        long elapsedTime = sw.getTotalTimeMillis();
        
        assertThat(elapsedTime).isGreaterThanOrEqualTo(1000);
    }
    
    @Test
    public void unnamedTask() throws Exception
    {
        StopWatch sw = new StopWatch();
        sw.start();
        Thread.sleep(1000);
        sw.stop();
        long elapsedTime = sw.getTotalTimeMillis();
        
        // Create as array to make it editable from inside lambda expressions
        final int[] counter = new int[] {0};
        sw.getTasksInfo((name, taskMillis) -> {
            int currentValue = counter[0]; 
            counter[0] = currentValue + 1;
            
            assertThat(StopWatch.UNNAMED_TASK.equals(name)).isTrue();
            assertThat(taskMillis)
                    .isGreaterThanOrEqualTo(1000)
                    .isLessThan(2000);            
        });
        
        assertThat(counter[0]).isEqualTo(1);
        assertThat(elapsedTime).isGreaterThanOrEqualTo(1000);
    }
    
    @Test
    public void multipleTaskTest() throws Exception
    {
        StopWatch sw = new StopWatch();
        sw.start("TASK_1");
        Thread.sleep(1000);
        sw.stop();
        sw.start("TASK_2");
        Thread.sleep(1000);
        sw.stop();
        long elapsedTime = sw.getTotalTimeMillis();
        
        assertThat(elapsedTime).isGreaterThanOrEqualTo(2000);
    }
    
    @Test
    public void lastTaskTest() throws Exception
    {
        StopWatch sw = new StopWatch();
        sw.start("TASK_1");
        Thread.sleep(1000);
        sw.stop();
        sw.start("TASK_2");
        Thread.sleep(1000);
        sw.stop();
        long taskTime = sw.getLastTaskTimeMillis();
        long elapsedTime = sw.getTotalTimeMillis();
        
        assertThat(taskTime)
                .isGreaterThanOrEqualTo(1000)
                .isLessThan(2000);     
        assertThat(elapsedTime).isGreaterThanOrEqualTo(2000);
    }
    
    @Test
    public void splitOneTaskTest() throws Exception
    {
        StopWatch sw = new StopWatch();
        sw.start("TASK_1");
        Thread.sleep(1000);
        long splitTime = sw.splitMillis();
        Thread.sleep(1000);
        sw.stop();
        long elapsedTime = sw.getTotalTimeMillis();
        
        assertThat(splitTime)
                .isGreaterThanOrEqualTo(1000)
                .isLessThan(2000);     
        assertThat(elapsedTime).isGreaterThanOrEqualTo(2000);
    }
    
    @Test
    public void splitMultipleTaskTest() throws Exception
    {
        StopWatch sw = new StopWatch();
        sw.start("TASK_1");
        Thread.sleep(1000);
        sw.stop();
        sw.start("TASK_2");
        Thread.sleep(1000);
        long splitTime = sw.splitCurrentTaskMillis();
        Thread.sleep(1000);
        sw.stop();
        long elapsedTime = sw.getTotalTimeMillis();
        
        assertThat(splitTime)
                .isGreaterThanOrEqualTo(1000)
                .isLessThan(2000);     
        assertThat(elapsedTime).isGreaterThanOrEqualTo(3000);        
    }
    
    @Test
    public void taskInfoConsumerTest() throws Exception
    {
        StopWatch sw = new StopWatch();
        sw.start("TASK_1");
        Thread.sleep(1000);
        sw.stop();
        sw.start("TASK_2");
        Thread.sleep(1000);
        sw.stop();
        
        // Create as array to make it editable from inside lambda expressions
        final int[] counter = new int[] {0};
        sw.getTasksInfo((name, taskMillis) -> {
            int currentValue = counter[0]; 
            counter[0] = currentValue + 1;
            
            assertThat(name).isIn("TASK_1", "TASK_2");
            assertThat(taskMillis)
                    .isGreaterThanOrEqualTo(1000)
                    .isLessThan(2000);            
        });
        
        assertThat(counter[0]).isEqualTo(2);
    }
    
    @Test
    public void stopUnstartedTaskTest()
    {
        Throwable t = Assertions.catchThrowable(() -> {
            StopWatch sw = new StopWatch();
            sw.stop();
        });
        
        assertThat(t).isNotNull().isInstanceOf(IllegalStateException.class);
    }
    
    @Test
    public void splitUnstartedTaskTest()
    {
        Throwable t = Assertions.catchThrowable(() -> {
            StopWatch sw = new StopWatch();
            sw.splitMillis();
        });
        
        assertThat(t).isNotNull().isInstanceOf(IllegalStateException.class);
    }
    
    @Test
    public void splitStoppedTaskTest()
    {
        Throwable t = Assertions.catchThrowable(() -> {
            StopWatch sw = new StopWatch();
            sw.start("TASK_1");
            sw.stop();
            sw.splitMillis();
        });
        
        assertThat(t).isNotNull().isInstanceOf(IllegalStateException.class);
    }
    
    @Test
    public void alreadyStartedTaskTest()
    {
        Throwable t = Assertions.catchThrowable(() -> {
            StopWatch sw = new StopWatch();
            sw.start("TASK_1");
            sw.start("TASK_2");
        });
        
        assertThat(t).isNotNull().isInstanceOf(IllegalStateException.class);
    }
    
    @Test
    public void nonStopedLastTaskTimeTest()
    {
        Throwable t = Assertions.catchThrowable(() -> {
            StopWatch sw = new StopWatch();
            sw.start("TASK_1");
            sw.getLastTaskTimeMillis();
        });
        
        assertThat(t).isNotNull().isInstanceOf(IllegalStateException.class);
    }	
}
