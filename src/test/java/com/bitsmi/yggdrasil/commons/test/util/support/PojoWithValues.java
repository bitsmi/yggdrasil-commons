package com.bitsmi.yggdrasil.commons.test.util.support;

import com.bitsmi.yggdrasil.commons.util.Value;

public class PojoWithValues 
{
	@Value("key1")
	private String param1;
	
	@Value(property="key2", defaultValue="default value")
	private String param2;
	
	@Value(property="${key3}")
	private String param3;
	
	@Value(property="pre_${key1}_ext")
	private String param4;

	private String param5;
	private String param6;
	private String param7;
	private String param8;
	private String param9;
	
	public String getParam1() 
	{
		return param1;
	}

	public String getParam2() 
	{
		return param2;
	}

	public String getParam3() 
	{
		return param3;
	}
	
	public String getParam4() 
	{
		return param4;
	}

	public String getParam5() 
	{
		return param5;
	}

	@Value("key5")
	public void setParam5(String param5) 
	{
		this.param5 = param5;
	}

	public String getParam6() 
	{
		return param6;
	}
	
	public void setParam6(@Value("key6")String param6) 
	{
		this.param6 = param6;
	}

	public String getParam7() 
	{
		return param7;
	}

	public String getParam8() 
	{
		return param8;
	}

	public void setParam7And8(@Value("key7")String param7, @Value("key8")String param8) 
	{
		this.param7 = param7;
		this.param8 = param8;
	}
	
	public String getParam9()
	{
		return param9;
	}
	
	@Value("key9")
	private void setParam9(String param9)
	{
		this.param9 = param9;
	}
}
