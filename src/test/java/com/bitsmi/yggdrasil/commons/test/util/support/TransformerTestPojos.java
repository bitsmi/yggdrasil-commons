package com.bitsmi.yggdrasil.commons.test.util.support;

import java.util.function.Function;

import com.bitsmi.yggdrasil.commons.util.ITransformable;
import com.bitsmi.yggdrasil.commons.util.ITransformer;

public class TransformerTestPojos
{
    public static class TransformableSourcePojo implements ITransformable
    {
        private String sourceValue1;
        private String sourceValue2;
        private SourceComplexPojo sourceComplexValue; 
        
        public String getSourceValue1() 
        {
            return sourceValue1;
        }

        public TransformableSourcePojo setSourceValue1(String sourceValue1) 
        {
            this.sourceValue1 = sourceValue1;
            return this;
        }

        public String getSourceValue2() 
        {
            return sourceValue2;
        }

        public TransformableSourcePojo setSourceValue2(String sourceValue2) 
        {
            this.sourceValue2 = sourceValue2;
            return this;
        }
        
        public SourceComplexPojo getSourceComplexValue() 
        {
            return sourceComplexValue;
        }

        public TransformableSourcePojo setSourceComplexValue(SourceComplexPojo sourceComplexValue) 
        {
            this.sourceComplexValue = sourceComplexValue;
            return this;
        }

        @Override
        public ITransformer<TransformableSourcePojo> transformed() 
        {
            return this::transform;
        }
        
        private <R> R transform(Function<? super TransformableSourcePojo, ? extends R> f) 
        {
            return f.apply(this);
        }
    }
    
    public static class SourceComplexPojo
    {
        private String sourceValue1;
        private String sourceValue2;
        
        public String getSourceValue1() 
        {
            return sourceValue1;
        }
        
        public SourceComplexPojo setSourceValue1(String sourceValue1) 
        {
            this.sourceValue1 = sourceValue1;
            return this;
        }
        
        public String getSourceValue2() 
        {
            return sourceValue2;
        }

        public SourceComplexPojo setSourceValue2(String sourceValue2) 
        {
            this.sourceValue2 = sourceValue2;
            return this;
        }
    }
    
    public static class TargetPojo
    {
        private String targetValue1;
        private String targetValue2;
        private TargetComplexPojo targetComplexValue;
        
        public String getTargetValue1() 
        {
            return targetValue1;
        }
        
        public TargetPojo setTargetValue1(String targetValue1) 
        {
            this.targetValue1 = targetValue1;
            return this;
        }
        
        public String getTargetValue2() 
        {
            return targetValue2;
        }
        
        public TargetPojo setTargetValue2(String targetValue2) 
        {
            this.targetValue2 = targetValue2;
            return this;
        }

        public TargetComplexPojo getTargetComplexValue() {
            return targetComplexValue;
        }

        public TargetPojo setTargetComplexValue(TargetComplexPojo targetComplexPojo) 
        {
            this.targetComplexValue = targetComplexPojo;
            return this;
        }
    }
    
    public static class TargetComplexPojo
    {
        private String targetValue1;
        private String targetValue2;
        
        public String getTargetValue1() 
        {
            return targetValue1;
        }
        
        public TargetComplexPojo setTargetValue1(String targetValue1) 
        {
            this.targetValue1 = targetValue1;
            return this;
        }
        
        public String getTargetValue2() 
        {
            return targetValue2;
        }

        public TargetComplexPojo setTargetValue2(String targetValue2) 
        {
            this.targetValue2 = targetValue2;
            return this;
        }
    }
    
    public static class SourcePojoTransformerFactory
    {
        public static Function<? super TransformableSourcePojo, ? extends TargetPojo> toTargetPojo()
        {
            return src -> {
                return new TargetPojo()
                        .setTargetValue1(src.getSourceValue1())
                        .setTargetValue2(src.getSourceValue2());
            };
        }       
    }
}
