package com.bitsmi.yggdrasil.commons.test.collection;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Stream;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import com.bitsmi.yggdrasil.commons.collection.FluentCollection;
import com.bitsmi.yggdrasil.commons.collection.FluentList;

/**
 * {@link FluentList} specific operations tests
 * For common {@link FluentCollection} operations see below
 * @author Antonio Archilla
 * @see FluentCollectionTestCase
 */
public class FluentListTestCase
{
    @Test
    public void newArrayListAddTest()
    {
        ArrayList<String> list = FluentList.<String>newArrayList()
                .add("VALUE1")
                .add("VALUE2")
                .add("VALUE3")
                .add(2, "VALUE2.5")
                .get();
        
        assertThat(list).hasSize(4)
                .containsExactly("VALUE1", "VALUE2", "VALUE2.5", "VALUE3");
    }
    
    @Test
    public void newLinkedListAddTest()
    {
        LinkedList<String> list = FluentList.<String>newLinkedList()
                .add("VALUE1")
                .add("VALUE2")
                .add("VALUE3")
                .add(2, "VALUE2.5")
                .get();
        
        assertThat(list).hasSize(4)
                .containsExactly("VALUE1", "VALUE2", "VALUE2.5", "VALUE3");
    }
    
    /**
     * Add FluentList concrete add operations to base FluentCollection add test. For base cases see below
     * @param testName
     * @param list
     * @see FluentCollectionTestCase#addTest(String, boolean, java.util.Collection)
     */
    @ParameterizedTest(name = "Test {0}")
    @MethodSource("createListValues")
    public void ofListAddTest(String testName, List<String> list)
    {
        List<String> resultList = FluentList.of(list)
                .clear()
                .add("VALUE1")
                .add("VALUE2")
                .add("VALUE3")
                .add(2, "VALUE2.5")
                .addAll(Arrays.asList("VALUE4", "VALUE5"))
                .addAll(1, Arrays.asList("VALUE1.1", "VALUE1.2"))
                .get();
        
        assertThat(resultList).hasSize(8)
                .containsExactly("VALUE1", "VALUE1.1", "VALUE1.2", "VALUE2", "VALUE2.5", "VALUE3", "VALUE4", "VALUE5");
    }
    
    @ParameterizedTest(name = "Test {0}")
    @MethodSource("createListValues")
    public void replaceAllTest(String testName, List<String> list)
    {
        List<String> resultList = FluentList.of(list)
                .clear()
                .add("VALUE1")
                .add("VALUE2")
                .add("VALUE3")
                .replaceAll(s -> s + " MOD")
                .get();
        
        assertThat(resultList).hasSize(3)
                .containsExactly("VALUE1 MOD", "VALUE2 MOD", "VALUE3 MOD");
    }
    
    @ParameterizedTest(name = "Test {0}")
    @MethodSource("createListValues")
    public void sortTest(String testName, List<String> list)
    {
        List<String> resultList = FluentList.of(list)
                .clear()
                .add("VALUE2.5")
                .add("VALUE2")
                .add("VALUE1")
                .add("VALUE3")
                .addAll(Arrays.asList("VALUE4", "VALUE5"))
                .addAll(Arrays.asList("VALUE1.1", "VALUE1.2"))
                .sort(Comparator.naturalOrder())
                .get();
        
        assertThat(resultList).hasSize(8)
                .containsExactly("VALUE1", "VALUE1.1", "VALUE1.2", "VALUE2", "VALUE2.5", "VALUE3", "VALUE4", "VALUE5");
    }
    
    /*--------------------------------*
     * SUPPORT METHODS AND CLASSES
     * ParameterizedTest's method sources must be public to be accessible by java module system
     *--------------------------------*/
    public static Stream<Object> createListValues()
    {
        final ArrayList<String> nonEmptyList = new ArrayList<>();
        nonEmptyList.add("INITIAL VALUE");
        
        return Stream.of(new Object[] {"ArrayList", 
                    new ArrayList<String>()
                },
                new Object[] {"LinkedList", 
                    new LinkedList<String>()
                },
                new Object[] {"Non Empty List", 
                    nonEmptyList
                });
    }
}
