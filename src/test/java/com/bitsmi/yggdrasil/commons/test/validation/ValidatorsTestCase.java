package com.bitsmi.yggdrasil.commons.test.validation;

import static com.bitsmi.yggdrasil.commons.validation.Constraints.isEmail;
import static com.bitsmi.yggdrasil.commons.validation.Constraints.isGreaterThan;
import static com.bitsmi.yggdrasil.commons.validation.Constraints.isLessThan;
import static com.bitsmi.yggdrasil.commons.validation.Constraints.isNotNull;
import static com.bitsmi.yggdrasil.commons.validation.Constraints.map;
import static org.assertj.core.api.Assertions.assertThat;

import java.util.stream.Stream;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import com.bitsmi.yggdrasil.commons.test.validation.support.TestBean;
import com.bitsmi.yggdrasil.commons.validation.ConstraintViolation;
import com.bitsmi.yggdrasil.commons.validation.Constraints;
import com.bitsmi.yggdrasil.commons.validation.Constraints.IConstraint;
import com.bitsmi.yggdrasil.commons.validation.Validation;
import com.bitsmi.yggdrasil.commons.validation.Validation.ValidationExecution;
import com.bitsmi.yggdrasil.commons.validation.Validators;
import com.bitsmi.yggdrasil.commons.validation.Validators.ValidationMode;

public class ValidatorsTestCase
{
    @ParameterizedTest
    @MethodSource("provideConstraintExpressionTestValues")
    @DisplayName("Constraint expression Test")
    public void constraintExpressionTest1(IConstraint<?> constraint, String controlExpression)
    {
        String expression = Constraints.explain(constraint);
        
        assertThat(expression).isEqualTo(controlExpression);
    }    
    
    @Test
    @DisplayName("Simple validation success Test")
    public void simpleTest1()
    {
        TestBean instance = new TestBean()
                .setName("john.random@foo.bar")
                .setAge(60);
        
        Validation validation = Validators.<TestBean>beanValidator()
        		.constraint("Valid user name", 
        			"Value ({0}) is not a valid user name", 
        			TestBean::getName, 
        			isNotNull(String.class)
                    	.and(isEmail())
                )
                .constraint("Valid user age",
                	"Value ({0}) is not a valid user age",
                	TestBean::getAge, 
                	isNotNull(Integer.class)
                    	.and(isLessThan(20)
                    		.or(isGreaterThan(50)))
                )
                .build()
                .validate(instance);
        
        assertThat(validation.isSuccess()).isTrue();
        assertThat(validation.isFailed()).isFalse();
        assertThat(validation.isSkipped()).isFalse();
        assertThat(validation.getConstraintViolations())
                .hasSize(0);
    }
    
    @Test
    @DisplayName("Mapped validation success Test")
    public void wrappedTest1()
    {
        TestBean instance = new TestBean()
                .setName("john.random@foo.bar")
                .setAge(60);
        
        Validation validation = Validators.<TestBean>beanValidator()
        		.constraint("Valid user name", 
        			"Value ({0}) is not a valid user name", 
        			TestBean::getName, 
        			isNotNull(String.class)
                    	.and(isEmail())
                    	.and(name -> name.length(), 
                    		isGreaterThan(10)
                    			.and(isLessThan(100))
                    	)
                )
                .constraint("Valid user age",
                	"Value ({0}) is not a valid user age",
                	TestBean::getAge, 
                	isNotNull(Integer.class)
                    	.and(isLessThan(20)
                    		.or(isGreaterThan(50)))
                )
                .build()
                .validate(instance);
        
        assertThat(validation.isSuccess()).isTrue();
        assertThat(validation.isFailed()).isFalse();
        assertThat(validation.isSkipped()).isFalse();
        assertThat(validation.getConstraintViolations())
                .hasSize(0);
    }
    
    @Test
    @DisplayName("Mapped validation as expression start Test")
    public void wrappedTest2()
    {
        TestBean instance = new TestBean()
                .setName("john.random@foo.bar")
                .setAge(60);
        
        Validation validation = Validators.<TestBean>beanValidator()
        		.constraint("Valid user name", 
        			"Value ({0}) is not a valid user name", 
        			TestBean::getName, 
                    map(String.class, 
                    	name -> name==null ? 0 : name.length(), 
                    	isGreaterThan(10)
                    		.and(isLessThan(100))
                    )
                    .and(isEmail())
                )
                .constraint("Valid user age",
                	"Value ({0}) is not a valid user age",
                	TestBean::getAge, 
                	isNotNull(Integer.class)
                    	.and(isLessThan(20)
                    		.or(isGreaterThan(50)))
                )
                .build()
                .validate(instance);
        
        assertThat(validation.isSuccess()).isTrue();
        assertThat(validation.isFailed()).isFalse();
        assertThat(validation.isSkipped()).isFalse();
        assertThat(validation.getConstraintViolations())
                .hasSize(0);
    }
    
    @Test
    @DisplayName("Mapped validation failed Test")
    public void wrappedTest3()
    {
        TestBean instance = new TestBean()
                .setName("john.random@foo.bar")
                .setAge(60);
        
        Validation validation = Validators.<TestBean>beanValidator()
        		.constraint("Valid user name", 
        			"Value ({0}) is not a valid user name", 
        			TestBean::getName, 
        			isNotNull(String.class)
                    	.and(isEmail())
                    	.and(name -> name.length(), 
                    		isGreaterThan(100)
                    			.and(isLessThan(200))
                    	)
                )
                .constraint("Valid user age",
                	"Value ({0}) is not a valid user age",
                	TestBean::getAge, 
                	isNotNull(Integer.class)
                    	.and(isLessThan(20)
                    		.or(isGreaterThan(50)))
                )
                .build()
                .validate(instance);
        
        assertThat(validation.isSuccess()).isFalse();
        assertThat(validation.isFailed()).isTrue();
        assertThat(validation.isSkipped()).isFalse();
        assertThat(validation.getConstraintViolations())
                .hasSize(1)
                .extracting(ConstraintViolation::getConstraintName, ConstraintViolation::getMessage)
                .containsOnly(Assertions.tuple("Valid user name", "Value (john.random@foo.bar) is not a valid user name"));
    }
    
    @Test
    @DisplayName("Simple validation OR failfast success Test")
    public void simpleTest2()
    {
        TestBean instance = new TestBean()
                .setName("john.random@foo.bar")
                .setAge(10);
        
        Validation validation = Validators.<TestBean>beanValidator()
        		.constraint("Valid user name", 
        			"Value ({0}) is not a valid user name", 
        			TestBean::getName, 
        			isNotNull(String.class)
                    	.and(isEmail())
                )
                .constraint("Valid user age",
                	"Value ({0}) is not a valid user age",
                	TestBean::getAge, 
                	isNotNull(Integer.class)
                    	.and(isLessThan(20)
                    		.or(isGreaterThan(50)))
                )
                .build()
                .validate(instance);
        
        assertThat(validation.isSuccess()).isTrue();
        assertThat(validation.isFailed()).isFalse();
        assertThat(validation.isSkipped()).isFalse();
        assertThat(validation.getConstraintViolations())
                .hasSize(0);
    }
    
    @Test
    @DisplayName("Simple validation failed Test")
    public void simpleTest3()
    {
        TestBean instance = new TestBean()
                .setName("john.random@foo.bar")
                .setAge(30);
        
        Validation validation = Validators.<TestBean>beanValidator()
        		.constraint("Valid user name", 
        			"Value ({0}) is not a valid user name", 
        			TestBean::getName, 
        			isNotNull(String.class)
                    	.and(isEmail())
                )
                .constraint("Valid user age",
                	"Value ({0}) is not a valid user age",
                	TestBean::getAge, 
                	isNotNull(Integer.class)
                    	.and(isLessThan(20)
                    		.or(isGreaterThan(50)))
                )
                .build()
                .validate(instance);
        
        assertThat(validation.isSuccess()).isFalse();
        assertThat(validation.isFailed()).isTrue();
        assertThat(validation.isSkipped()).isFalse();
        assertThat(validation.getConstraintViolations())
                .hasSize(1)
                .extracting(ConstraintViolation::getConstraintName, ConstraintViolation::getMessage)
                .containsOnly(Assertions.tuple("Valid user age", "Value (30) is not a valid user age"));
    }
    
    @Test
    @DisplayName("Validation modes fail fast Test")
    public void validationModesTest1()
    {
        // Nor name or age are valid values
        TestBean instance = new TestBean()
                .setName("Not an email")
                .setAge(40);
        
        Validation validation = Validators.<TestBean>beanValidator(ValidationMode.FAIL_FAST)
        		.constraint("Valid user name", 
        			"Value ({0}) is not a valid user name", 
        			TestBean::getName, 
        			isNotNull(String.class)
                    	.and(isEmail())
                )
                .constraint("Valid user age",
                	"Value ({0}) is not a valid user age",
                	TestBean::getAge, 
                	isNotNull(Integer.class)
                    	.and(isLessThan(20)
                    		.or(isGreaterThan(50)))
                )
                .build()
                .validate(instance);
        
        assertThat(validation.isSuccess()).isFalse();
        assertThat(validation.isFailed()).isTrue();
        assertThat(validation.isSkipped()).isFalse();
        assertThat(validation.getConstraintViolations())
                .hasSize(1)
                .extracting(ConstraintViolation::getConstraintName, ConstraintViolation::getMessage)
                .containsOnly(Assertions.tuple("Valid user name", "Value (Not an email) is not a valid user name"));
    }
    
    @Test
    @DisplayName("Validation modes validate all Test")
    public void validationModesTest2()
    {
        // Nor name or age are valid values
        TestBean instance = new TestBean()
                .setName("Not an email")
                .setAge(40);
        
        Validation validation = Validators.<TestBean>beanValidator(ValidationMode.ALL)
        		.constraint("Valid user name", 
        			"Value ({0}) is not a valid user name", 
        			TestBean::getName, 
        			isNotNull(String.class)
                    	.and(isEmail()))
                .constraint("Valid user age",
                	"Value ({0}) is not a valid user age",
                	TestBean::getAge, 
                	isNotNull(Integer.class)
                    	.and(isLessThan(20)
                    		.or(isGreaterThan(50))))
                .build()
                .validate(instance);
        
        assertThat(validation.isSuccess()).isFalse();
        assertThat(validation.isFailed()).isTrue();
        assertThat(validation.isSkipped()).isFalse();
        assertThat(validation.getConstraintViolations())
                .hasSize(2)
                .extracting(ConstraintViolation::getConstraintName, ConstraintViolation::getMessage)
                .containsOnly(Assertions.tuple("Valid user name", "Value (Not an email) is not a valid user name"),
            		Assertions.tuple("Valid user age", "Value (40) is not a valid user age"));
    }
    
    @Test
    @DisplayName("Validation execution Test")
    public void validationExecutionTest1()
    {
        // Nor name or age are valid values
        TestBean instance = new TestBean()
                .setName("john.random@foo.bar")
                .setAge(40);
        
        Validation validation = Validators.<TestBean>beanValidator(ValidationMode.ALL)
                .constraint("Valid user name", 
                    "Value ({0}) is not a valid user name", 
                    TestBean::getName, 
                    isNotNull(String.class)
                        .and(isEmail()))
                .constraint("Valid user age",
                    "Value ({0}) is not a valid user age",
                    TestBean::getAge, 
                    isNotNull(Integer.class)
                        .and(isLessThan(20)
                            .or(isGreaterThan(50))))
                .build()
                .validate(instance);
        
        ValidationExecution execution = validation.getExecution();
        assertThat(execution.getGroupExecution(Validation.EXECUTION_GROUP_DEFAULT).getStatementExecutions())
                .hasSize(2)
                .extracting(Validation.ConstraintStatementExecution::getResult)
                .containsExactly(Validation.Result.SUCCEEDED, Validation.Result.FAILED);
        // Succeeded
        Validation.ConstraintStatementExecution statementExecution = execution
                .getGroupExecution(Validation.EXECUTION_GROUP_DEFAULT)
                .getStatementExecutions()
                .get(0);
        assertThat(statementExecution.getValue()).isEqualTo("john.random@foo.bar");
        assertThat(statementExecution.getConstraintExecution())
                .asString()
                .isEqualTo("{Value = john.random@foo.bar} --> [([Value is not null]{SUCCEEDED}) AND ([Value is a valid email]{SUCCEEDED})]{SUCCEEDED}");
        // Failed
        statementExecution = execution.getGroupExecution(Validation.EXECUTION_GROUP_DEFAULT)
                .getStatementExecutions()
                .get(1);
        assertThat(statementExecution.getValue()).isEqualTo(40);
        assertThat(statementExecution.getConstraintExecution())
                .asString()
                .isEqualTo("{Value = 40} --> [([Value is not null]{SUCCEEDED}) AND ([([Value is less than (20)]{FAILED}) OR ([Value is greater than (50)]{FAILED})]{FAILED})]{FAILED}");
    }
    
    @Test
    @DisplayName("Non default group Test")
    public void groupValidationTest1()
    {
        // Nor name or age are valid values
        TestBean instance = new TestBean()
                .setName("john.random@foo.bar")
                .setAge(18);
        
        Validation validation = Validators.<TestBean>beanValidator(ValidationMode.FAIL_FAST)
                // Default group validations
                .constraint("Valid user name", 
                    "Value ({0}) is not a valid user name", 
                    TestBean::getName, 
                    isNotNull(String.class)
                        .and(isEmail()))
                .constraint("Valid user age",
                    "Value ({0}) is not a valid user age",
                    TestBean::getAge, 
                    isNotNull(Integer.class)
                        .and(isLessThan(20)
                            .or(isGreaterThan(50))))
                // Named group
                .group("Optional validations 1", v -> v.getName().endsWith("@foo.bar"))
                    .constraint("Valid user name", 
                        "Value ({0}) is not a valid user name", 
                        TestBean::getName, 
                        map(String.class, 
                            name -> name==null ? 0 : name.length(), 
                            isGreaterThan(10)
                                .and(isLessThan(100))
                        ))
                 // Named group
                .group("Optional validations 2", v -> v.getName().endsWith("@baz.qux"))
                    .constraint("Valid user name", 
                        "Value ({0}) is not a valid user name", 
                        TestBean::getName, 
                        map(String.class, 
                            name -> name==null ? 0 : name.length(), 
                            isGreaterThan(10)
                                .and(isLessThan(100))
                        ))
                .build()
                .validate(instance);
        
        ValidationExecution execution = validation.getExecution();
        assertThat(execution.getGroupExecution(Validation.EXECUTION_GROUP_DEFAULT).getStatementExecutions())
                .hasSize(2)
                .extracting(Validation.ConstraintStatementExecution::getResult)
                .containsExactly(Validation.Result.SUCCEEDED, Validation.Result.SUCCEEDED);
        
        assertThat(execution.getGroupExecution("Optional validations 1").isSuccess())
                .isTrue();
        assertThat(execution.getGroupExecution("Optional validations 1").getStatementExecutions())
                .hasSize(1)
                .extracting(Validation.ConstraintStatementExecution::getResult)
                .containsExactly(Validation.Result.SUCCEEDED);
        
        assertThat(execution.getGroupExecution("Optional validations 2").isSkipped())
                .isTrue();
    }
    
    /*----------------------------------*
     * HELPER METHODS AND CLASSES
     *----------------------------------*/
    public static Stream<Object> provideConstraintExpressionTestValues()
	{
    	// 1 || 2 && 3 || (4 || 5 || 6) && 7
    	IConstraint<?> c1 = isLessThan(1)
		    	.or(isLessThan(2))
		    	.and(isLessThan(3))
		    	.or(isLessThan(4)
		    		.or(isLessThan(5))
		    		.or(isLessThan(6)))
		    	.and(isLessThan(7));
    	
    	// 1 && 2 || 3 && (4 || 5 || 6) || 7
        IConstraint<Integer> c2 = isLessThan(1)
	            .and(isLessThan(2))
	            .or(isLessThan(3))
	            .and(isLessThan(4)
	                .or(isLessThan(5))
	                .or(isLessThan(6)))
	            .or(isLessThan(7));
        
        // NotNull && (<20 || >50)
        IConstraint<Integer> c3 = isNotNull(Integer.class)
        		.and(isLessThan(20)
        			.or(isGreaterThan(50)));
    	
		return Stream.of(
				new Object[] { c1, "([Value is less than (1)]) OR (([Value is less than (2)]) AND ([Value is less than (3)])) OR ((([Value is less than (4)]) OR ([Value is less than (5)]) OR ([Value is less than (6)])) AND ([Value is less than (7)]))" },
				new Object[] { c2, "(([Value is less than (1)]) AND ([Value is less than (2)])) OR (([Value is less than (3)]) AND (([Value is less than (4)]) OR ([Value is less than (5)]) OR ([Value is less than (6)]))) OR ([Value is less than (7)])" },
				new Object[] { c3, "([Value is not null]) AND (([Value is less than (20)]) OR ([Value is greater than (50)]))" }
    	);
	}
}
