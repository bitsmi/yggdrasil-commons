package com.bitsmi.yggdrasil.commons.test.util;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowable;

import java.util.function.Function;
import java.util.stream.Stream;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import com.bitsmi.yggdrasil.commons.util.Range;

public class RangeTestCase 
{
	@ParameterizedTest(name = "Test OK integer expression \"{0}\" with value \"{1}\"")
    @MethodSource("provideIntegerExpressionTestValues")
	public void integerExpressionRangeOkTest1(String expression, Integer testValue, boolean expectedResult)
	{
		Range<Integer> range = Range.parseIntegerExpression(expression);
		boolean result = range.contains(testValue);
		
		assertThat(result).isEqualTo(expectedResult);
	}
	
	@ParameterizedTest(name = "Test OK String expression \"{0}\" with value \"{1}\"")
    @MethodSource("provideStringExpressionTestValues")
	public void stringExpressionRangeOkTest1(String expression, String testValue, boolean expectedResult)
	{
		Range<String> range = Range.parseStringExpression(expression);
		boolean result = range.contains(testValue);
		
		assertThat(result).isEqualTo(expectedResult);
	}
	
	@ParameterizedTest(name = "Test invalid integer expressions \"{0}\"")
    @MethodSource("provideIntegerExpressionValidationTestValues")
	public void integerExpressionValidationTest1(String expression, String expectedError)
	{
		Throwable t = catchThrowable(() -> {
				Range.parseIntegerExpression(expression);
		});
		
		assertThat(t)
				.isInstanceOf(IllegalArgumentException.class)
				.hasMessage(expectedError);
		System.out.println("Error: " + t.getMessage());
	}
	
	@ParameterizedTest(name = "Test range to expression from source expression \"{0}\"")
    @MethodSource("provideFormatTestValues")
	public void formatTest1(String expression, Range.RangeExpressionFormat format, String expectedResult)
	{
		Range<String> range = Range.parseStringExpression(expression);
		String result = range.format(format, Function.identity());
		
		assertThat(result).isEqualTo(expectedResult);
	}
	
	@ParameterizedTest(name = "Factory methods Integer OK test")
    @MethodSource("provideIntegerRangeValues")
	public void integerFactoryMethodsTest1(Range<Integer> range, Integer testValue, boolean expectedResult)
	{
		boolean result = range.contains(testValue);
		
		assertThat(result).isEqualTo(expectedResult);
	}
	
	@ParameterizedTest(name = "Factory methods String OK test")
    @MethodSource("provideStringRangeValues")
	public void stringFactoryMethodsTest1(Range<String> range, String testValue, boolean expectedResult)
	{
		boolean result = range.contains(testValue);
		
		assertThat(result).isEqualTo(expectedResult);
	}
	
	/*--------------------------------*
     * SUPPORT METHODS AND CLASSES
     * ParameterizedTest's method sources must be public to be accessible by java module system
     *--------------------------------*/
    public static Stream<Object> provideIntegerExpressionTestValues()
    {
    	return Stream.of(
    			// Simplified
    			new Object[] { "0..10", 0, Boolean.TRUE },
    			new Object[] { "0..10", 10, Boolean.TRUE },
    			new Object[] { "0..10", 5, Boolean.TRUE },
                new Object[] { "0..10", 11, Boolean.FALSE },
                new Object[] { "0..10", -1, Boolean.FALSE },
                new Object[] { "-10..10", -10, Boolean.TRUE },
                new Object[] { "0..*", 999, Boolean.TRUE },
                new Object[] { "*..0", -999, Boolean.TRUE },
                new Object[] { "*..0", 1, Boolean.FALSE },
                new Object[] { "1..1", 1, Boolean.TRUE },
                new Object[] { "1..1", 2, Boolean.FALSE },
                new Object[] { "1 .. 10", 5, Boolean.TRUE },
                new Object[] { " 1..10 ", 5, Boolean.TRUE },
                new Object[] { " 1 .. 10 ", 5, Boolean.TRUE },
                // Generic
                new Object[] { "[0, 10]", 0, Boolean.TRUE },
    			new Object[] { "[0, 10]", 10, Boolean.TRUE },
    			new Object[] { "[0, 10]", 5, Boolean.TRUE },
                new Object[] { "[0, 10]", 11, Boolean.FALSE },
                new Object[] { "[0, 10]", -1, Boolean.FALSE },
                new Object[] { "[0, 10)", 10, Boolean.FALSE },
                new Object[] { "(0, 10]", 0, Boolean.FALSE },
                new Object[] { "[-10, 10]", -10, Boolean.TRUE },
                new Object[] { "[0, *)", 999, Boolean.TRUE },
                new Object[] { "(*, 0]", -999, Boolean.TRUE },
                new Object[] { "(*, 0]", 1, Boolean.FALSE },
                new Object[] { "[1, 1]", 1, Boolean.TRUE },
                new Object[] { "[1, 1]", 2, Boolean.FALSE },
                new Object[] { "[0 ,10]", 5, Boolean.TRUE },
                new Object[] { "[0 , 10]", 5, Boolean.TRUE },
                new Object[] { " [0,10] ", 5, Boolean.TRUE }
    	);
    }
    
    public static Stream<Object> provideStringExpressionTestValues()
    {
    	return Stream.of(
    			// Simplified
    			new Object[] { "aaa..bbb", "aaa", Boolean.TRUE },
    			new Object[] { "aaa..bbb", "bbb", Boolean.TRUE },
    			new Object[] { "aaa..bbb", "aba", Boolean.TRUE },
                new Object[] { "aaa..bbb", "caa", Boolean.FALSE },
                new Object[] { "bbb..ccc", "aaa", Boolean.FALSE },
                new Object[] { "aaa..*", "zzz", Boolean.TRUE },
                new Object[] { "*..zzz", "aaa", Boolean.TRUE },
                new Object[] { "*..bbb", "ccc", Boolean.FALSE },
                new Object[] { "aaa..aaa", "aaa", Boolean.TRUE },
                new Object[] { "aaa..aaa", "aab", Boolean.FALSE },
                new Object[] { "aaa .. bbb", "aab", Boolean.TRUE },
                new Object[] { " aaa..bbb ", "aab", Boolean.TRUE },
                new Object[] { " aaa .. bbb ", "aab", Boolean.TRUE },
                new Object[] { "a..bbb", "a", Boolean.TRUE },
                new Object[] { "aaa..b", "b", Boolean.TRUE },
                // Generic
                new Object[] { "[aaa, bbb]", "aaa", Boolean.TRUE },
    			new Object[] { "[aaa, bbb]", "bbb", Boolean.TRUE },
    			new Object[] { "[aaa, bbb]", "aab", Boolean.TRUE },
                new Object[] { "[aaa, bbb]", "caa", Boolean.FALSE },
                new Object[] { "[bbb, ccc]", "aaa", Boolean.FALSE },
                new Object[] { "[aaa, bbb)", "bbb", Boolean.FALSE },
                new Object[] { "(aaa, bbb]", "aaa", Boolean.FALSE },
                new Object[] { "[aaa, *)", "zzz", Boolean.TRUE },
                new Object[] { "(*, zzz]", "aaa", Boolean.TRUE },
                new Object[] { "(*, bbb]", "ccc", Boolean.FALSE },
                new Object[] { "[aaa, aaa]", "aaa", Boolean.TRUE },
                new Object[] { "[aaa, aaa]", "aba", Boolean.FALSE },
                new Object[] { "[aaa ,bbb]", "aba", Boolean.TRUE },
                new Object[] { "[aaa , bbb]", "aba", Boolean.TRUE },
                new Object[] { " [aaa,bbb] ", "aba", Boolean.TRUE },
                new Object[] { "[\"a a\",\"a c\"]", "a b", Boolean.TRUE },
                new Object[] { "[b..ccc]", "cc", Boolean.TRUE },
                new Object[] { "[aaa..b]", "ab", Boolean.TRUE },
                new Object[] { "[\"a\",\"bbb\"]", "ab", Boolean.TRUE },
                new Object[] { "[\"aa\",\"b\"]", "ab", Boolean.TRUE }
    	);
    }
    
    public static Stream<Object> provideIntegerExpressionValidationTestValues()
    {
    	return Stream.of(
    			new Object[] { "0, 10", "Invalid expression: Does not match any valid pattern"},
    			new Object[] { "/0, 10/", "Invalid expression: Does not match any valid pattern"},
    			new Object[] { "Invalid expression", "Invalid expression: Does not match any valid pattern" },
    			new Object[] { "[10, 0]", "Invalid Range: Range bounds are invalid" },
    			new Object[] { "[1, 1)", "Invalid Range: Range bounds are not compatible" },
    			new Object[] { "(1, 1]", "Invalid Range: Range bounds are not compatible" },
    			new Object[] { "(1, 1)", "Invalid Range: Range bounds are not compatible" }
    	);
    }
    
    public static Stream<Object> provideFormatTestValues()
    {
    	return Stream.of(
    			// Simplified
    			new Object[] { "aaa..bbb", Range.EXPRESSION_DEFAULT_FORMAT, "[aaa, bbb]" },
    			new Object[] { "aaa .. bbb", Range.EXPRESSION_DEFAULT_FORMAT, "[aaa, bbb]" },
    			new Object[] { " aaa..bbb ", Range.EXPRESSION_DEFAULT_FORMAT, "[aaa, bbb]" },
    			new Object[] { " aaa .. bbb ", Range.EXPRESSION_DEFAULT_FORMAT, "[aaa, bbb]" },
    			new Object[] { "\"a a a\"..\"b b b\"", Range.EXPRESSION_DEFAULT_FORMAT, "[\"a a a\", \"b b b\"]" },
    			// Generic
    			new Object[] { "[aaa, bbb]", Range.EXPRESSION_DEFAULT_FORMAT, "[aaa, bbb]" },
    			new Object[] { "[aaa , bbb]", Range.EXPRESSION_DEFAULT_FORMAT, "[aaa, bbb]" },
    			new Object[] { "[ aaa, bbb ]", Range.EXPRESSION_DEFAULT_FORMAT, "[aaa, bbb]" },
    			new Object[] { "[\"a a a\", \"b b b\"]", Range.EXPRESSION_DEFAULT_FORMAT, "[\"a a a\", \"b b b\"]" }
    	);
    }
    
    public static Stream<Object> provideIntegerRangeValues()
    {
    	return Stream.of(
                new Object[] { Range.closed(0, 10), 0, Boolean.TRUE },
    			new Object[] { Range.closed(0, 10), 10, Boolean.TRUE },
    			new Object[] { Range.closed(0, 10), 5, Boolean.TRUE },
                new Object[] { Range.closed(0, 10), 11, Boolean.FALSE },
                new Object[] { Range.closed(0, 10), -1, Boolean.FALSE },
                new Object[] { Range.closedOpen(0, 10), 10, Boolean.FALSE },
                new Object[] { Range.openClosed(0, 10), 0, Boolean.FALSE },
                new Object[] { Range.closed(-10, 10), -10, Boolean.TRUE },
                new Object[] { Range.atLeast(0), 999, Boolean.TRUE },
                new Object[] { Range.atMost(0), -999, Boolean.TRUE },
                new Object[] { Range.atMost(0), 1, Boolean.FALSE },
                new Object[] { Range.exactly(1), 1, Boolean.TRUE },
                new Object[] { Range.exactly(1), 2, Boolean.FALSE },  
                new Object[] { Range.all(), 0, Boolean.TRUE }
    	);
    }
    
    public static Stream<Object> provideStringRangeValues()
    {
    	return Stream.of(
                new Object[] { Range.closed("aaa",  "bbb"), "aaa", Boolean.TRUE },
    			new Object[] { Range.closed("aaa",  "bbb"), "bbb", Boolean.TRUE },
    			new Object[] { Range.closed("aaa",  "bbb"), "aab", Boolean.TRUE },
                new Object[] { Range.closed("aaa",  "bbb"), "caa", Boolean.FALSE },
                new Object[] { Range.closed("bbb",  "ccc"), "aaa", Boolean.FALSE },
                new Object[] { Range.closedOpen("aaa", "bbb"), "bbb", Boolean.FALSE },
                new Object[] { Range.openClosed("aaa", "bbb"), "aaa", Boolean.FALSE },
                new Object[] { Range.atLeast("aaa"), "zzz", Boolean.TRUE },
                new Object[] { Range.atMost("zzz"), "aaa", Boolean.TRUE },
                new Object[] { Range.atMost("bbb"), "ccc", Boolean.FALSE },
                new Object[] { Range.exactly("aaa"), "aaa", Boolean.TRUE },
                new Object[] { Range.exactly("aaa"), "aba", Boolean.FALSE },
                new Object[] { Range.all(), "aaa", Boolean.TRUE }
    	);
    }
}
