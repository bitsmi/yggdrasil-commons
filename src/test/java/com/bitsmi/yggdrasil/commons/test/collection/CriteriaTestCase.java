package com.bitsmi.yggdrasil.commons.test.collection;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowable;

import java.util.HashMap;
import java.util.Map;

import org.junit.jupiter.api.Test;

import com.bitsmi.yggdrasil.commons.collection.Criteria;
import com.bitsmi.yggdrasil.commons.exception.MissingValueException;

public class CriteriaTestCase 
{
    @Test
    public void plainExpressionTest()
    {
        Map<String, Object> context = new HashMap<>();
        context.put("A", Boolean.FALSE);
        context.put("B", Boolean.TRUE);
        context.put("C", Boolean.TRUE);
        context.put("D", Boolean.FALSE);
        
        // A | B & C | D
        Criteria criteria1 = new Criteria()
                .isTrue("A")
                .or().isTrue("B")
                    .and().isTrue("C")
                .or().isTrue("D")
                .build();
        
        // A & B & C | D
        Criteria criteria2 = new Criteria()
                .isTrue("A")
                    .and().isTrue("B")
                    .and().isTrue("C")
                .or().isTrue("D")
                .build();
        
        // A | B | C & D
        Criteria criteria3 = new Criteria()
                .isTrue("A")
                .or().isTrue("B")
                .or().isTrue("C")
                    .and().isTrue("D")
                .build();
        
        boolean result1 = criteria1.evaluate(context);
        String expression1 = criteria1.toString();
        boolean result2 = criteria2.evaluate(context);
        String expression2 = criteria2.toString();
        boolean result3 = criteria3.evaluate(context);
        String expression3 = criteria3.toString();
        
        assertThat(result1).isTrue();
        assertThat(expression1).isEqualTo("[{A} OR [[{B} AND {C}] OR {D}]]");
        assertThat(result2).isFalse();
        assertThat(expression2).isEqualTo("[{A} AND [[{B} AND {C}] OR {D}]]");
        assertThat(result3).isTrue();
        assertThat(expression3).isEqualTo("[[{A} OR {B}] OR [{C} AND {D}]]");
    }
    
	@Test
	public void groupExpressionTest()
	{
		Map<String, Object> context = new HashMap<>();
		context.put("A", Boolean.FALSE);
		context.put("B", Boolean.TRUE);
		context.put("C", Boolean.FALSE);
		context.put("D", Boolean.TRUE);
		context.put("E", Boolean.TRUE);
		context.put("F", Boolean.FALSE);

		// A | B & C | (D | E) & F
		Criteria criteria1 = new Criteria()
				.isTrue("A")
				.or().isTrue("B").and().isTrue("C")
				.or().startGroup()
					.isTrue("D").or().isTrue("E")
				.endGroup()
				.and().isTrue("F")
				.build();
		
		// A | B & (C | D | E) & F
        Criteria criteria2 = new Criteria()
                .isTrue("A")
                .or().isTrue("B")
                    .and().startGroup()
                        .isTrue("C")
                        .or().isTrue("D")
                        .or().isTrue("E")
                    .endGroup()
                    .and().isTrue("F")
                    .build();
				
		boolean result1 = criteria1.evaluate(context);
		String expression1 = criteria1.toString();
		boolean result2 = criteria2.evaluate(context);
        String expression2 = criteria2.toString();
		
		assertThat(result1).isFalse();
		assertThat(expression1).isEqualTo("[{A} OR [[{B} AND {C}] OR [([{D} OR {E}]) AND {F}]]]");
		assertThat(result2).isFalse();
        assertThat(expression2).isEqualTo("[{A} OR [{B} AND [([[{C} OR {D}] OR {E}]) AND {F}]]]");
	}
	
	@Test
	public void equalsExpressionTest()
	{
		Map<String, Object> context = new HashMap<>();
		context.put("A", Boolean.TRUE);
		context.put("B", "VALUE_1");
		context.put("C", "VALUE_1");
		context.put("D", Boolean.FALSE);
		
		// A & B=C & B='VALUE_1' & !D
		Criteria criteria = new Criteria()
				.isTrue("A")
				.and().isEqualsToReference("B", "C")
				.and().isEqualsToValue("B", "VALUE_1")
				.and().isFalse("D")
				.build();
		
		boolean result = criteria.evaluate(context);
		
		assertThat(result).isTrue();
	}
	
	@Test
	public void notEqualsExpressionTest()
	{
		Map<String, Object> context = new HashMap<>();
		context.put("A", Boolean.TRUE);
		context.put("B", "VALUE_1");
		context.put("C", "VALUE_2");
		context.put("D", Boolean.FALSE);
		
		// A & B=C & B='VALUE_1' & !D
		Criteria criteria = new Criteria()
				.isTrue("A")
				.and().isNotEqualsToReference("B", "C")
				.and().isNotEqualsToValue("C", "NAN")
				.and().isFalse("D")
				.build();
		
		boolean result = criteria.evaluate(context);
		
		assertThat(result).isTrue();
	}
	
	@Test
	public void nullExpressionTest()
	{
		Map<String, Object> context = new HashMap<>();
		context.put("A", Boolean.TRUE);
		context.put("B", null);
		context.put("C", "VALUE_2");
		context.put("D", Boolean.FALSE);
		
		// A & B is null & C is not null & !D
		Criteria criteria = new Criteria()
				.isTrue("A")
				.and().isNull("B")
				.and().isNotNull("C")
				.and().isFalse("D")
				.build();
		
		boolean result = criteria.evaluate(context);
		
		assertThat(result).isTrue();
	}
	
	@Test
	public void compareToExpressionTest()
	{
		Map<String, Object> context = new HashMap<>();
		context.put("A", 11);
		context.put("B", 8);
		context.put("C", 9);
		context.put("D", 10);
		
		// A>10 & A>=11 & A>C & A>=B & B<10 & B<=8 & B<C & B<=D
		Criteria criteria = new Criteria()
				.isGreaterOrEqualsToValue("A", 10)
				.and().isGreaterOrEqualsToValue("A", 11)
				.and().isGreaterThanReference("A", "C")
				.and().isGreaterOrEqualsToReference("A", "B")
				.and().isLessThanValue("B", 10)
				.and().isLessOrEqualsToValue("B", 8)
				.and().isLessThanReference("B", "C")
				.and().isLessOrEqualsToReference("B", "D")
				.build();
		
		boolean result = criteria.evaluate(context);
		
		assertThat(result).isTrue();
	}

	@Test
	public void negateExpressionTest()
	{
		Map<String, Object> context = new HashMap<>();
        context.put("A", Boolean.FALSE);
        context.put("B", Boolean.TRUE);
        context.put("C", Boolean.TRUE);
        context.put("D", Boolean.FALSE);
        
        // A | !B & C | D
        Criteria criteria = new Criteria()
                .isTrue("A")
                .or().not().isTrue("B")
                    .and().isTrue("C")
                .or().isTrue("D")
                .build();

        boolean result1 = criteria.evaluate(context);
        String expression = criteria.toString();
        
        context.put("B", Boolean.FALSE);
        boolean result2 = criteria.evaluate(context);
        
        assertThat(result1).isFalse();
        assertThat(result2).isTrue();
        assertThat(expression).isEqualTo("[{A} OR [[!{B} AND {C}] OR {D}]]");
	}
	
	@Test
	public void negateGroupExpressionTest()
	{
		Map<String, Object> context = new HashMap<>();
		context.put("A", Boolean.TRUE);
		context.put("B", Boolean.FALSE);
		context.put("C", Boolean.FALSE);
		context.put("D", Boolean.TRUE);

		// A & !(B | C) & D
		Criteria criteria = new Criteria()
				.isTrue("A")
				.and().not().startGroup()
					.isTrue("B").or().isTrue("C")
				.endGroup()
				.and().isTrue("D")
				.build();
		
		boolean result1 = criteria.evaluate(context);
		String expression = criteria.toString();
		
		context.put("B", Boolean.TRUE);
		boolean result2 = criteria.evaluate(context);
		
		assertThat(result1).isTrue();
		assertThat(result2).isFalse();
		assertThat(expression).isEqualTo("[{A} AND [!([{B} OR {C}]) AND {D}]]");
	}
	
	/*---------------------------------------------*
	 * FAIL
	 *---------------------------------------------*/
	@Test
	public void missingExpressionTest()
	{
		Map<String, Object> context = new HashMap<>();
        context.put("A", Boolean.FALSE);
        context.put("B", Boolean.TRUE);
        context.put("C", Boolean.TRUE);
        // Missing D
        
        // A | B & C | D
        Criteria criteria = new Criteria()
                .isTrue("A")
                .or().isTrue("B")
                    .and().isTrue("C")
                .or().isTrue("D")
                .build();
        
        Throwable thrown = catchThrowable(() -> criteria.evaluate(context));
        
        assertThat(thrown).isNotNull().isInstanceOf(MissingValueException.class);
	}
}
