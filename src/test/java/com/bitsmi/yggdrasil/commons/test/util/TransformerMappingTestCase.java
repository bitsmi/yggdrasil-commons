package com.bitsmi.yggdrasil.commons.test.util;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import com.bitsmi.yggdrasil.commons.test.util.support.TransformerTestPojos.SourceComplexPojo;
import com.bitsmi.yggdrasil.commons.test.util.support.TransformerTestPojos.TargetComplexPojo;
import com.bitsmi.yggdrasil.commons.test.util.support.TransformerTestPojos.TargetPojo;
import com.bitsmi.yggdrasil.commons.test.util.support.TransformerTestPojos.TransformableSourcePojo;
import com.bitsmi.yggdrasil.commons.util.TransformerMapping;

public class TransformerMappingTestCase
{
    @Test
    @DisplayName("Transformer mapping OK Test")
    public void transformerMappingTest1()
    {
        TransformerMapping<TransformableSourcePojo, TargetPojo> mapping = buildMapping();
        
        TransformableSourcePojo srcPojo = new TransformableSourcePojo()
                .setSourceValue1("Value 1")
                .setSourceValue2("Value 2")
                .setSourceComplexValue(new SourceComplexPojo()
                        .setSourceValue1("Complex value 1")
                        .setSourceValue2("Complex value 2"));
        
        TargetPojo targetPojo = mapping.apply(srcPojo);
        
        assertThat(targetPojo.getTargetValue1()).isEqualTo(srcPojo.getSourceValue1());
        assertThat(targetPojo.getTargetValue2()).isEqualTo(srcPojo.getSourceValue2());
        assertThat(targetPojo.getTargetComplexValue()).isNotNull()
                .satisfies(value -> {
                    SourceComplexPojo sourceValue = srcPojo.getSourceComplexValue();
                    assertThat(value.getTargetValue1()).isEqualTo(sourceValue.getSourceValue1());
                    assertThat(value.getTargetValue2()).isEqualTo(sourceValue.getSourceValue2());
                });
    }
    
    @Test
    @DisplayName("Transformer mapping nulls Test")
    public void transformerMappingTest2()
    {
        TransformerMapping<TransformableSourcePojo, TargetPojo> mapping = buildMapping();
        
        TransformableSourcePojo srcPojo = new TransformableSourcePojo()
                .setSourceValue1("Value 1")
                .setSourceValue2(null)
                .setSourceComplexValue(new SourceComplexPojo()
                        .setSourceValue1("Complex value 1")
                        .setSourceValue2(null));
        
        TargetPojo targetPojo = mapping.apply(srcPojo);
        
        assertThat(targetPojo.getTargetValue1()).isEqualTo(srcPojo.getSourceValue1());
        assertThat(targetPojo.getTargetValue2()).isNull();
        assertThat(targetPojo.getTargetComplexValue()).isNotNull()
                .satisfies(value -> {
                    SourceComplexPojo sourceValue = srcPojo.getSourceComplexValue();
                    assertThat(value.getTargetValue1()).isEqualTo(sourceValue.getSourceValue1());
                    assertThat(value.getTargetValue2()).isNull();
                });
    }
    
    @Test
    @DisplayName("Transformer mapping null complex value Test")
    public void transformerMappingTest3()
    {
        TransformerMapping<TransformableSourcePojo, TargetPojo> mapping = buildMapping();
        
        TransformableSourcePojo srcPojo = new TransformableSourcePojo()
                .setSourceValue1("Value 1")
                .setSourceValue2(null)
                .setSourceComplexValue(null);
        
        TargetPojo targetPojo = mapping.apply(srcPojo);
        
        assertThat(targetPojo.getTargetValue1()).isEqualTo(srcPojo.getSourceValue1());
        assertThat(targetPojo.getTargetValue2()).isNull();
        assertThat(targetPojo.getTargetComplexValue()).isNull();
    }
    
    /*------------------------------------------*
     * HELPER METHODS
     *------------------------------------------*/
    static TransformerMapping<TransformableSourcePojo, TargetPojo> buildMapping()
    {
        TransformerMapping<SourceComplexPojo, TargetComplexPojo> complexValueMapping = new TransformerMapping<SourceComplexPojo, TargetComplexPojo>()
                .withTarget(TargetComplexPojo::new)
                .from(SourceComplexPojo::getSourceValue1).to(TargetComplexPojo::setTargetValue1)
                .from(SourceComplexPojo::getSourceValue2).to(TargetComplexPojo::setTargetValue2);
        
        return new TransformerMapping<TransformableSourcePojo, TargetPojo>()
                .withTarget(TargetPojo::new)
                .from(TransformableSourcePojo::getSourceValue1).to(TargetPojo::setTargetValue1)
                .from(src -> src.getSourceValue2()).to((target, value) -> target.setTargetValue2(value))
                .from(TransformableSourcePojo::getSourceComplexValue, complexValueMapping)
                    .to(TargetPojo::setTargetComplexValue);
    }
}
