package com.bitsmi.yggdrasil.commons.util;

import java.util.LinkedList;
import java.util.List;

public class StopWatch 
{
	public static final String UNNAMED_TASK = "UNNAMED";
	
	private List<Task> finishedTasks = new LinkedList<>();
	private long totalTimeMillis = 0;
	private Task currentTask;
	private Task lastFinishedTask;
	
	public void start()
	{
		start(UNNAMED_TASK);
	}
	
	public void start(String taskName)
	{
		if(currentTask!=null) {
			throw new IllegalStateException("Ja hi ha una tasca iniciada");
		}
		
		currentTask = new Task(taskName);
		currentTask.start();
	}
	
	public void stop()
	{
		if(currentTask==null) {
			throw new IllegalStateException("No hi ha cap tasca iniciada");
		}
		
		currentTask.stop();
		finishedTasks.add(currentTask);
		lastFinishedTask = currentTask;
		totalTimeMillis += currentTask.getTimeMillis(); 
		currentTask = null;
	}
	
	public long splitMillis()
	{
		if(currentTask==null) {
			throw new IllegalStateException("No hi ha cap tasca iniciada");
		}
		
		return getTotalTimeMillis() + currentTask.splitMillis();
	}
	
	public long splitCurrentTaskMillis()
	{
		if(currentTask==null) {
			throw new IllegalStateException("No hi ha cap tasca iniciada");
		}
		
		return currentTask.splitMillis();
	}
	
	public long getTotalTimeMillis()
	{
		return totalTimeMillis;
	}
	
	public double getTotalTimeSeconds()
	{
		long millis = getTotalTimeMillis();
		return millis / 1000.0;
	}
	
	public long getLastTaskTimeMillis()
	{
		if(finishedTasks.size()==0) {
			throw new IllegalStateException("No hi ha cap tasca finalitzada");
		}
		return lastFinishedTask.getTimeMillis();
	}
	
	public void getTasksInfo(TaskInfoConsumer consumer)
	{
		if(finishedTasks.size()==0) {
			throw new IllegalStateException("No hi ha cap tasca finalitzada");
		}
		
		for(Task task:finishedTasks) {
			consumer.accept(task.getName(), task.getTimeMillis());
		}
	}
	
	static class Task
	{
		private String name;
		private boolean running;
		private long startTimeMillis;
		private long timeMillis;
		
		public Task(String name)
		{
			this.name = name;
		}
		
		public String getName() 
		{
			return name;
		}
		
		public void start()
		{
			if(startTimeMillis>0) {
				throw new IllegalStateException("La tasca ja ha sigut iniciada previament");
			}
			
			this.startTimeMillis = System.currentTimeMillis();
			this.running = true;
		}

		public void stop()
		{
			if(startTimeMillis==0) {
				throw new IllegalStateException("La tasca no ha sigut iniciada");
			}
			
			this.running = false;
			this.timeMillis = System.currentTimeMillis() - startTimeMillis;
		}

		public long getTimeMillis() 
		{
			return timeMillis;
		}
		
		public long splitMillis()
		{
			if(startTimeMillis==0) {
				throw new IllegalStateException("La tasca no ha sigut iniciada");
			}
			if(!running) {
				throw new IllegalStateException("La tasca es troba aturada");
			}
			
			return System.currentTimeMillis() - startTimeMillis;
		}
	}
	
	@FunctionalInterface
	public static interface TaskInfoConsumer
	{
		public void accept(String name, long taskMillis);
	}
}
