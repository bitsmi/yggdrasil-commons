package com.bitsmi.yggdrasil.commons.util;

import java.util.List;

/**
 *
 * @author Antonio Archilla
 * @since 1.6.0
 */
public interface IProgressTrackerTask 
{
	public static final int TASK_MIN_VALUE = 0;
	public static final int TASK_MAX_VALUE = 100;
	
	public List<? extends IProgressTrackerTask> split(int... subtaskAmounts);
	public boolean hasSubtasks();
	public List<? extends IProgressTrackerTask> getSubtasks();
	public int getSubtaskCount();
	public IProgressTrackerTask getCurrentSubtask();
	public int getCurrentSubtaskIndex();
	public IProgressTrackerTask getSubtask(int index);
	
	public int computeProgress();
	
	public int advance(int amount);
	public int advance(int amount, IProgressTrackerListener.ProgressInfo info);
	public boolean isComplete();
	public void complete();
	public void complete(IProgressTrackerListener.ProgressInfo info);
	
	public IProgressTrackerTask addProgressListener(IProgressTrackerListener listener);
}
