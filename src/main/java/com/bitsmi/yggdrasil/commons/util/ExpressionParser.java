package com.bitsmi.yggdrasil.commons.util;

import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

import com.bitsmi.yggdrasil.commons.util.Expressions.Expression;
import com.bitsmi.yggdrasil.commons.util.Expressions.PlainExpression;

public class ExpressionParser 
{
    private List<Expression> expressions;
    
    private ExpressionParser()
    {
        
    }
    
    public ParseResult parse(String input)
    {
        Objects.requireNonNull(input);
        
        ParsingContext ctx = new ParsingContext(this, input);
        ParseResult result = new ParseResult(ctx); 
        while(!isExhaustedAtPosition(ctx, ctx.getPosition())) {
            ParsedExpression parsedExpr = evaluateExpressions(ctx);
            if(parsedExpr==null || !parsedExpr.matches()) {
                break;
            }
            else if(parsedExpr!=null) {
                result.addParsedExpression(parsedExpr);
            }
        }
        
        return result;
    }
    
    private ParsedExpression evaluateExpressions(ParsingContext globalCtx)
    {
        ParsedExpression parsedExpr = null;
        Iterator<Expression> iterator = expressions.iterator();
        while(iterator.hasNext()) {
            ParsingContext localCtx = new ParsingContext(globalCtx);
            Expression expression = iterator.next();
            parsedExpr = expression.accept(localCtx);
            
            if(parsedExpr!=null && parsedExpr.matches()) {
                localCtx.commit();
                break;
            }
        }
        
        return parsedExpr;
    }
    
    public boolean isExhaustedAtPosition(ParsingContext ctx, int position)
    {
        return ctx.input.length()<=position;
    }
    
    public char read(ParsingContext ctx, int position)
    {
        return ctx.input.charAt(position);
    }
    
    ParsedExpression visit(ParsingContext ctx, PlainExpression expr)
    {
        ParsedExpression result = new ParsedExpression(expr);
        
        ParsingExpressionContext expressionCtx = new ParsingExpressionContext(ctx);
        boolean follow = !isExhaustedAtPosition(ctx, expressionCtx.getPosition());
        // Matches == at least one char matches the expression
        boolean matches = false;
        while (follow) {
            char c = read(ctx, expressionCtx.getPosition());
            expressionCtx.setCurrentChar(c);
            boolean currentMatches = expr.test(expressionCtx); 
            matches = matches || currentMatches;
            expressionCtx.appendToProcessedValue(c);
            if(currentMatches) {
                expressionCtx.appendToMatchingValue(c);
                expressionCtx.incrementPosition();
            }
            follow = expr.isGreedy() && currentMatches && !isExhaustedAtPosition(ctx, expressionCtx.getPosition());
        }
    
        result.setMatch(matches);
        result.setProcessedValue(expressionCtx.getProcessedValue());
        result.setMatchingValue(expressionCtx.getMatchingValue());
        result.setStartPosition(expressionCtx.getStartingPosition());
        result.setEndPosition(expressionCtx.getPosition());
    
        return result;
    }
    
	public static ExpressionParserBuilder builder()
	{
		return new ExpressionParserBuilder();
	}
	
	public static class ExpressionParserBuilder
	{
	    private List<Expression> expressions = new ArrayList<>();
	    
	    public ExpressionParserBuilder withExpressions(Expression... expressions)
	    {
	        this.expressions.addAll(Arrays.asList(expressions));
	        return this;
	    }
	    
	    public ExpressionParser build()
	    {
	        ExpressionParser parser = new ExpressionParser();
	        parser.expressions = expressions;
	        
	        return parser;
	    }
	}
	
	public static class ParsingContext
	{
	    private ExpressionParser parser;
	    private String input;
	    private int position;
	    private Map<String, List<String>> captures = new HashMap<>();
	    
	    private ParsingContext parentCtx;
	    
	    ParsingContext(ExpressionParser parser, String input)
	    {
	        this.parser = parser;
	        this.input = input;
	    }
	    
	    public ParsingContext(ParsingContext parentCtx)
        {
            this(parentCtx.parser, parentCtx.input);
            this.parentCtx = parentCtx;
            this.position = parentCtx.position;
        }
	    
	    public ExpressionParser getParser()
	    {
	        return parser;
	    }
	    
	    public String getInput()
	    {
	        return input;
	    }
	    
	    public int getPosition()
	    {
	        return position;
	    }
	    
	    public void setPosition(int position)
	    {
	        this.position = position;
	    }
	    
	    public List<String> getCapturedValues(String varName)
	    {
	        return captures.get(varName);
	    }
	    
	    public void addCapturedValue(String varName, String value)
	    {
	        if(!captures.containsKey(varName)) {
	            captures.put(varName, new ArrayList<>());
	        }
	        
	        captures.get(varName).add(value);
	    }
	    
	    public void updatePosition(ParsedExpression expressionResult)
	    {
	        // Reset position
	        if(!expressionResult.matches()) {
	            setPosition(expressionResult.getStartPosition());
	        }
	        else {
	            setPosition(expressionResult.getEndPosition());
	        }
	    }
	    
	    public void commit()
	    {
	        if(parentCtx==null) {
	            return;
	        }
	        
	        parentCtx.position = position;
	        captures.entrySet().stream()
	                .flatMap(entry -> entry.getValue()
	                    .stream()
	                    .map(value -> new AbstractMap.SimpleImmutableEntry<>(entry.getKey(), value)))
	                .forEach(entry -> parentCtx.addCapturedValue(entry.getKey(), entry.getValue()));
	    }
	}
	
	public static class ParsingExpressionContext
    {
        private int startingPosition;
        private int position;
        private char currentChar;
        private StringBuilder processedValue = new StringBuilder();
        private StringBuilder matchingValue = new StringBuilder();
        
        ParsingExpressionContext(ParsingContext ctx)
        {
            this.startingPosition = ctx.getPosition();
            this.position = ctx.getPosition();
        }
        
        public int getPosition()
        {
            return position;
        }
        
        public int getStartingPosition()
        {
            return startingPosition;
        }
        
        public void incrementPosition()
        {
            position++;
        }
        
        public void resetPosition()
        {
            this.position = startingPosition;
        }
        
        public char getCurrentChar()
        {
            return currentChar;
        }
        
        public void setCurrentChar(char currentChar)
        {
            this.currentChar = currentChar;
        }
        
        public String getProcessedValue()
        {
            return processedValue.toString();
        }
        
        public void appendToProcessedValue(char c)
        {
            processedValue.append(c);
        }
        
        public String getMatchingValue()
        {
            return matchingValue.toString();
        }
        
        public void appendToMatchingValue(char c)
        {
            matchingValue.append(c);
        }
    }
	
	public static class ParseResult
	{
	    private ParsingContext parsingContext;
	    private List<ParsedExpression> parsedExpressions = new ArrayList<>();
	    
	    ParseResult(ParsingContext parsingContext)
	    {
	        this.parsingContext = parsingContext;
	    }
	    
	    public ParsingContext getParsingContext()
	    {
	        return parsingContext;
	    }
	    
	    public List<ParsedExpression> getParsedExpressions()
	    {
	        return parsedExpressions;
	    }
	    
	    public List<ParsedExpression> getMatchedParsedExpressions()
	    {
	        return parsedExpressions.stream()
	                .filter(ParsedExpression::matches)
	                .collect(Collectors.toList());
	    }
	    
	    public List<ParsedExpression> getParsedExpressions(String name)
        {
	        Objects.requireNonNull(name);
	        
            return parsedExpressions.stream()
                    .filter(parsedExpr -> name.equals(parsedExpr.getExpression().getName()))
                    .collect(Collectors.toList());
        }
	    
	    public void addParsedExpression(ParsedExpression parsedExpression)
	    {
	        Objects.requireNonNull(parsedExpression);
	        parsedExpressions.add(parsedExpression);
	    }
	    
	    public List<String> getCapturedValues(String name)
	    {
	        return parsingContext.getCapturedValues(name);
	    }
	    
	    public boolean isInputFullyMatched()
	    {
	        return parsingContext.getInput().length() == parsingContext.getPosition();
	    }
	    
	    public void visitParsedExpressions(IParsedExpressionVisitor visitor)
	    {
	        boolean follow = true;
	        Iterator<ParsedExpression> iterator = parsedExpressions.iterator();
	        while(follow && iterator.hasNext()) {
	            ParsedExpression parsedExpression = iterator.next();
	            String parsedExprPath = "/" + parsedExpression.getExpression().getName();
	            follow = visitParsedExpression(parsedExprPath, parsedExpression, visitor);
	        }
	    }
	    
	    boolean visitParsedExpression(String expressionPath, ParsedExpression parsedExpression, IParsedExpressionVisitor visitor)
	    {
	        boolean follow = visitor.visit(expressionPath, parsedExpression);
	        
	        if(!follow) {
	            return false;
	        }
	        
	        // Siblings expressions of the same kind count
	        Map<String, Integer[]> siblingsCount = parsedExpression.getNestedParsedExpressions()
	                .stream()
	                .collect(Collectors.groupingBy(pex -> pex.getExpression().getName(), Collectors.counting()))
	                .entrySet()
	                .stream()
	                .filter(entry -> entry.getValue() > 1)
	                .collect(Collectors.toMap(entry -> entry.getKey(), entry -> new Integer[] {0}));
	        
	        Iterator<ParsedExpression> iterator = parsedExpression.getNestedParsedExpressions().iterator();
            while(follow && iterator.hasNext()) {
                ParsedExpression nestedParsedExpression = iterator.next();
                String nestedExprName = nestedParsedExpression.getExpression().getName();
                String nestedExprPath = expressionPath + "/" + nestedExprName;
                if(siblingsCount.containsKey(nestedExprName)) {
                    int count = siblingsCount.get(nestedExprName)[0]++;
                    nestedExprPath += "[" + count + "]";
                }
                
                follow = visitParsedExpression(nestedExprPath, nestedParsedExpression, visitor);
            }
            
            return follow;
	    }
	}
	
	public static class ParsedExpression
	{
	    private Expression expression;
	    private boolean match;
	    private int startPosition;
	    private int endPosition;
	    private String processedValue;
	    private String matchingValue;
	    
	    private List<ParsedExpression> nestedParsedExpressions = new ArrayList<>();
	    
	    public ParsedExpression(Expression expression)
	    {
	        this.expression = expression;
	    }
	    
	    public void update(boolean match, String processedValue, String matchingValue, int startPosition, int endPosition)
	    {
	        this.match = match;
	        this.processedValue = processedValue;
	        this.matchingValue = matchingValue;
            this.startPosition = startPosition;
            this.endPosition = endPosition;
	    }
	    
	    public Expression getExpression()
	    {
	        return expression;
	    }
	    
	    public boolean matches()
	    {
	        return match;
	    }
	    
	    public void setMatch(boolean match)
	    {
	        this.match = match;
	    }

        public int getStartPosition()
        {
            return startPosition;
        }

        public void setStartPosition(int startPosition)
        {
            this.startPosition = startPosition;
        }

        public int getEndPosition()
        {
            return endPosition;
        }

        public void setEndPosition(int endPosition)
        {
            this.endPosition = endPosition;
        }
        
        public String getProcessedValue()
        {
            return processedValue;
        }
        
        public void setProcessedValue(String processedValue)
        {
            this.processedValue = processedValue;
        }
        
        public String getMatchingValue()
        {
            return matchingValue;
        }
        
        public void setMatchingValue(String matchingValue)
        {
            this.matchingValue = matchingValue;
        }
        
        public List<ParsedExpression> getNestedParsedExpressions()
        {
            return nestedParsedExpressions;
        }
        
        public boolean hasNestedParsedExpressions()
        {
            return !nestedParsedExpressions.isEmpty();
        }
        
        public void addNestedParsedExpression(ParsedExpression nested)
        {
            this.nestedParsedExpressions.add(nested);
        }
	}
	
	public static interface IParsedExpressionVisitor
	{
	    public boolean visit(String path, ParsedExpression expr);
	}
}
