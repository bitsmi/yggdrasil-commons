package com.bitsmi.yggdrasil.commons.util;

/**
 * @since 1.8.0, 2.1.0
 */
public interface ITransformable 
{
    public ITransformer<?> transformed();
}
