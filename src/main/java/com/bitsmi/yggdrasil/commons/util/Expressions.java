package com.bitsmi.yggdrasil.commons.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.function.BiFunction;
import java.util.function.Predicate;
import java.util.function.Supplier;
import java.util.stream.Collectors;

import com.bitsmi.yggdrasil.commons.util.ExpressionParser.ParsedExpression;
import com.bitsmi.yggdrasil.commons.util.ExpressionParser.ParsingContext;
import com.bitsmi.yggdrasil.commons.util.ExpressionParser.ParsingExpressionContext;

public class Expressions 
{
    public static final Expression END = new EndExpression();
    public static final Expression WHITESPACE = singleCharIn('\n', '\t', '\r', ' ');
    public static final Expression DIGIT = singleCharBetween('0', '9');
    
	public static Expression sequence(Expression... expressions)
	{
	    Objects.requireNonNull(expressions);
        
        SequenceExpression sequenceExpression = new SequenceExpression();
        sequenceExpression.addExpressions(expressions);
        
        return sequenceExpression;
	}
	
	public static Expression choice(Expression... expressions)
	{
	    Objects.requireNonNull(expressions);
	    
	    ChoiceExpression choiceExpression = new ChoiceExpression();
	    choiceExpression.addExpressions(expressions);
	    
	    return choiceExpression;
	}
	
	public static Expression not(Expression expression)
	{
	    Objects.requireNonNull(expression);
	    
	    WrappedExpression wrappedExpression = new WrappedExpression("not", expression);
	    wrappedExpression.setProcessor((ctx, result) -> {
	        // Copy and modify wrapped expression result
	        ParsedExpression finalResult = new ParsedExpression(wrappedExpression);
	        finalResult.update(!result.matches(), 
	                result.getProcessedValue(),
	                // If expression didn't match, the negated matched value will be the processed input
	                !result.matches() ? result.getProcessedValue() : null,
	                result.getStartPosition(),
	                result.getStartPosition() + result.getProcessedValue().length());
	        return finalResult;
	    });
	    
	    return wrappedExpression;
	}
	
    public static Expression lookahead(Expression expression)
    {
        Objects.requireNonNull(expression);
        
        WrappedExpression wrappedExpression = new WrappedExpression("lookahead", expression);
        wrappedExpression.setProcessor((ctx, result) -> {
            // Copy and modify wrapped expression result
            ParsedExpression finalResult = new ParsedExpression(wrappedExpression);
            finalResult.update(result.matches(),
                // Lookahead expression doesn't modify current parsing context
                null,
                null,
                /* Do not use ctx.getPosition() because it will be set with lookahead's inner expression positions
                 * This will be reset after this update to the position just before lookahead match
                 */
                result.getStartPosition(), 
                result.getStartPosition());
            
            return finalResult;
        });
        
        return wrappedExpression;
    }

	public static Expression optional(Expression expression)
	{
	    Objects.requireNonNull(expression);
        
        WrappedExpression wrappedExpression = new WrappedExpression("optional", expression);
        wrappedExpression.setProcessor((ctx, result) -> {
            // Copy and modify wrapped expression result
            ParsedExpression finalResult = new ParsedExpression(wrappedExpression);
            
            int startPosition = result.getStartPosition();
            int endPosition = startPosition;
            String processedValue = null;
            String matchingValue = null;
            if(result.matches()) {
                endPosition = result.getEndPosition();
                processedValue = result.getProcessedValue();
                matchingValue = result.getMatchingValue();
            }
            finalResult.update(true, 
                processedValue,
                matchingValue,
                startPosition, 
                endPosition);
            
            return finalResult;
        });
        
        return wrappedExpression;
	}
	
	public static Expression greedy(Expression expression)
	{
	    return new GreedyExpression(expression);
	}
	
	public static Expression named(String name, Expression expression)
	{
	    Objects.requireNonNull(expression);
        
        WrappedExpression wrappedExpression = new WrappedExpression(name, expression);
        wrappedExpression.setProcessor((ctx, result) -> {
            // Copy wrapped expression result
            ParsedExpression finalResult = new ParsedExpression(wrappedExpression);
            finalResult.update(result.matches(), 
                result.getProcessedValue(),
                result.getMatchingValue(),
                result.getStartPosition(), 
                result.getEndPosition());
            return finalResult;
        });
        
        return wrappedExpression;
	}

	public static Expression capture(String varName, Expression expression)
	{
	    Objects.requireNonNull(expression);
        
        WrappedExpression wrappedExpression = new WrappedExpression("capture", expression);
        // Override default provider adding var name to expression string
        wrappedExpression.setToStringInfoProvider(() -> varName + " = " + expression.toString());
        wrappedExpression.setProcessor((ctx, result) -> {
            if(result.matches()) {
                ctx.addCapturedValue(varName, result.getMatchingValue());
            }
            
            // Copy wrapped expression result
            ParsedExpression finalResult = new ParsedExpression(wrappedExpression);
            finalResult.update(result.matches(), 
                    result.getProcessedValue(),
                    result.getMatchingValue(), 
                    result.getStartPosition(), 
                    result.getEndPosition());
            return finalResult;
        });
        
        return wrappedExpression;
	}
	
	public static Expression singleChar(Character character)
	{
		return singleCharIn(character);
	}
	
	public static Expression singleCharIn(Character... characters)
	{
	    Objects.requireNonNull(characters);
	    
	    Set<Character> set = new HashSet<>(Arrays.asList(characters));
	    PlainExpression expression = new PlainExpression("singleCharIn", ctx -> set.contains(ctx.getCurrentChar()));
	    expression.setToStringInfoProvider(set::toString);
        
        return expression;
	}
	
	public static Expression singleCharBetween(char from, char to)
    {
        PlainExpression expression = new PlainExpression("singleCharBetween", ctx -> from <= ctx.getCurrentChar() && ctx.getCurrentChar() <= to);
        expression.setToStringInfoProvider(() -> "[ " + from + ", " + to + " ]");
        
        return expression;
    }
	
	public static Expression charsIn(Character... characters)
    {
	    Objects.requireNonNull(characters);
        
        Set<Character> set = new HashSet<>(Arrays.asList(characters));
        PlainExpression expression = new PlainExpression("charsIn", ctx -> set.contains(ctx.getCurrentChar()));
        expression.setToStringInfoProvider(set::toString);
        expression.setGreedy(true);
        
        return expression;
    }
	
	public static Expression charsBetween(char from, char to)
    {
        PlainExpression expression = new PlainExpression("charsBetween", ctx -> from <= ctx.getCurrentChar() && ctx.getCurrentChar() <= to);
        expression.setToStringInfoProvider(() -> "[ " + from + ", " + to + " ]");
        expression.setGreedy(true);
        
        return expression;
    }
	
	public static Expression anyChar()
	{
	    return new PlainExpression("anyChar", ctx -> true);
	}
	
	public static Expression exactly(String string)
	{
	    Objects.requireNonNull(string);
	    
	    Predicate<ParsingExpressionContext> predicate = ctx -> {
	        String currentValue = ctx.getProcessedValue();
	        String nextValue = currentValue + ctx.getCurrentChar();
	        
	        return string.equals(nextValue) || string.startsWith(nextValue); 
	    };
	    
	    PlainExpression expression = new PlainExpression("exactly", predicate);
	    expression.setToStringInfoProvider(() -> string);
	    expression.setGreedy(true);
	    
	    return expression;
	}
	
	/**
	 * Matches values in a-zA-Z0-9<br/>
	 * Greedy expression
	 */
	public static Expression singleAsciiAlphanumeric()
	{
        Predicate<ParsingExpressionContext> predicate = ctx -> {
            char currentChar = ctx.getCurrentChar();
            return 'a' <= currentChar && currentChar <= 'z'
                || 'A' <= currentChar && currentChar <= 'Z'
                || '0' <= currentChar && currentChar <= '9';
        };
        
        PlainExpression expression = new PlainExpression("singleAsciiAlphanumeric", predicate);
        
        return expression;
	}
	
	/**
	 * Matches printable values. E.G: <br/>
	 * <pre>
	 * 'a' = true
     * 'A' = true
     * '3' = true
     * '-' = true
     * '\n = false
     * '&copy;' = false
     * </pre>
     * 
     * Greedy expression
     */
    public static Expression singleAsciiPrintable() 
    {
        Predicate<ParsingExpressionContext> predicate = ctx -> {
            char currentChar = ctx.getCurrentChar();
            return currentChar >= 32 && currentChar < 127;
        };
        
        PlainExpression expression = new PlainExpression("singleAsciiPrintable", predicate);
        
        return expression;
    }
	
	/*-----------------*
	 * EXPRESSIONS
	 *-----------------*/
	public static abstract class Expression
	{
	    protected String name;
	    protected Supplier<String> toStringInfoProvider;
	    
	    Expression(String name)
	    {
	        this.name = name;
	    }
	    
	    public String getName()
	    {
	        return name;
	    }
	    
	    public abstract ParsedExpression accept(ParsingContext ctx);
	    
	    @Override
	    public String toString()
	    {
	        StringBuilder resultBuilder = new StringBuilder(name)
	                .append("(");
	        if(toStringInfoProvider!=null) {
	            resultBuilder.append(toStringInfoProvider.get());
	        }
	        resultBuilder.append(")");
	        
	        return resultBuilder.toString();
	    }
	    
	    void setToStringInfoProvider(Supplier<String> toStringInfoProvider)
	    {
	        this.toStringInfoProvider = toStringInfoProvider;
	    }
	}
	
	static class PlainExpression extends Expression
	{
	    protected String name;
	    protected Predicate<ParsingExpressionContext> predicate;
	    protected boolean greedy = false;
	    
	    PlainExpression(String name, Predicate<ParsingExpressionContext> predicate)
        {
	        super(name);
            this.predicate = predicate;
        }

        public boolean isGreedy()
        {
            return greedy;
        }
        
        protected PlainExpression setGreedy(boolean greedy)
        {
            this.greedy = greedy;
            return this;
        }
	    
        @Override
        public ParsedExpression accept(ParsingContext ctx)
        {
            ParsedExpression result = ctx.getParser().visit(ctx, this);
            ctx.updatePosition(result);
            
            return result;
        }
        
        public boolean test(ParsingExpressionContext ctx)
        {
            return predicate.test(ctx);
        }
	}
	
	static class WrappedExpression extends Expression
	{
	    protected Expression expression; 
	    protected BiFunction<ParsingContext, ParsedExpression, ParsedExpression> processor;
	    
	    WrappedExpression(String name, Expression expression)
	    {
	        super(name);
	        this.expression = expression;
	        // Default
	        setToStringInfoProvider(expression::toString);
	    }
	    
	    public WrappedExpression setProcessor(BiFunction<ParsingContext, ParsedExpression, ParsedExpression> processor)
        {
            this.processor = processor;
            return this;
        }
	    
	    @Override
        public ParsedExpression accept(ParsingContext ctx)
        {
	        ParsedExpression expressionResult = expression.accept(ctx);
	        ParsedExpression finalResult = processor.apply(ctx, expressionResult);
	        finalResult.addNestedParsedExpression(expressionResult);
	        
	        ctx.updatePosition(finalResult);
	        
	        return finalResult;
        }
	}
	
	static class GreedyExpression extends Expression
    {
        protected Expression expression; 
        
        GreedyExpression(Expression expression)
        {
            super("greedy");
            this.expression = expression;
            setToStringInfoProvider(expression::toString);
        }
        
        @Override
        public ParsedExpression accept(ParsingContext ctx)
        {
            boolean follow = true;
            boolean matches = false;
            int startPosition = ctx.getPosition();
            int endPosition = ctx.getPosition();
            StringBuilder processedValueBuilder = new StringBuilder();
            StringBuilder matchingValueBuilder = new StringBuilder();
            while(follow) {
                ParsedExpression expressionResult = expression.accept(ctx);
                follow = expressionResult.matches();
                matches = matches || expressionResult.matches();
                processedValueBuilder.append(expressionResult.getProcessedValue());
                if(expressionResult.matches()) {
                    matchingValueBuilder.append(expressionResult.getMatchingValue());
                    endPosition = expressionResult.getEndPosition();
                }
            }
            
            ParsedExpression finalResult = new ParsedExpression(this);
            finalResult.update(matches, 
                    processedValueBuilder.toString(),
                    matchingValueBuilder.toString(),
                    startPosition, 
                    endPosition);
            
            ctx.updatePosition(finalResult);
            
            return finalResult;
        }
    }
	
	static class EndExpression extends Expression
    {
        EndExpression()
        {
            super("end");
        }
        
        @Override
        public ParsedExpression accept(ParsingContext ctx)
        {
            boolean exhausted = ctx.getParser().isExhaustedAtPosition(ctx, ctx.getPosition());
            
            ParsedExpression result = new ParsedExpression(this);
            result.setStartPosition(ctx.getPosition());
            result.setEndPosition(ctx.getPosition());
            result.setMatch(exhausted);
            
            return result;
        }
    }
	
	static abstract class ComposedExpression extends Expression
	{
	    protected List<Expression> nestedExpressions = new ArrayList<>(); 
	    
	    ComposedExpression(String name)
        {
            super(name);
            this.toStringInfoProvider = this::toStringInfoSupplier;
        }
	    
        public void addExpressions(Expression... expressions)
        {
            nestedExpressions.addAll(Arrays.asList(expressions));
        }
        
        protected String toStringInfoSupplier()
        {
            String nestedExpressionsString = nestedExpressions.stream()
                    .map(Expression::toString)
                    .collect(Collectors.joining(", "));
            
            return new StringBuilder()
                    .append("[")
                    .append(nestedExpressionsString)
                    .append("]")
                    .toString();
        }
	}
	
	static class SequenceExpression extends ComposedExpression
    {
	    SequenceExpression()
        {
            super("sequence");
        }
	    
        @Override
        public ParsedExpression accept(ParsingContext ctx)
        {
            ParsedExpression finalResult = new ParsedExpression(this);
            finalResult.setStartPosition(ctx.getPosition());
            
            boolean matches = true;
            StringBuilder processedValueBuilder = new StringBuilder();
            StringBuilder matchingValueBuilder = new StringBuilder();
            int endPosition = ctx.getPosition();
            Iterator<Expression> iterator = nestedExpressions.iterator();
            while(matches && iterator.hasNext()) {
                Expression expression = iterator.next();
                ParsedExpression expressionResult = expression.accept(ctx);
                matches = expressionResult.matches();
                String processedValue = expressionResult.getProcessedValue();
                // Prevent optional expression to add "null" to result value
                if(processedValue!=null) {
                    processedValueBuilder.append(expressionResult.getProcessedValue());
                }
                if(matches) {
                    endPosition = expressionResult.getEndPosition();
                    String matchingValue = expressionResult.getMatchingValue();
                    // Prevent optional expression to add "null" to result value
                    if(matchingValue!=null) {
                        matchingValueBuilder.append(matchingValue);
                    }
                }
                finalResult.addNestedParsedExpression(expressionResult);
            }
            finalResult.setMatch(matches);
            finalResult.setProcessedValue(processedValueBuilder.toString());
            
            if(finalResult.matches()) {
                finalResult.setEndPosition(endPosition);
                finalResult.setMatchingValue(matchingValueBuilder.toString());
                ctx.updatePosition(finalResult);
            }
            
            return finalResult;
        }
    }
	
	static class ChoiceExpression extends ComposedExpression
    {
	    ChoiceExpression()
        {
	        super("choice");
        }
	    
        @Override
        public ParsedExpression accept(ParsingContext ctx)
        {
            ParsedExpression finalResult = new ParsedExpression(this);
            
            Iterator<Expression> iterator = nestedExpressions.iterator();
            while(!finalResult.matches() && iterator.hasNext()) {
                Expression expression = iterator.next();
                ParsedExpression expressionResult = expression.accept(ctx);
                if(expressionResult.matches()) {
                    finalResult.update(expressionResult.matches(),
                            expressionResult.getProcessedValue(),
                            expressionResult.getMatchingValue(), 
                            expressionResult.getStartPosition(), 
                            expressionResult.getEndPosition());
                }
                finalResult.addNestedParsedExpression(expressionResult);
            }
            
            if(finalResult.matches()) {
                ctx.updatePosition(finalResult);
            }
            
            return finalResult;
        }
    }
}
