package com.bitsmi.yggdrasil.commons.util;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

/**
 *
 * @author Antonio Archilla
 * @since 1.6.0
 */
@FunctionalInterface
public interface IProgressTrackerListener 
{
	void onTaskProgression(IProgressTrackerTask task, ProgressInfo info);

    default IProgressTrackerListener andThen(IProgressTrackerListener after) 
    {
        Objects.requireNonNull(after);
        return (task, info) -> { onTaskProgression(task, info); after.onTaskProgression(task, info); };
    }
    
    public static class ProgressInfo
    {
    	private String message;
    	private Map<String, Object> attributes = null;

		public String getMessage() 
		{
			return message;
		}

		public ProgressInfo setMessage(String message) 
		{
			this.message = message;
			return this;
		}

		public Map<String, Object> getAttributes() 
		{
			return Optional.ofNullable(attributes)
					.orElse(Collections.emptyMap());
		}

		public ProgressInfo setAttributes(Map<String, Object> attributes) 
		{
			this.attributes = attributes;
			return this;
		}
		
		public ProgressInfo putAttribute(String key, Object value)
		{
			this.attributes = Optional.ofNullable(this.attributes)
					.orElse(new HashMap<>());
			this.attributes.put(key, value);
			
			return this;
		}
    }
}	