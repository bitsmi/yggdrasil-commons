package com.bitsmi.yggdrasil.commons.util;

import java.util.Optional;
import java.util.function.Function;
import java.util.function.Supplier;

import com.bitsmi.yggdrasil.commons.util.UncheckedFunctionals.ThrowingSupplier;

/**
 * @since 1.2.0
 */
public class Lazy<T> 
{
	private Supplier<T> supplier;
	private T value;
	private boolean evaluated = false;
	
	private Lazy() {}
	
	public static <T> Lazy<T> of(Supplier<T> supplier)
	{
		Lazy<T> lazy = new Lazy<>();
		lazy.supplier = supplier;
		
		return lazy;
	}
	
	/**
     * @since 1.8.0, 2.1.0 
     */
	public static <T, E extends Exception> Lazy<T> ofThrowing(ThrowingSupplier<T, E> supplier)
    {
	    return of(UncheckedFunctionals.uncheckedSupplier(supplier));        
    }
	
	public boolean isEvaluated()
	{
		return evaluated;
	}
	
	public T get()
	{
		if(!evaluated) {
			computeValue();
		}
		
		return value;
	}
	
	public Optional<T> optional()
	{
		return Optional.ofNullable(get());
	}
	
	/**
     * @since 1.8.0, 2.1.0 
     */
    public <U> Lazy<U> map(Function<T, U> mapper)
    {
        return Lazy.of(() -> mapper.apply(value));
    }
    
    /**
     * @since 1.8.0, 2.1.0 
     */
    public Lazy<T> refresh()
    {
        computeValue();
        return this;
    }
    
    private void computeValue()
    {
        value = supplier.get();
        evaluated = true;
    }
}
