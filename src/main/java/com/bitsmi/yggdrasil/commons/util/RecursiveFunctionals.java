package com.bitsmi.yggdrasil.commons.util;

import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.function.Consumer;
import java.util.function.Function;

/**
 * @since 1.2.0
 */
public class RecursiveFunctionals 
{
	public static <T> Consumer<T> consumer(Consumer<T> consumer, Function<T, Collection<T>> extractor)
	{
		return x -> {
			RecursiveConsumer<Consumer<T>, T> helper = (recursive, elem) -> {
				Collection<T> children = Optional.ofNullable(extractor.apply(elem)).orElse(Collections.emptyList());				
				consumer.accept(elem);
				for(T child:children) {
					recursive.accept(recursive, child);
				}
			};
			helper.accept(helper, x);
		};
	}
	
	public static <T> void sortElementListHierarchy(List<T> collection, Comparator<T> comparator, Function<T, List<T>> extractor)
	{
		Consumer<T> consumer = buildHierarchySortRecursiveConsumer(comparator, extractor);

		// Sort top elements
		collection.sort(comparator);
		// Sort child elements
		for(T elem:collection) {
			consumer.accept(elem);
		}
	}
	
	public static <T> void sortElementHierarchy(T elem, Comparator<T> comparator, Function<T, List<T>> extractor)
	{
		Consumer<T> consumer = buildHierarchySortRecursiveConsumer(comparator, extractor);
		consumer.accept(elem);
	}
	
	private static <T> Consumer<T> buildHierarchySortRecursiveConsumer(Comparator<T> comparator, Function<T, List<T>> extractor)
	{
		return RecursiveFunctionals.<T>consumer(node -> {
			List<T> children = extractor.apply(node);
			if(children!=null) {
				children.sort(comparator);
			}
		},
		// Generic types conversion
		node -> (Collection<T>)extractor.apply(node));		
	}
	
	@FunctionalInterface
	public static interface RecursiveConsumer<C, T>
	{
		public void accept(RecursiveConsumer<C, T> recursiveConsumer, T elem);
	}
}
