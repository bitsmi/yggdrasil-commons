package com.bitsmi.yggdrasil.commons.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Parameter;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Properties;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Stream;

public class PropertiesEvaluator 
{
	private static final Pattern EXPRESSION_PATTERN = Pattern.compile("(.*)\\$\\{(.*)\\}(.*)");
	
	private Map<String, String> properties = new HashMap<>(); 
	
	public PropertiesEvaluator addFilePropertiesSource(File file) throws IOException
	{
	    Properties properties = new Properties();        
        try(FileInputStream fis = new FileInputStream(file)){
            properties.load(fis);
        }
        addProperties(properties);
        
        return this;
	}
	
	public PropertiesEvaluator addFilePropertiesSourceFromClasspath(String classpathResource) throws IOException
    {
        try(InputStream is = getClass().getResourceAsStream(classpathResource)){
            addInputStreamPropertiesSource(is);
        }
        
        return this;
    }
	
	public PropertiesEvaluator addInputStreamPropertiesSource(InputStream is) throws IOException
    {
        Properties properties = new Properties();        
        properties.load(is);
        addProperties(properties);
        
        return this;
    }
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public PropertiesEvaluator addProperties(Properties properties)
	{
		this.properties.putAll((Map)properties);
		return this;
	}
	
	public PropertiesEvaluator addProperties(Map<String, String> properties)
	{
		this.properties.putAll(properties);
		return this;
	}
	
	public void injectValues(Object object) throws ReflectiveOperationException
	{
		injectAttributes(object);
		injectMethods(object);
	}
	
	private void injectAttributes(Object object)
	{
		List<Field> annotatedFields = ReflectionUtils.scanAnnotatedFields(object.getClass(), Value.class);
		for(Field field:annotatedFields) {
			if(!field.getType().isAssignableFrom(String.class)) {
				throw new IllegalArgumentException(String.format("Value no assignable. Field must be of type String [%s]", field.getName()));
			}
			
			Value annotation = field.getAnnotation(Value.class);
			
			String value = null;
			try{
				value = evaluateAnnotation(annotation);
			}
			catch(IllegalArgumentException e) {
				throw new IllegalArgumentException(String.format("Illegal annotation on field (%s): %s", field.getName(), e.getMessage()), e);
			}
			
			boolean accessible = field.isAccessible();
			if(!accessible) {
				field.setAccessible(true);
			}
			try {
				field.set(object, value);
			}
			catch(IllegalAccessException e) {
				// This cannot happen at this point
			}
			finally {
				if(!accessible) {
					field.setAccessible(accessible);
				}
			}
		}
	}
	
	private void injectMethods(Object object) throws ReflectiveOperationException
	{
		Set<Method> annotatedMethods = new HashSet<>();
		annotatedMethods.addAll(ReflectionUtils.scanAnnotatedMethods(object.getClass(), Value.class));
		annotatedMethods.addAll(ReflectionUtils.scanMethodsWithAnnotatedParameters(object.getClass(), Value.class));
		
		for(Method method:annotatedMethods) {
			Class<?>[] parameterTypes = method.getParameterTypes();
			boolean methodAnnotated = method.isAnnotationPresent(Value.class);
			int parameterCount = method.getParameterCount();
			boolean allParametersAnnotated = ReflectionUtils.assertAllParametersAnnotated(method, Value.class);			
			// Methods must have at least 1 parameter
			if(methodAnnotated && parameterCount==0) {
				throw new IllegalArgumentException(String.format("Value annotated methods must have at least 1 parameter [%s]",
						method.toString()));
			}			
			// Methods must be annotated at parameter or method level, but only one annotation is allowed
			boolean firstParameterAnnotated = ReflectionUtils.assertParameterAnnotated(method, 0, Value.class);
			if(methodAnnotated && firstParameterAnnotated) {
				throw new IllegalArgumentException(String.format("Method must only specify one Value annotation at parameter or method level for the first parameter [%s]",
						method.toString()));
			}
			// Methods annotated at parameter level, must have all it's parameters annotated
			else if (parameterCount>1 && !allParametersAnnotated) {
				throw new IllegalArgumentException(String.format("Method must specify Value annotations for all it's parameters [%s]",
						method.toString()));
			}
			
			// All parameters must be compatible with String type
			Optional<Class<?>> optNonAssignableParams = Stream.of(parameterTypes)
					.filter(type -> !type.isAssignableFrom(String.class))
					.findAny();
			if(optNonAssignableParams.isPresent()) {
				throw new IllegalArgumentException(String.format("Found non assignable value for object. Method parameter must be of type String [%s - %s]",
						method.toString(),
						optNonAssignableParams.get().getName()));
			}
			
			if(methodAnnotated) {
				Value annotation = method.getAnnotation(Value.class);
				String value = evaluateAnnotation(annotation);
				if(!method.isAccessible()) {
					method.setAccessible(true);
				}
				method.invoke(object, value);
			}
			else {
				Object[] values = new String[method.getParameterCount()];
				Parameter[] parameters = method.getParameters();
				for(int i=0; i<parameters.length; i++) {
					Parameter parameter = parameters[i];
					Value annotation = parameter.getAnnotation(Value.class);
					String value = evaluateAnnotation(annotation);
					values[i] = value;
				}
				if(!method.isAccessible()) {
					method.setAccessible(true);
				}
				method.invoke(object, values);
			}			
		}
	}
	
	private String evaluateAnnotation(Value annotation)
	{
		String property = annotation.property();
		String defaultValue = annotation.defaultValue();
		
		if("".equals(property)) {
			property = annotation.value();
			if("".equals(property)) {
				throw new IllegalArgumentException(String.format("Property must be specified")); 
			}
		}
		String key = evaluateString(property);
		// Key may contain other replacement placeholders
		String oldKey = property;
		while(!key.equals(oldKey)) {
			oldKey = key;
			key = evaluateString(key);
		}
		
		String value = properties.get(key);
		if(value!=null) {
			value = evaluateString(value);
		}
		else if(value==null && !Value.EXPRESSION_DEFAULT_VALUE.equals(defaultValue)) {
			value = evaluateString(defaultValue);
		}
		
		if(value==null) {
			throw new IllegalArgumentException(String .format("Value not found for property (%s)", key));
		}
		
		return value;
	}
	
	private String evaluateString(String str)
	{
		String evaluatedString = str;
		
		Matcher matcher = EXPRESSION_PATTERN.matcher(str);
		if(matcher.matches()) {
			String prefix = matcher.group(1);
			String key = matcher.group(2);
			String sufix = matcher.group(3); 
			String value = properties.get(key);
			if(value==null) {
				throw new IllegalArgumentException(String .format("Value not found for property (%s)", key));
			}
			evaluatedString = prefix + value + sufix;
		}
		
		return evaluatedString;
	}
}
