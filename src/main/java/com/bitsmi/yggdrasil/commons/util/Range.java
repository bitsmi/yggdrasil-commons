package com.bitsmi.yggdrasil.commons.util;

import static com.bitsmi.yggdrasil.commons.util.Expressions.DIGIT;
import static com.bitsmi.yggdrasil.commons.util.Expressions.WHITESPACE;
import static com.bitsmi.yggdrasil.commons.util.Expressions.anyChar;
import static com.bitsmi.yggdrasil.commons.util.Expressions.capture;
import static com.bitsmi.yggdrasil.commons.util.Expressions.choice;
import static com.bitsmi.yggdrasil.commons.util.Expressions.greedy;
import static com.bitsmi.yggdrasil.commons.util.Expressions.lookahead;
import static com.bitsmi.yggdrasil.commons.util.Expressions.named;
import static com.bitsmi.yggdrasil.commons.util.Expressions.not;
import static com.bitsmi.yggdrasil.commons.util.Expressions.optional;
import static com.bitsmi.yggdrasil.commons.util.Expressions.sequence;
import static com.bitsmi.yggdrasil.commons.util.Expressions.singleAsciiPrintable;
import static com.bitsmi.yggdrasil.commons.util.Expressions.singleChar;
import static com.bitsmi.yggdrasil.commons.util.Expressions.singleCharIn;

import java.util.Objects;
import java.util.function.Function;

import com.bitsmi.yggdrasil.commons.util.ExpressionParser.ParseResult;
import com.bitsmi.yggdrasil.commons.util.ExpressionParser.ParsedExpression;
import com.bitsmi.yggdrasil.commons.util.Expressions.Expression;

/**
 * @since 1.8.0, 2.1.0
 */
public class Range <T extends Comparable<T>>
{
    public static final RangeExpressionFormat EXPRESSION_DEFAULT_FORMAT;
    public static final RangeExpressionFormat EXPRESSION_SIMPLIFIED_FORMAT;
    
    static {
        EXPRESSION_DEFAULT_FORMAT = RangeExpressionFormatBuilder.newInstance()
                .withOpenBoundSymbols("(", ")")
                .withClosedBoundSymbols("[", "]")
                .withInfiniteBoundSymbol("*")
                .withSeparator(", ")
                .build();
        
        EXPRESSION_SIMPLIFIED_FORMAT = RangeExpressionFormatBuilder.newInstance()
                .withOpenBoundSymbols("", "")
                .withClosedBoundSymbols("", "")
                .withInfiniteBoundSymbol("*")
                .withSeparator("..")
                .build();
    }
    
    private static final ExpressionParser EXPRESSION_PARSER = createExpressionParser();
    
    private IBound<T> lowerBound;
    private IBound<T> upperBound;

    private Range(IBound<T> lowerBound, IBound<T> upperBound)
    {
        this.lowerBound = lowerBound;
        this.upperBound = upperBound;
        
        // Validate that the bounds are valid
        String errorMessage = this.validate();
        if(errorMessage!=null) {
            throw new IllegalArgumentException("Invalid Range: " + errorMessage);
        }
    }
    
    public T getLowerBound()
    {
        if(lowerBound.isInfinite()) {
            return null;
        }
        
        T value = ((FiniteBound<T>)lowerBound).getValue();
        return value;
    }
    
    public boolean isLowerBoundOpen()
    {
        return lowerBound.isOpen();
    }
    
    public T getUpperBound()
    {
        if(upperBound.isInfinite()) {
            return null;
        }
        
        T value = ((FiniteBound<T>)upperBound).getValue();
        return value;
    }
    
    public boolean isUpperBoundOpen()
    {
        return lowerBound.isOpen();
    }
    
    public boolean contains(T value)
    {
        return lowerBound.contains(value)
                && upperBound.contains(value);
    }
    
    public String format(Function<T, String> mapper)
    {
        return this.format(EXPRESSION_DEFAULT_FORMAT, mapper);
    }
    
    public String format(RangeExpressionFormat format, Function<T, String> mapper)
    {
        Objects.requireNonNull(format);
        Objects.requireNonNull(mapper);
        
        StringBuilder builder = new StringBuilder();
        /* Lower bound */
        if(lowerBound.isOpen()) {
            builder.append(format.openLowerBoundSymbol);
        }
        else {
            builder.append(format.closedLowerBoundSymbol);
        }
        
        if(lowerBound.isInfinite()) {
            builder.append("*");
        }
        else {
            T value = ((FiniteBound<T>)lowerBound).getValue();
            String strValue = mapper.apply(value);
            if(StringUtils.containsWhitespace(strValue)) {
                builder.append("\"").append(strValue).append("\"");
            }
            else {
                builder.append(strValue);
            }
        }
        
        builder.append(format.separator);
        
        /* Upper bound */
        if(upperBound.isInfinite()) {
            builder.append(format.infiniteBoundSymbol);
        }
        else {
            T value = ((FiniteBound<T>)upperBound).getValue();
            String strValue = mapper.apply(value);
            if(StringUtils.containsWhitespace(strValue)) {
                builder.append("\"").append(strValue).append("\"");
            }
            else {
                builder.append(strValue);
            }
        }
        
        if(upperBound.isOpen()) {
            builder.append(format.openUpperBoundSymbol);
        }
        else {
            builder.append(format.closedUpperBoundSymbol);
        }
        
        return builder.toString();
    }
    
    public static Range<Integer> parseIntegerExpression(String expression)
    {
        return parseExpression(expression, str -> Integer.valueOf(str));
    }
    
    public static Range<String> parseStringExpression(String expression)
    {
        return parseExpression(expression, Function.identity());
    }
    
    public static <T extends Comparable<T>> Range<T> parseExpression(String expression, Function<String, T> mapper)
    {
        Objects.requireNonNull(expression);
        
        expression = expression.trim();
        ParseResult parseResult = EXPRESSION_PARSER.parse(expression);
        if(!parseResult.isInputFullyMatched()
                && parseResult.getMatchedParsedExpressions().size()!=1) {
            throw new IllegalArgumentException("Invalid expression: Does not match any valid pattern");
        }
        ParsedExpression parsedExpression = parseResult.getMatchedParsedExpressions().get(0);
        String lowerBoundDelimiter = null;
        String lowerStrValue = null;
        String upperBoundDelimiter = null;
        String upperStrValue = null;
        boolean genericExpression = false; 
        if("GENERIC_EXPR".equals(parsedExpression.getExpression().getName())) {
            genericExpression = true;
            lowerBoundDelimiter = parseResult.getCapturedValues("LOWER_BOUND").get(0);
            upperBoundDelimiter = parseResult.getCapturedValues("UPPER_BOUND").get(0);
        }
        lowerStrValue = parseResult.getCapturedValues("LOWER_VALUE").get(0);
        upperStrValue = parseResult.getCapturedValues("UPPER_VALUE").get(0);
        
        IBound<T> lowerBound;
        IBound<T> upperBound;
        if(genericExpression) {
            // Infinite bounds cannot be closed
            if("[".equals(lowerBoundDelimiter) && "*".equals(lowerStrValue)
                    || "]".equals(lowerBoundDelimiter) && "*".equals(lowerStrValue)) {
                throw new IllegalArgumentException("Invalid expression: Infinite bounds cannot be closed");
            }
            // String "" at the beggining / end of bounds expressions
            if(lowerStrValue.startsWith("\"") && lowerStrValue.endsWith("\"")) {
                lowerStrValue = lowerStrValue.substring(1, lowerStrValue.length()-1);                
            }
            if(upperStrValue.startsWith("\"") && upperStrValue.endsWith("\"")) {
                upperStrValue = upperStrValue.substring(1, upperStrValue.length()-1);                
            }
            
            if("*".equals(lowerStrValue)) {
                lowerBound = new InfiniteBound<T>();
            }
            else {
                T lowerBoundValue = mapper.apply(lowerStrValue);
                lowerBound = new LowerBound<T>("(".equals(lowerBoundDelimiter), lowerBoundValue);
            }
            
            if("*".equals(upperStrValue)) {
                upperBound = new InfiniteBound<T>();
            }
            else {
                T upperBoundValue = mapper.apply(upperStrValue);
                upperBound = new UpperBound<T>(")".equals(upperBoundDelimiter), upperBoundValue);
            }
        }
        // Simplified format
        else {
            // String "" at the beggining / end of bounds expressions
            if(lowerStrValue.startsWith("\"") && lowerStrValue.endsWith("\"")) {
                lowerStrValue = lowerStrValue.substring(1, lowerStrValue.length()-1);                
            }
            if(upperStrValue.startsWith("\"") && upperStrValue.endsWith("\"")) {
                upperStrValue = upperStrValue.substring(1, upperStrValue.length()-1);                
            }
            
            if("*".equals(lowerStrValue)) {
                lowerBound = new InfiniteBound<T>();
            }
            else {
                T lowerBoundValue = mapper.apply(lowerStrValue);
                lowerBound = new LowerBound<T>(false, lowerBoundValue);
            }
            
            if("*".equals(upperStrValue)) {
                upperBound = new InfiniteBound<T>();
            }
            else {
                T upperBoundValue = mapper.apply(upperStrValue);
                upperBound = new UpperBound<T>(false, upperBoundValue);
            }
        }
        
        return new Range<>(lowerBound, upperBound);
    }

    public static <T extends Comparable<T>> Range<T> open(T lowerValue, T upperValue)
    {
        Objects.requireNonNull(lowerValue);
        Objects.requireNonNull(upperValue);
        
        LowerBound<T> lowerBound = new LowerBound<>(true, lowerValue);
        UpperBound<T> upperBound = new UpperBound<T>(true, upperValue);
        
        return new Range<>(lowerBound, upperBound);
    }
    
    public static <T extends Comparable<T>> Range<T> closed(T lowerValue, T upperValue)
    {
        Objects.requireNonNull(lowerValue);
        Objects.requireNonNull(upperValue);
        
        LowerBound<T> lowerBound = new LowerBound<>(false, lowerValue);
        UpperBound<T> upperBound = new UpperBound<T>(false, upperValue);
        
        return new Range<>(lowerBound, upperBound);
    }
    
    public static <T extends Comparable<T>> Range<T> openClosed(T lowerValue, T upperValue)
    {
        Objects.requireNonNull(lowerValue);
        Objects.requireNonNull(upperValue);
        
        LowerBound<T> lowerBound = new LowerBound<>(true, lowerValue);
        UpperBound<T> upperBound = new UpperBound<T>(false, upperValue);
        
        return new Range<>(lowerBound, upperBound);
    }
    
    public static <T extends Comparable<T>> Range<T> closedOpen(T lowerValue, T upperValue)
    {
        Objects.requireNonNull(lowerValue);
        Objects.requireNonNull(upperValue);
        
        LowerBound<T> lowerBound = new LowerBound<>(false, lowerValue);
        UpperBound<T> upperBound = new UpperBound<T>(true, upperValue);
        
        return new Range<>(lowerBound, upperBound);
    }
    
    /**
     * Return a range that contains values strictly greater than {@code value} 
     * @param value T - Reference value for the Range
     * @return
     */
    public static <T extends Comparable<T>> Range<T> greaterThan(T value)
    {
        Objects.requireNonNull(value);
        
        LowerBound<T> lowerBound = new LowerBound<>(true, value);
        InfiniteBound<T> upperBound = new InfiniteBound<T>();
        
        return new Range<>(lowerBound, upperBound);
    }
    
    /**
     * Return a range that contains values greater than or equals to {@code value} 
     * @param value T - Reference value for the Range
     * @return
     */
    public static <T extends Comparable<T>> Range<T> atLeast(T value)
    {
        Objects.requireNonNull(value);
        
        LowerBound<T> lowerBound = new LowerBound<>(false, value);
        InfiniteBound<T> upperBound = new InfiniteBound<T>();
        
        return new Range<>(lowerBound, upperBound);
    }
    
    /**
     * Return a range that contains values strictly less than {@code value} 
     * @param value T - Reference value for the Range
     * @return
     */
    public static <T extends Comparable<T>> Range<T> lessThan(T value)
    {
        Objects.requireNonNull(value);
        
        InfiniteBound<T> lowerBound = new InfiniteBound<T>();
        UpperBound<T> upperBound = new UpperBound<>(true, value);
        
        return new Range<>(lowerBound, upperBound);
    }
    
    /**
     * Return a range that contains values less than or equals to {@code value} 
     * @param value T - Reference value for the Range
     * @return
     */
    public static <T extends Comparable<T>> Range<T> atMost(T value)
    {
        Objects.requireNonNull(value);
        
        InfiniteBound<T> lowerBound = new InfiniteBound<T>();
        UpperBound<T> upperBound = new UpperBound<>(true, value);
        
        return new Range<>(lowerBound, upperBound);
    }
    
    public static <T extends Comparable<T>> Range<T> all()
    {
        InfiniteBound<T> lowerBound = new InfiniteBound<T>();
        InfiniteBound<T> upperBound = new InfiniteBound<T>();
        
        return new Range<>(lowerBound, upperBound);
    }
    
    public static <T extends Comparable<T>> Range<T> exactly(T value)
    {
        LowerBound<T> lowerBound = new LowerBound<>(false, value);
        UpperBound<T> upperBound = new UpperBound<>(false, value);
        
        return new Range<>(lowerBound, upperBound);
    }
    
    String validate()
    {
        if(lowerBound.isInfinite() || upperBound.isInfinite()) {
            return null;
        }
        
        FiniteBound<T> finiteLowerBound = (FiniteBound<T>)lowerBound;
        FiniteBound<T> finiteUpperBound = (FiniteBound<T>)upperBound;
        int comparison = finiteLowerBound.getValue().compareTo(finiteUpperBound.getValue());
        
        // lower bound > upper bound
        if(comparison>0) {
            return "Range bounds are invalid";
        }
        // Bounds like (1, 1), [1, 1) or (1, 1] are invalid
        if(comparison==0 && (finiteLowerBound.isOpen() || finiteUpperBound.isOpen())) {
            return "Range bounds are not compatible";
        }
        
        return null;
    }
    
    static interface IBound<T extends Comparable<T>>
    {
        public boolean isOpen();
        public boolean isInfinite();
        public boolean contains(T value);
    }
    
    static abstract class FiniteBound<T extends Comparable<T>> implements IBound<T>
    {
        protected boolean open;
        protected T value;
        
        public FiniteBound(boolean open, T value)
        {
            this.open = open;
            this.value = value;
        }
        
        @Override
        public boolean isOpen()
        {
            return this.open;
        }
        
        public T getValue()
        {
            return value;
        }
        
        @Override
        public boolean isInfinite() 
        {
            return false;
        }
    }
    
    static class LowerBound<T extends Comparable<T>> extends FiniteBound<T>
    {
        public LowerBound(boolean open, T value)
        {
            super(open, value);
        }
        
        @Override
        public boolean contains(T value)
        {
            int comparison = this.value.compareTo(value);
            return comparison<0
                    || !this.open && comparison==0;
        }
    }
    
    static class UpperBound<T extends Comparable<T>> extends FiniteBound<T>
    {
        public UpperBound(boolean open, T value)
        {
            super(open, value);
        }
        
        @Override
        public boolean contains(T value)
        {
            int comparison = this.value.compareTo(value);
            return comparison>0
                    || !this.open && comparison==0;
        }
    }
    
    static class InfiniteBound<T extends Comparable<T>> implements IBound<T>
    {
        @Override
        public boolean isOpen()
        {
            return true;
        }
        
        @Override
        public boolean isInfinite()
        {
            return true;
        }
        
        @Override
        public boolean contains(T value)
        {
            return true;
        }
    }
    
    public static class RangeExpressionFormat
    {
        private String openLowerBoundSymbol;
        private String closedLowerBoundSymbol;
        private String openUpperBoundSymbol;
        private String closedUpperBoundSymbol;
        private String infiniteBoundSymbol;
        private String separator;
        
        RangeExpressionFormat() {  }
    }
    
    public static class RangeExpressionFormatBuilder
    {
        private String openLowerBoundSymbol;
        private String closedLowerBoundSymbol;
        private String openUpperBoundSymbol;
        private String closedUpperBoundSymbol;
        private String infiniteBoundSymbol;
        private String separator;
        
        private RangeExpressionFormatBuilder() {  }
        
        public static RangeExpressionFormatBuilder newInstance()
        {
            return new RangeExpressionFormatBuilder();
        }
        
        public RangeExpressionFormatBuilder withOpenBoundSymbols(String openLowerBoundSymbol, String openUpperBoundSymbol)
        {
            this.openLowerBoundSymbol = openLowerBoundSymbol;
            this.openUpperBoundSymbol = openUpperBoundSymbol;
            return this;
        }
        
        public RangeExpressionFormatBuilder withClosedBoundSymbols(String closedLowerBoundSymbol, String closedUpperBoundSymbol)
        {
            this.closedLowerBoundSymbol = closedLowerBoundSymbol;
            this.closedUpperBoundSymbol = closedUpperBoundSymbol;
            return this;
        }
        
        public RangeExpressionFormatBuilder withInfiniteBoundSymbol(String infiniteBoundSymbol)
        {
            this.infiniteBoundSymbol = infiniteBoundSymbol;
            return this;
        }
        
        public RangeExpressionFormatBuilder withSeparator(String separator)
        {
            this.separator = separator;
            return this;
        }
        
        public RangeExpressionFormat build()
        {
            Objects.requireNonNull(openLowerBoundSymbol);
            
            RangeExpressionFormat format = new RangeExpressionFormat();
            format.openLowerBoundSymbol = openLowerBoundSymbol;
            format.closedLowerBoundSymbol = closedLowerBoundSymbol;
            format.openUpperBoundSymbol = openUpperBoundSymbol;
            format.closedUpperBoundSymbol = closedUpperBoundSymbol;
            format.infiniteBoundSymbol = infiniteBoundSymbol;
            format.separator = separator;
            
            return format;
        }
    }
    
    private static final ExpressionParser createExpressionParser()
    {
        
        Expression NUMBER = Expressions.sequence(
                optional(singleChar('-')),
                greedy(DIGIT));
        Expression STRING = sequence(
            // Optional as value can contain only a single character before separator
            optional(greedy(
                sequence(
                    Expressions.singleAsciiPrintable(), 
                    lookahead(not(singleCharIn(' ', '.', ',', ']', ')')))
                )
            )),
            // Catch the last character before separator
            optional(
                sequence(
                    Expressions.singleAsciiPrintable(), 
                    lookahead(singleCharIn(' ', '.', ',', ']', ')'))
                )
            )
        );
        // Match String expressions surrounded by "
        Expression DELIMITED_STRING = sequence(
            singleChar('"'),
            // Optional as value can contain only a single character before separator
            optional(greedy(
                sequence(
                    singleAsciiPrintable(), 
                    lookahead(not(singleCharIn('"')))
                )
            )),
            // Catch the last character before string closing
            optional(
                sequence(
                    anyChar(), 
                    lookahead(singleCharIn('"'))
                )
            ),
            singleChar('"')
        );        
        Expression VALUE = choice(
                DELIMITED_STRING,
                NUMBER,
                STRING
        );

        // [-10, 100)
        Expression genericExpression = Expressions.named("GENERIC_EXPR", 
                sequence(
                    named("LOWER_BOUND", capture("LOWER_BOUND", singleCharIn('[', '('))),
                    optional(greedy(WHITESPACE)),
                    named("LOWER_VALUE", capture("LOWER_VALUE", choice(singleChar('*'), VALUE))),
                    optional(greedy(WHITESPACE)),
                    singleChar(','),
                    optional(greedy(WHITESPACE)),
                    named("UPPER_VALUE", capture("UPPER_VALUE", choice(singleChar('*'), VALUE))),
                    optional(greedy(WHITESPACE)),
                    named("UPPER_BOUND", capture("UPPER_BOUND", singleCharIn(']', ')')))
                )
        );
        // -10..100
        Expression simplifiedExpression = Expressions.named("SIMPLIFIED_EXPR", 
                sequence(
                    named("LOWER_VALUE", capture("LOWER_VALUE", choice(singleChar('*'), VALUE))),                    
                    optional(greedy(WHITESPACE)),
                    Expressions.exactly(".."),
                    optional(greedy(WHITESPACE)),
                    named("UPPER_VALUE", capture("UPPER_VALUE", choice(singleChar('*'), VALUE)))
                )
        );
        
        ExpressionParser parser = ExpressionParser.builder()
                .withExpressions(
                    genericExpression,  
                    simplifiedExpression
                )
                .build();
        
        return parser;
    }
}
