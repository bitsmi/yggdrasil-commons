package com.bitsmi.yggdrasil.commons.util;

import java.util.function.Function;

/**
 * @since 1.8.0, 2.1.0
 */
@FunctionalInterface
public interface ITransformer<T> 
{
    public <R> R by(Function<? super T, ? extends R> func);
}
