package com.bitsmi.yggdrasil.commons.util;

import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.util.Objects;

/**
 * @since 1.1.0
 */
public class ReflectiveCallHelper <T>
{
	private CallContext<T> context;
	
	private ReflectiveCallHelper(CallContext<T> context)
	{
		this.context = context;
	}
	
	public static <T> ReflectiveCallHelper<T> of(Class<T> clazz)
	{
		Objects.requireNonNull(clazz);
		
		CallContext<T> context = new CallContext<>();
		context.clazz = clazz;
		
		return new ReflectiveCallHelper<T>(context);
	}
	
	public ReflectiveCallInstance<T> withDefaultConstructor()
	{
		this.context.instantiateDefault = true;
		return new ReflectiveCallInstance<T>(this.context);
	}
	
	public ReflectiveCallInstance<T> withConstructor(Class<?>... types)
	{			
		this.context.instantiateDefault = false;
		this.context.constructorParamTypes = types;
		return new ReflectiveCallInstance<T>(this.context);
	}
	
	/**
	 * @since 1.5.0
	 */
	public ReflectiveCallInstance<T> withInstance(T instance)
	{
		Objects.requireNonNull(instance);
		
		this.context.instantiateDefault = true;
		this.context.instance = instance;
		return new ReflectiveCallInstance<T>(this.context);
	}
	
	public <R> R callStatic(String methodName) throws ReflectiveOperationException
	{
		Objects.requireNonNull(methodName);
		
		return callStatic(methodName, new Class<?>[] {}, new Object[] {});
	}
	
	@SuppressWarnings("unchecked")
	public <R> R callStatic(String methodName, Class<?>[] paramTypes, Object... params) throws ReflectiveOperationException
	{
		Objects.requireNonNull(methodName);
		CallContext.validateParameters(paramTypes, params);
		
		Method method = this.context.clazz.getMethod(methodName, paramTypes);
        Object result = method.invoke(null, params);
        
        return (R)result;
	}
	
	public static class ReflectiveCallInstance<T>
	{
		private CallContext<T> context;
		
		private ReflectiveCallInstance(CallContext<T> context)
		{
			this.context = context;
		}
		
		public ReflectiveCallInstance<T> instantiateWith(Object... params)
		{
			context.constructorParamValues = params;
			return this;
		}
		
		public T newDefaultInstance() throws ReflectiveOperationException
		{
			context.validateContext(true);
			context.instance = context.clazz.getConstructor().newInstance();
			return context.instance;
		}
		
		public T newInstance() throws ReflectiveOperationException
		{
			if(context.instantiateDefault) {
				return newDefaultInstance();
			}
			return newInstance(context.constructorParamValues);
		}
		
		public T newInstance(Object... params) throws ReflectiveOperationException
		{
			context.validateContext(false);

			if(this.context.cachedConstructor==null) {
				Constructor<T> constructor = context.clazz.getConstructor(context.constructorParamTypes);
				this.context.cachedConstructor = constructor;
			}
			
			this.context.instance = this.context.cachedConstructor.newInstance(params);
			return this.context.instance;
		}
		
		public T getCurrentInstance()
		{
			return context.instance;
		}
		
		public <R> R call(String methodName) throws ReflectiveOperationException
		{
			return call(methodName, new Class<?>[] {}, new Object[] {});
		}
		
		@SuppressWarnings("unchecked")
		public <R> R call(String methodName, Class<?>[] paramTypes, Object... params) throws ReflectiveOperationException
		{
			Objects.requireNonNull(methodName);
			CallContext.validateParameters(paramTypes, params);
			
			T instance = context.instance;
			if(instance==null) {
				instance = newInstance();
			}
			Method method = context.clazz.getMethod(methodName, paramTypes);
	        Object result = method.invoke(instance, params);
			
			return (R)result;
		}
	}
	
	private static class CallContext<T>
	{
		private Class<T> clazz;
		private boolean instantiateDefault;
		private Class<?>[] constructorParamTypes;
		private Object[] constructorParamValues;
		private Constructor<T> cachedConstructor;
		private T instance;
		
		void validateContext(boolean checkAsDefault)
		{
			if(checkAsDefault && !instantiateDefault) {
				throw new IllegalStateException("Helper has been configured with non default constructor");
			}
			else if(!checkAsDefault && instantiateDefault) {
				throw new IllegalStateException("Helper has been configured with default constructor");
			}
			
			if(!instantiateDefault) {
				Objects.requireNonNull(constructorParamTypes);
				Objects.requireNonNull(constructorParamValues);
				
				if(this.constructorParamTypes.length!=constructorParamValues.length) {
					throw new IllegalStateException("Incorrect constructor parameter number");
				}
				validateParameters(constructorParamTypes, constructorParamValues);
			}
			else if(constructorParamValues!=null && constructorParamValues.length>0) {
				throw new IllegalStateException("Default constructor doesn't admit parameters");
			}
		}
		
		static void validateParameters(Class<?>[] types, Object[] values)
		{
			for(int i=0; i<types.length; i++) {
				Class<?> type = types[i];
				Object value = values[i];
				if(value!=null && !type.isAssignableFrom(value.getClass())) {
					throw new IllegalStateException("Incorrect parameter specification [" + i + "]");
				}
			}
		}
	}
}
