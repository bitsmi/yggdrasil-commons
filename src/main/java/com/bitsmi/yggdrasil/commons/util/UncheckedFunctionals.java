package com.bitsmi.yggdrasil.commons.util;

import java.util.function.BiFunction;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Supplier;

/**
 * @since 1.1.0
 */
public class UncheckedFunctionals 
{
	public static <T, E extends Exception> Consumer<T> uncheckedConsumer(ThrowingConsumer<T, E> consumer)
	{
		return i -> {
			try {
				consumer.accept(i);
			}
			catch(Exception e) {
				UncheckedFunctionals.<RuntimeException>rethrow(e);
			}
		};
	}
	
	public static <T, E extends Exception> Supplier<T> uncheckedSupplier(ThrowingSupplier<T, E> supplier)
	{
		return () -> {
			try {
				return supplier.get();
			}
			catch(Exception e) {
				UncheckedFunctionals.<RuntimeException>rethrow(e); 
			}
			
			// This will never happen
            return null;
		};
	}
	
	public static <T, R, E extends Exception> Function<T, R> uncheckedFunction(ThrowingFunction<T, R, E> function)
	{
		return (i) -> {
			try {
				return function.apply(i);
			}
			catch(Exception e) {
				UncheckedFunctionals.<RuntimeException>rethrow(e); 
			}
			
			// This will never happen
			return null;
		};
	}
	
	/**
	 * @since 1.5.0
	 */
	public static <T, U, R, E extends Exception> BiFunction<T, U, R> uncheckedBiFunction(ThrowingBiFunction<T, U, R, E> function)
	{
		return (p1, p2) -> {
			try {
				return function.apply(p1, p2);
			}
			catch(Exception e) {
				UncheckedFunctionals.<RuntimeException>rethrow(e); 
			}
			
			// This will never happen
			return null;
		};
	}

	@FunctionalInterface
	public static interface ThrowingCallable<E extends Exception>
	{
		public void call() throws E;
	}
	
	@FunctionalInterface
	public static interface ThrowingConsumer<T, E extends Exception>
	{
		public void accept(T i) throws E;
	}
	
	@FunctionalInterface
	public static interface ThrowingSupplier<T, E extends Exception>
	{
		public T get() throws E;
	}
	
	@FunctionalInterface
	public static interface ThrowingFunction<T, R, E extends Exception>
	{
		public R apply(T i) throws E;
	}
	
	/**
	 * @since 1.5.0
	 */
	@FunctionalInterface
	public static interface ThrowingBiFunction<T, U, R, E extends Exception>
	{
		public R apply(T p1, U p2) throws E;
	}
	
	/**
	 * Util method that allows converting a checked exception into unchecked. 
	 * It relies on java's generics type erasure in order to achieve that.
	 * @param throwable
	 * @throws E
	 */
	@SuppressWarnings("unchecked")
    static <E extends Throwable> void rethrow(Throwable throwable) throws E
    {
        throw (E)throwable;
    }
}
