package com.bitsmi.yggdrasil.commons.util;

import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target({ElementType.FIELD, ElementType.METHOD, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Inherited
public @interface Value 
{
	public static final String EXPRESSION_DEFAULT_VALUE = "[unassigned]";
	
	String value() default "";
	String property() default "";
	String defaultValue() default EXPRESSION_DEFAULT_VALUE;	
}