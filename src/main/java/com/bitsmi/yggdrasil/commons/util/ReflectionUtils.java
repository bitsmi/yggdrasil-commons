package com.bitsmi.yggdrasil.commons.util;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Parameter;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.lang.reflect.TypeVariable;
import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * @since 1.6.0
 */
public class ReflectionUtils 
{
	/*-------------------------------*
	 * GENERICS INTROSPECTION
	 *-------------------------------*/
	/**
	 * Determine if a method parameter is parameterized with a generic type or a concrete class
	 * @param parameter {@link Parameter} - Method parameter
	 * @return boolean - True if it's parameterized with a generic type, false otherwise
	 */
	public static boolean isParameterizedMethodParameter(Parameter parameter)
	{
		Type type = parameter.getParameterizedType();
        if(type instanceof ParameterizedType) {
        	ParameterizedType parameterizedType = (ParameterizedType)type;
        	Type[] generics = parameterizedType.getActualTypeArguments();
        	return generics.length>0;
        }
        
        return false;
	}
	
	/**
	 * Determine if a method parameter is parameterized with non concrete generic type
	 * @param parameter {@link Parameter} - Method parameter
	 * @return boolean - True if the parameter is a non concrete generic type, false otherwise
	 */
	public static boolean isGenericMethodParameter(Parameter parameter)
	{
		Type type = parameter.getParameterizedType();
        return type instanceof TypeVariable;
	}
	
	/**
	 * Extract method parameter's concrete type arguments, if any. 
	 * If parameter doesn't have parameterized types or they are generic types (Type variables), the method will return an empty list. Only those that are
	 * associated with a concrete class will be returned.
	 * @param parameter {@link Parameter} - Method parameter
	 * @return {@link List}&lt;{@link Class}&lt;?&gt;&gt; - List of type argument classes associated with the parameter.  
	 */
	public static List<Class<?>> extractMethodParameterTypeArguments(Parameter parameter)
	{
		Type type = parameter.getParameterizedType();
        if(type instanceof ParameterizedType) {
        	ParameterizedType parameterizedType = (ParameterizedType)type;
        	Type[] generics = parameterizedType.getActualTypeArguments();
        	return Stream.of(generics)
        			.filter(generic -> generic instanceof Class)
        			.map(generic -> (Class<?>)generic)
        			.collect(Collectors.toList());
        }
        
        return Collections.emptyList();
	}
	
	/**
	 * Determine if a class is parameterized. The parameterized type may be defined by a concrete class: E.G.<br/> 
	 * <code>class Sample extends GenericSample&lt;String&gt; {<br/> 
	 * ...<br/>
	 * }</code><br/>
	 * or by an generic type: E.G.<br/>
	 * <code>public class Sample &lt;T&gt; {<br/> 
	 * ...<br/>
	 * }</code>
	 * @param classDef {@link Class}&lt;?&gt; - Class definition
	 * @return boolean - True if it's parameterized, false otherwise
	 */
	public static boolean isParameterizedClass(Class<?> classDef)
	{
		// Non defined generic types
		Type[] generics = classDef.getTypeParameters();
		if(generics.length>0) {
			return true;
		}
		
		// Defined generic types. Look for generics at super class definition  
		Type type = classDef.getGenericSuperclass();
		if(type instanceof ParameterizedType) {
			return true;
		}
		
		return false;
	}
	
	/**
	 * Extract classe's concrete type arguments, if any. 
	 * If class definition doesn't have parameterized types or they are generic types (Type variables), the method will return an empty list. Only those that are
	 * associated with a concrete class will be returned.
	 * @param classDef {@link Class} - Class definition
	 * @return {@link List}&lt;{@link Class}&lt;?&gt;&gt; - List of type argument classes associated with the class definition.  
	 */
	public static List<Class<?>> extractClassTypeArguments(Class<?> classDef)
	{
		// Defined generic types. Look for generics at super class definition  
		Type type = classDef.getGenericSuperclass();
		if(type instanceof ParameterizedType) {
			ParameterizedType parameterizedType = (ParameterizedType)type;
        	Type[] generics = parameterizedType.getActualTypeArguments();
        	return Stream.of(generics)
        			.filter(generic -> generic instanceof Class)
        			.map(generic -> (Class<?>)generic)
        			.collect(Collectors.toList());
		}
        
        return Collections.emptyList();
	}
	
	/*-------------------------------*
	 * ANNOTATION UTILS
	 *-------------------------------*/
	public static List<Field> scanAnnotatedFields(Class<?> objectClass, Class<? extends Annotation> annotationClass)
	{
		Objects.requireNonNull(objectClass);
		Objects.requireNonNull(annotationClass);
		
		List<Field> annotatedFields = new LinkedList<>();
		
        while(objectClass!=Object.class){
        	List<Field> fields = Arrays.asList(objectClass.getDeclaredFields());
        	for(Field field:fields) {
        		if(field.isAnnotationPresent(annotationClass)) {
        			annotatedFields.add(field);
        		}
        	}
            
        	objectClass = objectClass.getSuperclass();
        }
        
        return annotatedFields;
	}
	
	public static List<Method> scanAnnotatedMethods(Class<?> objectClass, Class<? extends Annotation> annotationClass)
	{
		Objects.requireNonNull(objectClass);
		Objects.requireNonNull(annotationClass);
		
		List<Method> annotatedMethods = new LinkedList<>();
		
        while(objectClass!=Object.class){
        	List<Method> methods = Arrays.asList(objectClass.getDeclaredMethods());
        	for(Method method:methods) {
        		if(method.isAnnotationPresent(annotationClass)) {
        			annotatedMethods.add(method);
        		}
        	}
            
        	objectClass = objectClass.getSuperclass();
        }
		
		return annotatedMethods;
	}
	
	public static List<Method> scanMethodsWithAnnotatedParameters(Class<?> objectClass, Class<? extends Annotation> annotationClass)
	{
		Objects.requireNonNull(objectClass);
		Objects.requireNonNull(annotationClass);
		
		List<Method> annotatedMethods = new LinkedList<>();
				
        while(objectClass!=Object.class){
        	List<Method> methods = Arrays.asList(objectClass.getDeclaredMethods());
        	for(Method method:methods) {
        		Annotation[][] paramAnnotations = method.getParameterAnnotations();
        		boolean includeMethod = false;
        		for(int i=0; i<paramAnnotations.length && !includeMethod; i++) {
        			boolean containsAnnotation = false;
        			for(int j=0; j<paramAnnotations[i].length && !containsAnnotation; j++) {
        				containsAnnotation = annotationClass.equals(paramAnnotations[i][j].annotationType());
        			}
        			includeMethod = containsAnnotation;
        		}
        		
        		if(includeMethod) {
        			annotatedMethods.add(method);
        		}
        	}
            
        	objectClass = objectClass.getSuperclass();
        }
		
		return annotatedMethods;
	}	
	
    public static List<Parameter> scanAnnotatedParameters(Method method, Class<? extends Annotation> annotationClass)
    {
        Objects.requireNonNull(method);
        Objects.requireNonNull(annotationClass);
        
        List<Parameter> annotatedParameters = Stream.of(method.getParameters())
                .filter(param -> param.isAnnotationPresent(annotationClass))
                .collect(Collectors.toList());

        return annotatedParameters;
    }
	
	public static boolean assertAllParametersAnnotated(Method method, Class<?> annotationClass)
	{
		Objects.requireNonNull(method);
		Objects.requireNonNull(annotationClass);
		
		boolean result = true;
		Annotation[][] paramAnnotations = method.getParameterAnnotations();
		for(int i=0; i<paramAnnotations.length && result; i++) {
			boolean containsAnnotation = false;
			for(int j=0; j<paramAnnotations[i].length && !containsAnnotation; j++) {
				containsAnnotation = annotationClass.equals(paramAnnotations[i][j].annotationType());
			}
			result = result && containsAnnotation;
		}
		
		return result;
	}
	
	public static boolean assertParameterAnnotated(Method method, int index, Class<?> annotationClass)
	{
		Objects.requireNonNull(method);
		Objects.requireNonNull(annotationClass);
		if(index<0 || index>=method.getParameterCount()) {
			throw new IllegalArgumentException(String.format("Invalid parameter index for method (%s) [%s]", method.getName(), index));
		}
		
		boolean containsAnnotation = false;
		Annotation[][] paramAnnotations = method.getParameterAnnotations();
		for(int j=0; j<paramAnnotations[index].length && !containsAnnotation; j++) {
			containsAnnotation = annotationClass.equals(paramAnnotations[index][j].annotationType());
		}
		
		return containsAnnotation;
	}
}
