package com.bitsmi.yggdrasil.commons.util;

import java.util.LinkedList;
import java.util.List;
import java.util.stream.IntStream;

/**
 *
 * @author Antonio Archilla
 * @since 1.6.0
 */
public class ProgressTracker implements IProgressTrackerTask 
{
	private NestableTrackerTask mainTask;
	
	private ProgressTracker()
	{
		
	}
	
	public static final ProgressTracker newTracker()
	{
		return new ProgressTracker()
				.initialize();
	}
	
	private ProgressTracker initialize()
	{
		this.mainTask = new NestableTrackerTask(TASK_MIN_VALUE, TASK_MAX_VALUE);
		return this;
	}
	
	@Override
	public ProgressTracker addProgressListener(IProgressTrackerListener listener)
	{
		mainTask.addProgressListener(listener);
		return this;
	}
	
	@Override
	public List<? extends IProgressTrackerTask> split(int... subtaskAmounts)
	{
		return mainTask.split(subtaskAmounts);
	}
	
	@Override
	public boolean hasSubtasks()
	{
		return mainTask.hasSubtasks();
	}
	
	@Override
	public List<? extends IProgressTrackerTask> getSubtasks()
	{
		return mainTask.getSubtasks();
	}
	
	@Override
	public int getSubtaskCount()
	{
		return mainTask.getSubtaskCount();
	}
	
	@Override
	public IProgressTrackerTask getCurrentSubtask() 
	{
		return mainTask.getCurrentSubtask();
	}
	
	@Override
	public int getCurrentSubtaskIndex()
	{
		return mainTask.getCurrentSubtaskIndex();
	}
	
	@Override
	public IProgressTrackerTask getSubtask(int index)
	{
		return mainTask.getSubtask(index);
	}
	
	@Override
	public int computeProgress()
	{
		return mainTask.computeProgress();
	}
	
	@Override
	public int advance(int amount)
	{
		return mainTask.advance(amount);
	}
	
	@Override
	public int advance(int amount, IProgressTrackerListener.ProgressInfo info)
	{
		return mainTask.advance(amount, info);
	}
	
	@Override
	public boolean isComplete()
	{
		return mainTask.isComplete();
	}
	
	@Override
	public void complete()
	{
		mainTask.complete();
	}
	
	@Override
	public void complete(IProgressTrackerListener.ProgressInfo info)
	{
		mainTask.complete(info);
	}
	
	private static class NestableTrackerTask implements IProgressTrackerTask
	{
		private NestableTrackerTask parentTask;
		private int startValue;
		private int endValue;
		private int relativeValue;
		private List<NestableTrackerTask> subtasks;
		private NestableTrackerTask currentSubtask;
		private int currentSubtaskIndex;
		
		private IProgressTrackerListener listener;
		
		NestableTrackerTask(int startValue, int endValue)
		{
			this.startValue = startValue;
			this.endValue = endValue;
			this.subtasks = new LinkedList<>();
		}
		
		NestableTrackerTask(NestableTrackerTask parentTask, int startValue, int endValue)
		{
			this(startValue, endValue);
			this.parentTask = parentTask;
		}
		
		@Override
		public IProgressTrackerTask addProgressListener(IProgressTrackerListener listener)
		{
			if(this.listener!=null) {
				this.listener = this.listener.andThen(listener);
			}
			else {
				this.listener = listener;
			}
		
			return this;
		}
		
		@Override
		public List<? extends IProgressTrackerTask> split(int... subtaskAmounts)
		{
			if(!subtasks.isEmpty()) {
				throw new IllegalStateException("Task already split");
			}
			
			int sum = IntStream.of(subtaskAmounts).sum();
			if(sum>TASK_MAX_VALUE) {
				String errMsg = String.format("La suma dels valors per les subtasques es superior al màxim permès (%s)", TASK_MAX_VALUE);
				throw new IllegalArgumentException(errMsg);
			}
			
			int index = TASK_MIN_VALUE;
			for(int amount:subtaskAmounts) {
				subtasks.add(new NestableTrackerTask(this, index, index += amount));
			}
			
			boolean addRemaining = sum<TASK_MAX_VALUE && index<TASK_MAX_VALUE;
			if(addRemaining) {
				subtasks.add(new NestableTrackerTask(this, index, TASK_MAX_VALUE));
			}
			
			currentSubtaskIndex = 0;
			currentSubtask = subtasks.get(currentSubtaskIndex);
			return subtasks;
		}
		
		@Override
		public boolean hasSubtasks()
		{
			return !subtasks.isEmpty();
		}
		
		@Override
		public List<? extends IProgressTrackerTask> getSubtasks()
		{
			return subtasks;
		}
		
		@Override
		public int getSubtaskCount()
		{
			return subtasks.size();
		}
		
		@Override
		public IProgressTrackerTask getCurrentSubtask()
		{
			tryNextSubtask();
			return currentSubtask;
		}
		
		@Override
		public int getCurrentSubtaskIndex()
		{
			tryNextSubtask();
			return currentSubtaskIndex;
		}
		
		private void tryNextSubtask()
		{
			assertCurrentSubtasks();
			
			if(currentSubtask.isComplete() && currentSubtaskIndex<subtasks.size()) {
				currentSubtaskIndex++;
				currentSubtask = subtasks.get(currentSubtaskIndex);
			}
		}
		
		@Override
		public IProgressTrackerTask getSubtask(int index)
		{
			return subtasks.get(index);
		}
		
		public int getStartValue()
		{
			return startValue;
		}
		
		public int getEndValue() 
		{
			return endValue;
		}
		
		@Override
		public int computeProgress()
		{
			if(currentSubtask==null){
				return relativeValue;
			}
			
			int total = 0;
			for(NestableTrackerTask task:subtasks) {
				int progress = task.computeProgress();
				progress *= (Float.valueOf(task.getEndValue()-task.getStartValue())) / (TASK_MAX_VALUE-TASK_MIN_VALUE);
				total += progress;
			}
			
			return total;
		}
		
		@Override
		public int advance(int amount)
		{
			return advance(amount, null);
		}
		
		@Override
		public int advance(int amount, IProgressTrackerListener.ProgressInfo info)
		{
			return advanceInternal(amount, info);
		}
		
		private int advanceInternal(int amount, IProgressTrackerListener.ProgressInfo info)
		{
			if(currentSubtask==null){
				int absoluteValue = relativeValue + amount;
				relativeValue = Math.min(relativeValue + amount, TASK_MAX_VALUE);
				
				notifyProgression(info);
				
				return absoluteValue - TASK_MAX_VALUE;
			}

			// Transformar a valor relatiu a la subtask
			amount *= Float.valueOf(TASK_MAX_VALUE-TASK_MIN_VALUE) / (currentSubtask.getEndValue()-currentSubtask.getStartValue());
			int remaining = currentSubtask.advanceInternal(amount, info);
			// Propagar a següent subtasca
			while(remaining>0 && currentSubtaskIndex<subtasks.size()-1) {
				// Transformar a valor relatiu a la task actual
				remaining *= (Float.valueOf(currentSubtask.getEndValue()-currentSubtask.getStartValue())) / (TASK_MAX_VALUE-TASK_MIN_VALUE);
				
				currentSubtaskIndex++;
				currentSubtask = subtasks.get(currentSubtaskIndex);
				
				// If subtask is already used cannot store overflow
				if(currentSubtask.computeProgress()>0) {
					throw new IllegalStateException("Task progress overflow. Next subtask is already used");
				}
				
				// Transformar a valor relatiu a la subtask següent
				remaining *= Float.valueOf(TASK_MAX_VALUE-TASK_MIN_VALUE) / (currentSubtask.getEndValue()-currentSubtask.getStartValue());
				remaining = currentSubtask.advance(remaining);
			}
			
			int total = computeProgress();
			
			return total - TASK_MAX_VALUE;
		}
		
		public boolean isComplete()
		{
			if(currentSubtask==null) {
				return relativeValue==TASK_MAX_VALUE;
			}
			
			return currentSubtask.isComplete() && currentSubtaskIndex==subtasks.size()-1;
		}
		
		@Override
		public void complete()
		{
			completeInternal(null);
		}
		
		@Override
		public void complete(IProgressTrackerListener.ProgressInfo info)
		{
			completeInternal(info);
		}
		
		private void completeInternal(IProgressTrackerListener.ProgressInfo info)
		{
			if(currentSubtask==null) {
				relativeValue = TASK_MAX_VALUE;
				notifyProgression(info);
				return;
			}

			for(NestableTrackerTask subtask:subtasks) {
				subtask.completeInternal(info);
			}
			
			currentSubtaskIndex = subtasks.size()-1;
			currentSubtask = subtasks.get(currentSubtaskIndex);
			
			notifyProgression(info);
		}
		
		private void notifyProgression(IProgressTrackerListener.ProgressInfo info)
		{
			if(listener!=null) {
				listener.onTaskProgression(this, info);
			}
			
			if(parentTask!=null) {
				parentTask.notifyProgression(info);
			}			
		}
		
		private void assertCurrentSubtasks()
		{
			if(currentSubtask==null) {
				throw new IllegalStateException("This task has no subtasks");
			}
		}
	}
}
