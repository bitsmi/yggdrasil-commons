package com.bitsmi.yggdrasil.commons.util;

/**
 * 
 * @since 1.8.0, 2.1.0
 */
public class StringUtils 
{
	// From org.apache.commons.lang3.StringUtils
    public static boolean containsWhitespace(final CharSequence seq) 
    {
        if (isEmpty(seq)) {
            return false;
        }
        final int strLen = seq.length();
        for (int i = 0; i < strLen; i++) {
            if (Character.isWhitespace(seq.charAt(i))) {
                return true;
            }
        }
        return false;
    }
    
    // From org.apache.commons.lang3.StringUtils
    public static boolean isEmpty(final CharSequence cs) 
    {
        return cs == null || cs.length() == 0;
    }
    
    // From org.apache.commons.lang3.StringUtils
    public static boolean isBlank(final CharSequence cs) 
    {
        final int strLen = length(cs);
        if (strLen == 0) {
            return true;
        }
        for (int i = 0; i < strLen; i++) {
            if (!Character.isWhitespace(cs.charAt(i))) {
                return false;
            }
        }
        return true;
    }
    
    // From org.apache.commons.lang3.StringUtils
    public static int length(final CharSequence cs) 
    {
        return cs == null ? 0 : cs.length();
    }
}
