package com.bitsmi.yggdrasil.commons.util;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.function.BiConsumer;
import java.util.function.Function;
import java.util.function.Supplier;

public class TransformerMapping <S, T> 
{
    private Supplier<T> targetSupplier; 
    private List<TransformerMappingOperation<S, T, ?>> operations = new ArrayList<>();
    
    public TransformerMapping <S, T> withTarget(T target)
    {
        Objects.requireNonNull(target);
        
        return withTarget(() -> target);
    }
    
    public TransformerMapping <S, T> withTarget(Supplier<T> targetSupplier)
    {
        Objects.requireNonNull(targetSupplier);
        
        this.targetSupplier = targetSupplier;
        
        return this;
    }
    
    public <R> TransformerMappingOperation<S, T, R> from(Function<S, R> extractor)
    {
        Objects.requireNonNull(extractor);
        
        TransformerMappingOperation<S, T, R> op = new TransformerMappingOperation<>(this, extractor);
        operations.add(op);
        
        return op;
    }
    
    public <U, R> TransformerMappingOperation<S, T, R> from(Function<S, U> extractor, TransformerMapping<U, R> downstream)
    {
        Objects.requireNonNull(extractor);
        Objects.requireNonNull(downstream);
        
        TransformerMappingOperation<S, T, R> op = new TransformerMappingOperation<>(this, extractor, downstream);
        operations.add(op);
        
        return op;
    }
    
    public T apply(S source)
    {
        if(source==null) {
            return null;
        }
        
        T target = targetSupplier.get();
        if(target==null) {
            throw new IllegalStateException("Mapping target cannot be null");
        }
        
        for(TransformerMappingOperation<S, T, ?> operation:operations) {
            operation.apply(source, target);
        }
        
        return target;
    }
    
    public static class TransformerMappingOperation<S, T, R>
    {
        private TransformerMapping<S, T> parent;
        private Function<S, R> mapper;
        private BiConsumer<T, R> targetFiller;
        
        TransformerMappingOperation(TransformerMapping<S, T> parent, Function<S, R> mapper)
        {
            this.parent = parent;
            this.mapper = mapper;
        }
        
        <U>TransformerMappingOperation(TransformerMapping<S, T> parent, Function<S, U> mapper, TransformerMapping<U, R> downstream)
        {
            this.parent = parent;
            this.mapper = src -> {
                U value = mapper.apply(src);
                return downstream.apply(value);
            };
        }
        
        public TransformerMapping<S, T> to(BiConsumer<T, R> targetFiller)
        {
            this.targetFiller = targetFiller;
            return parent;
        }
        
        void apply(S source, T target)
        {
            R value = mapper.apply(source);
            targetFiller.accept(target, value);
        }
    }
}
