package com.bitsmi.yggdrasil.commons.collection;

import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

public class TableViewRecord <T extends IQueryableData> extends ViewRecord<T>
{
    private T sourceData;
    private Set<String> filteredColumns;
    
    TableViewRecord(T sourceData, Set<String> filteredColumns)
    {
        this.sourceData = sourceData;
        this.filteredColumns = filteredColumns;
    }
    
    public Map<String, Object> getValues()
    {
        return sourceData.getValues().keySet()
                .stream()
                .filter(filteredColumns::contains)
                .collect(Collectors.toMap(Function.identity(), sourceData::getValue));
    }
    
    public Object getValue(String columnName)
    {
        if(!filteredColumns.contains(columnName)) {
            throw new IllegalArgumentException("Missing column " + columnName);
        }
        
        return sourceData.getValue(columnName);
    }
}
