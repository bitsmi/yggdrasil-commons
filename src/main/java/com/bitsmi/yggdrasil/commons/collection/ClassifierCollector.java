package com.bitsmi.yggdrasil.commons.collection;

import java.util.HashSet;
import java.util.Set;
import java.util.function.BiConsumer;
import java.util.function.BinaryOperator;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Collector;

public class ClassifierCollector<T> implements Collector<T, Classification<T>, Classification<T>> 
{
	private ClassifierMatcher<T> classifier;
	
	public ClassifierCollector(ClassifierMatcher<T> classifier) 
	{
		this.classifier = classifier;
	}
	
	@Override
	public Set<Characteristics> characteristics() 
	{
		Set<Characteristics> characteristics = new HashSet<>();
		characteristics.add(Characteristics.UNORDERED);
		
		return characteristics;
	}
	
	@Override
	public Supplier<Classification<T>> supplier() 
	{
		return () -> classifier.build();
	}
	
	@Override
	public BiConsumer<Classification<T>, T> accumulator() 
	{
		return (c, e) -> c.classify(e);
	}

	@Override
	public BinaryOperator<Classification<T>> combiner() 
	{
		return (c1, c2) -> {
			c1.combine(c2);
			return c1;
		};
	}

	@Override
	public Function<Classification<T>, Classification<T>> finisher() 
	{
		return Function.identity(); 
	}
}
