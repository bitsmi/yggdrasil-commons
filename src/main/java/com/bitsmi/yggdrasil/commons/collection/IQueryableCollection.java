package com.bitsmi.yggdrasil.commons.collection;

import java.util.Collection;
import java.util.stream.Stream;

public interface IQueryableCollection<T extends IQueryableData> 
{
	public IQueryableCollection<T> add(T record);
	public IQueryableCollection<T> addAll(Collection<T> data);
	public boolean remove(T record);
	public void clear();
	public Stream<T> stream();
	public long size();
}
