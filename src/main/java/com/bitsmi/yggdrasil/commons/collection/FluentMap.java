package com.bitsmi.yggdrasil.commons.collection;

import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.TreeMap;
import java.util.function.BiFunction;
import java.util.function.Function;

/**
 *
 * @param <K> 
 * @param <V>
 * 
 * @author Antonio Archilla
 * @version 1.5.0
 */
public class FluentMap <T extends Map<K, V>, K, V>
{
    private T mapInstance;
    
    private FluentMap() { }
    
    public static <T extends Map<K, V>, K, V> FluentMap<T, K, V> of(T map)
    {
        FluentMap<T, K, V> FluentMap = new FluentMap<>();
        FluentMap.mapInstance = map;
        
        return FluentMap;
    }
    
    public static <K, V> FluentMap<HashMap<K, V>, K, V> newHashMap()
    {
        return of(new HashMap<K, V>());
    }
    
    public static <K, V> FluentMap<LinkedHashMap<K, V>, K, V> newLinkedHashMap()
    {
        return of(new LinkedHashMap<K, V>());
    }
    
    public static <K, V> FluentMap<TreeMap<K, V>, K, V> newTreeMap()
    {
        return of(new TreeMap<K, V>());
    }
    
    public static <K, V> FluentMap<TreeMap<K, V>, K, V> newTreeMap(Comparator<K> comparator)
    {
        return of(new TreeMap<K, V>(comparator));
    }
    
    public FluentMap<T, K, V> put(K key, V value)
    {
        mapInstance.put(key, value);
        return this;
    }
    
    public FluentMap<T, K, V> putAll(Map<K, V> map)
    {
        mapInstance.putAll(map);
        return this;
    }
    
    public FluentMap<T, K, V> putIfAbsent(K key, V value)
    {
        mapInstance.putIfAbsent(key, value);
        return this;
    }
    
    public FluentMap<T, K, V> compute(K key, BiFunction<? super K, ? super V, ? extends V> remappingFunction)
    {
        mapInstance.compute(key, remappingFunction);
        return this;
    }
    
    public FluentMap<T, K, V> computeIfAbsent(K key, Function<? super K, ? extends V> mappingFunction)
    {
        mapInstance.computeIfAbsent(key, mappingFunction);
        return this;
    }
    
    public FluentMap<T, K, V> computeIfPresent(K key, BiFunction<? super K,? super V,? extends V> remappingFunction)
    {
        mapInstance.computeIfPresent(key, remappingFunction);
        return this;
    }
    
    public FluentMap<T, K, V> merge(K key, V value, BiFunction<? super V,? super V,? extends V> remappingFunction)
    {
        mapInstance.merge(key, value, remappingFunction);
        return this;
    }
    
    public FluentMap<T, K, V> clear()
    {
        mapInstance.clear();
        return this;
    }
    
    public FluentMap<T, K, V> replace(K key, V value)
    {
        mapInstance.replace(key, value);
        return this;
    }
    
    public FluentMap<T, K, V> replace(K key, V oldValue, V newValue)
    {
        mapInstance.replace(key, oldValue, newValue);
        return this;
    }
    
    public FluentMap<T, K, V> replaceAll(BiFunction<? super K, ? super V, ? extends V> function)
    {
        mapInstance.replaceAll(function);
        return this;
    }
    
    public T get()
    {
        return mapInstance;
    }
}
