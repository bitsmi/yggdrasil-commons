package com.bitsmi.yggdrasil.commons.collection;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class OnMemoryTableBuilder 
{
	private List<String> columnNames;
	
	public OnMemoryTableBuilder withColumns(String... columnNames)
	{
		this.columnNames = Arrays.asList(columnNames);
		return this;
	}
	
	public OnMemoryTable build()
	{
		/* Validations */
		Set<String> diffColumnNames = new HashSet<>();
		diffColumnNames.addAll(columnNames);
		if(diffColumnNames.size()!=columnNames.size()) {
			throw new IllegalStateException("The table contains repeated column names");
		}
		
		OnMemoryTable table = new OnMemoryTable()
				.setColumnNames(columnNames);
		
		return table;
	}
}
