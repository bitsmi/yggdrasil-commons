package com.bitsmi.yggdrasil.commons.collection;

public class QueryableCollectionFactory 
{
	/*----------------------------------*
	 * COLLECTIONS
	 *----------------------------------*/
	public static final OnMemoryTableBuilder newOnMemoryTable()
	{
		return new OnMemoryTableBuilder();
	}
	
	/*----------------------------------*
	 * VIEWS
	 *----------------------------------*/
	public static final <T extends ITable<R>, R extends IQueryableData> SingleTableViewBuilder<T, R> newSingleTableView(Class<T> tableDef)
	{
		return new SingleTableViewBuilder<T, R>();
	}
}
