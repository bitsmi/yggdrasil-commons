package com.bitsmi.yggdrasil.commons.collection;

import java.util.Collection;
import java.util.Iterator;
import java.util.stream.Stream;

/**
 *
 * @param <T> 
 * @param <E>
 * 
 * @author Antonio Archilla
 * @version 1.5.0
 */
public class FluentCollection <T extends Collection<E>, E>
{
    protected T collectionInstance; 
    
    public static <T extends Collection<E>, E> FluentCollection<T, E> of(T collection)
    {
        FluentCollection<T, E> fluentCollection = new FluentCollection<>(collection);
        return fluentCollection;
    }
    
    protected FluentCollection(T collectionInstance)
    {
        this.collectionInstance = collectionInstance;
    }
    
    public FluentCollection<T, E> add(E elem)
    {
        collectionInstance.add(elem);
        return this;
    }
    
    public FluentCollection<T, E> addAll(Collection<E> collection)
    {
        collectionInstance.addAll(collection);
        return this;
    }
    
    public FluentCollection<T, E> clear()
    {
        collectionInstance.clear();
        return this;
    }
    
    public FluentCollection<T, E> retainAll(Collection<E> collection)
    {
        collectionInstance.retainAll(collection);
        return this;
    }
    
    public T get()
    {
        return collectionInstance;
    }
    
    public Iterator<E> toIterator()
    {
        return collectionInstance.iterator();
    }
    
    public Stream<E> toStream()
    {
        return collectionInstance.stream();
    }
}
