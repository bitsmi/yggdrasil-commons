package com.bitsmi.yggdrasil.commons.collection;

import java.util.List;
import java.util.Map;

public interface ITable <T extends IQueryableData> extends IQueryableCollection<T> 
{
	public List<String> getColumnNames();
	
	public ITable<T> addRecord(Object... recordData);
	public ITable<T> addRecord(List<Object> recordData);
	public ITable<T> addRecord(Map<String, Object> recordData);
	
	/**
	 * Delete records whose columns values match the specified parameters 
	 * @param column {@link String} - Column name
	 * @param value {@link Object} - Column value
	 * @return long - Number of deleted records
	 */
	public long removeRecords(String column, Object value);
	
	public T get(int index);
	public Object getValue(int index, String columnName);
}
