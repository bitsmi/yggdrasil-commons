package com.bitsmi.yggdrasil.commons.collection;

import java.util.Map;

public interface IQueryableData 
{
    public Map<String, Object> getValues();
    public Object getValue(String columnName);
}
