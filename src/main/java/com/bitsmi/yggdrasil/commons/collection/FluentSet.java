package com.bitsmi.yggdrasil.commons.collection;

import java.util.Collection;
import java.util.Comparator;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Set;
import java.util.TreeSet;

/**
 *
 * @param <T> 
 * @param <E>
 * 
 * @author Antonio Archilla
 * @version 1.5.0
 */
public class FluentSet <T extends Set<E>, E> extends FluentCollection<T, E>
{
    private FluentSet(T setInstance) 
    { 
        super(setInstance);
    }
    
    public static <T extends Set<E>, E> FluentSet<T, E> of(T set)
    {
        FluentSet<T, E> FluentSet = new FluentSet<>(set);
        return FluentSet;
    }
    
    public static <E> FluentSet<HashSet<E>, E> newHashSet()
    {
        return of(new HashSet<E>());
    }
    
    public static <E> FluentSet<LinkedHashSet<E>, E> newLinkedHashSet()
    {
        return of(new LinkedHashSet<E>());
    }
    
    public static <E> FluentSet<TreeSet<E>, E> newTreeSet()
    {
        return of(new TreeSet<E>());
    }
    
    public static <E> FluentSet<TreeSet<E>, E> newTreeSet(Comparator<E> comparator)
    {
        return of(new TreeSet<E>(comparator));
    }
    
    @Override
    public FluentSet<T, E> add(E elem)
    {
        super.add(elem);
        return this;
    }
    
    @Override
    public FluentSet<T, E> addAll(Collection<E> collection)
    {
        super.addAll(collection);
        return this;
    }
    
    @Override
    public FluentSet<T, E> clear()
    {
        super.clear();
        return this;
    }
    
    @Override
    public FluentSet<T, E> retainAll(Collection<E> collection)
    {
        super.retainAll(collection);
        return this;
    }
}
