package com.bitsmi.yggdrasil.commons.collection;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;
import java.util.function.UnaryOperator;

/**
 *
 * @param <T> 
 * @param <E>
 * 
 * @author Antonio Archilla
 * @version 1.5.0
 */
public class FluentList <T extends List<E>, E> extends FluentCollection<T, E>
{
    private FluentList(T listInstance) 
    { 
        super(listInstance);
    }
    
    public static <T extends List<E>, E> FluentList<T, E> of(T list)
    {
        FluentList<T, E> fluentList = new FluentList<>(list);
        return fluentList;
    }
    
    public static <E> FluentList<ArrayList<E>, E> newArrayList()
    {
        return of(new ArrayList<E>());
    }
    
    public static <E> FluentList<LinkedList<E>, E> newLinkedList()
    {
        return of(new LinkedList<E>());
    }
    
    @Override
    public FluentList<T, E> add(E elem)
    {
        super.add(elem);
        return this;
    }
    
    public FluentList<T, E> add(int index, E elem)
    {
        get().add(index, elem);
        return this;
    }
    
    @Override
    public FluentList<T, E> addAll(Collection<E> collection)
    {
        super.addAll(collection);
        return this;
    }
    
    public FluentList<T, E> addAll(int index, Collection<E> collection)
    {
        get().addAll(index, collection);
        return this;
    }
    
    @Override
    public FluentList<T, E> clear()
    {
        get().clear();
        return this;
    }
    
    @Override
    public FluentList<T, E> retainAll(Collection<E> collection)
    {
        get().retainAll(collection);
        return this;
    }
    
    public FluentList<T, E> replaceAll(UnaryOperator<E> operator)
    {
        get().replaceAll(operator);
        return this;
    }
    
    public FluentList<T, E> sort(Comparator<E> comparator)
    {
        get().sort(comparator);
        return this;
    }
}
