package com.bitsmi.yggdrasil.commons.collection;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class SingleTableViewBuilder <T extends ITable<R>, R extends IQueryableData> 
{
	private T sourceTable;
	private Set<String> filteredColumns;
	private Criteria criteria;
	
	public SingleTableViewBuilder<T, R> selectAll()
	{
	    // Nothing to do...
		return this;
	}
	
	public SingleTableViewBuilder<T, R> select(String... columns)
    {
        Objects.requireNonNull(columns);
        
        if(columns.length>0) {
            this.filteredColumns = Stream.of(columns)
                    .collect(Collectors.toSet());
        }
        
        return this;
    }
	
	public SingleTableViewBuilder<T, R> with(T table)
	{
		this.sourceTable = table;
		return this;
	}
	
	/**
	 * Synonym of {@link #with(ITable)}
	 * @return {@link SingleTableViewBuilder} - Builder instance to chain method calls
	 */
	public SingleTableViewBuilder<T, R> from(T table)
	{
		return with(table);
	}
	
	public WhereClauseBuilder<T, R> where()
	{
		return new WhereClauseBuilder<>(this);
	}
	
	void setCriteria(Criteria criteria)
	{
		this.criteria = criteria;
	}
	
	public SingleTableView<T, R> build()
	{
	    // Check if filtered columns are present into table's
	    if(filteredColumns!=null && !sourceTable.getColumnNames().containsAll(filteredColumns)) {
	        throw new IllegalArgumentException("Some of the selected columns are not included into table columns");
	    }
	    
	    if(filteredColumns==null) {
	        filteredColumns = new HashSet<>(sourceTable.getColumnNames());
	    }
	    
		SingleTableView<T, R> view = new SingleTableView<T, R>()
				.setSourceTable(sourceTable)
				.setFilteredColumns(filteredColumns)
				.setCriteria(criteria);
		view.refresh();
		return view;
	}
	
	public static class WhereClauseBuilder<T extends ITable<S>, S extends IQueryableData> 
	{
		private SingleTableViewBuilder<T, S> viewBuilder;
		private Criteria evaluator;
		
		private WhereClauseBuilder(SingleTableViewBuilder<T, S> viewBuilder)
		{
			this.viewBuilder = viewBuilder;
			this.evaluator = new Criteria();
		}
		
		public SingleTableView<T, S> build()
		{
			viewBuilder.setCriteria(evaluator);
			return viewBuilder.build();
		}
		
		public WhereExpressionBuilder<T, S> isTrue(String expression)
	    {
			WhereExpressionBuilder<T, S> builder = new WhereExpressionBuilder<>(this, evaluator.isTrue(expression));
	        return builder;
	    }
		
		public WhereExpressionBuilder<T, S> isFalse(String expression)
	    {
			WhereExpressionBuilder<T, S> builder = new WhereExpressionBuilder<>(this, evaluator.isFalse(expression));
	        return builder;
	    }
		
		public WhereExpressionBuilder<T, S> isNull(String expression)
	    {
			WhereExpressionBuilder<T, S> builder = new WhereExpressionBuilder<>(this, evaluator.isNull(expression));
	        return builder;
	    }
		
		public WhereExpressionBuilder<T, S> isNotNull(String expression)
	    {
			WhereExpressionBuilder<T, S> builder = new WhereExpressionBuilder<>(this, evaluator.isNotNull(expression));
	        return builder;
	    }
		
		public WhereExpressionBuilder<T, S> isEqualsToValue(String expression, Object value)
        {
			WhereExpressionBuilder<T, S> builder = new WhereExpressionBuilder<>(this, evaluator.isEqualsToValue(expression, value));
	        return builder;
        }
        
        public WhereExpressionBuilder<T, S> isEqualsToReference(String expression1, String expression2)
        {
        	WhereExpressionBuilder<T, S> builder = new WhereExpressionBuilder<>(this, evaluator.isEqualsToReference(expression1, expression2));
	        return builder;
        }
        
        public WhereExpressionBuilder<T, S> isNotEqualsToValue(String expression, Object value)
        {
        	WhereExpressionBuilder<T, S> builder = new WhereExpressionBuilder<>(this, evaluator.isNotEqualsToValue(expression, value));
	        return builder;
        }
        
        public WhereExpressionBuilder<T, S> isNotEqualsToReference(String expression1, String expression2)
        {
        	WhereExpressionBuilder<T, S> builder = new WhereExpressionBuilder<>(this, evaluator.isNotEqualsToReference(expression1, expression2));
	        return builder;
        }
        
        public WhereExpressionBuilder<T, S> isGreaterThanValue(String expression, Comparable<?> value)
        {
        	WhereExpressionBuilder<T, S> builder = new WhereExpressionBuilder<>(this, evaluator.isGreaterThanValue(expression, value));
	        return builder;
        }
        
        public WhereExpressionBuilder<T, S> isGreaterThanReference(String expression1, String expression2)
        {
        	WhereExpressionBuilder<T, S> builder = new WhereExpressionBuilder<>(this, evaluator.isGreaterThanReference(expression1, expression2));
	        return builder;
        }
        
        public WhereExpressionBuilder<T, S> isGreaterOrEqualsToValue(String expression, Comparable<?> value)
        {
        	WhereExpressionBuilder<T, S> builder = new WhereExpressionBuilder<>(this, evaluator.isGreaterOrEqualsToValue(expression, value));
	        return builder;
        }
        
        public WhereExpressionBuilder<T, S> isGreaterOrEqualsToReference(String expression1, String expression2)
        {
        	WhereExpressionBuilder<T, S> builder = new WhereExpressionBuilder<>(this, evaluator.isGreaterOrEqualsToReference(expression1, expression2));
	        return builder;
        }
        
        public WhereExpressionBuilder<T, S> isLessThanValue(String expression, Comparable<?> value)
        {
        	WhereExpressionBuilder<T, S> builder = new WhereExpressionBuilder<>(this, evaluator.isLessThanValue(expression, value));
	        return builder;
        }
        
        public WhereExpressionBuilder<T, S> isLessThanReference(String expression1, String expression2)
        {
        	WhereExpressionBuilder<T, S> builder = new WhereExpressionBuilder<>(this, evaluator.isLessThanReference(expression1, expression2));
	        return builder;
        }
        
        public WhereExpressionBuilder<T, S> isLessOrEqualsToValue(String expression, Comparable<?> value)
        {
        	WhereExpressionBuilder<T, S> builder = new WhereExpressionBuilder<>(this, evaluator.isLessOrEqualsToValue(expression, value));
	        return builder;
        }
        
        public WhereExpressionBuilder<T, S> isLessOrEqualsToReference(String expression1, String expression2)
        {
        	WhereExpressionBuilder<T, S> builder = new WhereExpressionBuilder<>(this, evaluator.isLessOrEqualsToReference(expression1, expression2));
	        return builder;
        }
	}
	
	public static class WhereExpressionBuilder <T extends ITable<S>, S extends IQueryableData> 
	{
		private WhereClauseBuilder<T, S> clauseBuilder;
		private Criteria.CriteriaBuilder criteriaBuilder;
		private WhereExpressionBuilder<T, S> parentBuilder;
		
		WhereExpressionBuilder(WhereClauseBuilder<T, S> clauseBuilder, Criteria.CriteriaBuilder expressionBuilder)
        {
            this.clauseBuilder = clauseBuilder;
            this.criteriaBuilder = expressionBuilder;
        }
		
		WhereExpressionBuilder(WhereExpressionBuilder<T, S> parentBuilder, Criteria.CriteriaBuilder expressionBuilder)
		{
			this.parentBuilder = parentBuilder;
			this.criteriaBuilder = expressionBuilder;
		}
		
		public SingleTableView<T, S> build()
        {
			if(parentBuilder!=null) {
                throw new IllegalStateException("Group not closed");
            }
			
            criteriaBuilder.build();
            return clauseBuilder.build();
        }
		
		public WhereExpressionBuilder<T, S> not()
		{
			criteriaBuilder.not();
			return this;
		}
		
		public WhereExpressionBuilder<T, S> isTrue(String expression)
        {
			criteriaBuilder.isTrue(expression);
        	return this;
        }
		
		public WhereExpressionBuilder<T, S> isFalse(String expression)
        {
			criteriaBuilder.isFalse(expression);
        	return this;
        }
		
		public WhereExpressionBuilder<T, S> isNull(String expression)
        {
			criteriaBuilder.isNull(expression);
        	return this;
        }
		
		public WhereExpressionBuilder<T, S> isNotNull(String expression)
        {
			criteriaBuilder.isNotNull(expression);
        	return this;
        }
		
		public WhereExpressionBuilder<T, S> isEqualsToValue(String expression, Object value)
        {
			criteriaBuilder.isEqualsToValue(expression, value);
        	return this;
        }
        
        public WhereExpressionBuilder<T, S> isEqualsToReference(String expression1, String expression2)
        {
        	criteriaBuilder.isEqualsToReference(expression1, expression2);
        	return this;
        }
        
        public WhereExpressionBuilder<T, S> isNotEqualsToValue(String expression, Object value)
        {
        	criteriaBuilder.isNotEqualsToValue(expression, value);
        	return this;
        }
        
        public WhereExpressionBuilder<T, S> isNotEqualsToReference(String expression1, String expression2)
        {
        	criteriaBuilder.isNotEqualsToReference(expression1, expression2);
        	return this;
        }
        
        public WhereExpressionBuilder<T, S> isGreaterThanValue(String expression, Comparable<?> value)
        {
        	criteriaBuilder.isGreaterThanValue(expression, value);
        	return this;
        }
        
        public WhereExpressionBuilder<T, S> isGreaterThanReference(String expression1, String expression2)
        {
        	criteriaBuilder.isGreaterThanReference(expression1, expression2);
        	return this;
        }
        
        public WhereExpressionBuilder<T, S> isGreaterOrEqualsToValue(String expression, Comparable<?> value)
        {
        	criteriaBuilder.isGreaterOrEqualsToValue(expression, value);
        	return this;
        }
        
        public WhereExpressionBuilder<T, S> isGreaterOrEqualsToReference(String expression1, String expression2)
        {
        	criteriaBuilder.isGreaterOrEqualsToReference(expression1, expression2);
        	return this;
        }
        
        public WhereExpressionBuilder<T, S> isLessThanValue(String expression, Comparable<?> value)
        {
        	criteriaBuilder.isLessThanValue(expression, value);
        	return this;
        }
        
        public WhereExpressionBuilder<T, S> isLessThanReference(String expression1, String expression2)
        {
        	criteriaBuilder.isLessThanReference(expression1, expression2);
        	return this;
        }
        
        public WhereExpressionBuilder<T, S> isLessOrEqualsToValue(String expression, Comparable<?> value)
        {
        	criteriaBuilder.isLessOrEqualsToValue(expression, value);
        	return this;
        }
        
        public WhereExpressionBuilder<T, S> isLessOrEqualsToReference(String expression1, String expression2)
        {
        	criteriaBuilder.isLessOrEqualsToReference(expression1, expression2);
        	return this;
        }
		
		public WhereExpressionBuilder<T, S> and()
        {
            criteriaBuilder.and();
            return this;
        }
        
        public WhereExpressionBuilder<T, S> or()
        {
        	criteriaBuilder.or();
            return this;
        }
        
        public WhereExpressionBuilder<T, S> startGroup()
        {
            return new WhereExpressionBuilder<T, S>(this, criteriaBuilder.startGroup());
        }
        
        public WhereExpressionBuilder<T, S> endGroup()
        {
        	if(parentBuilder!=null) {
                throw new IllegalStateException("Group not started");
            }
        	criteriaBuilder.endGroup();
        	return parentBuilder;
        }
	}
}
