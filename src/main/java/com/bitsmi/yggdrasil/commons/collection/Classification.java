package com.bitsmi.yggdrasil.commons.collection;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Predicate;

public final class Classification <T> 
{
	public static final String KEY_NONE_MATCH = "__NONE_MATCH__";
	
	private Map<String, Entry<T>> entries;
	
	public Map<String, Entry<T>> getEntries() 
	{
		return entries;
	}
	
	public Optional<Entry<T>> getEntry(String key)
	{
		return Optional.ofNullable(entries.get(key));
	}
	
	public Optional<Entry<T>> getNoneMatchEntry()
	{
		return getEntry(KEY_NONE_MATCH);
	}

	void setEntries(Map<String, Entry<T>> entries) 
	{
		this.entries = entries;
	}
	
	void addEntry(Entry<T> entry)
	{
		if(entries==null) {
			entries = new HashMap<>();
		}
		entries.put(entry.getKey(), entry);
	}
	
	void classify(T elem)
	{
		boolean matches = false;
		for(String key:entries.keySet()) {
			if(KEY_NONE_MATCH.equals(key)) {
				continue;
			}
			
			Entry<T> entry = entries.get(key);
			Predicate<T> predicate = entry.getPredicate();
			if(predicate.test(elem)) {
				matches = true;
				entry.addResult(elem);
			}
		}
		
		if(!matches && entries.containsKey(KEY_NONE_MATCH)) {
			entries.get(KEY_NONE_MATCH).addResult(elem);
		}
	}
	
	void combine(Classification<T> classification)
	{
		for(String key:classification.entries.keySet()) {
			Entry<T> otherEntry = classification.entries.get(key);
			this.entries.get(key).addAllResults(otherEntry.getResults());
		}
	}
	
	public static final class Entry<T>
	{
		private String key;
		private Predicate<T> predicate;
		private List<T> results;
		
		public String getKey() 
		{
			return key;
		}
		
		void setKey(String key) 
		{
			this.key = key;
		}
		
		public Predicate<T> getPredicate() 
		{
			return predicate;
		}
		
		void setPredicate(Predicate<T> predicate) 
		{
			this.predicate = predicate;
		}
		
		public List<T> getResults() 
		{
			if(results==null) {
				return Collections.emptyList();
			}
			return results;
		}
		
		void setResults(List<T> results) 
		{
			this.results = results;
		}
		
		void addResult(T result)
		{
			if(results==null) {
				results = new ArrayList<>();
			}
			results.add(result);
		}
		
		void addAllResults(List<T> results)
		{
			if(results==null) {
				results = new ArrayList<>();
			}
			results.addAll(results);
		}
	}
}
