package com.bitsmi.yggdrasil.commons.collection;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class ClassifierMatcher <T>
{
	private List<ClassificationMatcher<T>> matchers = new ArrayList<>();
    private ClassificationMatcher<T> defaultMatcher;
	
	public ClassifierMatcher<T> matcher(String key, Predicate<T> predicate)
    {
        Objects.requireNonNull(predicate);
        
        ClassificationMatcher<T> matcher = new ClassificationMatcher<>(this)
        		.setKey(key)
                .setPredicate(predicate);
        
        matchers.add(matcher);
        
        return this;
    }
	
	public ClassifierMatcher<T> includeNonMatchResults()
    {
        ClassificationMatcher<T> matcher = new ClassificationMatcher<>(this)
        		.setKey(Classification.KEY_NONE_MATCH)
                .setPredicate(elem -> true);
        this.defaultMatcher = matcher;
        
        return this; 
    }
	
	Classification<T> build()
	{
		Map<String, Classification.Entry<T>> entries = matchers.stream()
				.map(this::classificationMatcherToClassificationEntryMapper)
				.collect(Collectors.toMap(Classification.Entry::getKey, Function.identity()));
		
		Classification<T> classification = new Classification<>();
		classification.setEntries(entries);
		
		// Default entry matcher (if defined)
		if(defaultMatcher!=null) {
			classification.addEntry(classificationMatcherToClassificationEntryMapper(defaultMatcher));
		}
		
		return classification;
	}
	
	private Classification.Entry<T> classificationMatcherToClassificationEntryMapper(ClassificationMatcher<T> matcher)
	{
		Classification.Entry<T> entry = new Classification.Entry<>();
		entry.setKey(matcher.getKey());
		entry.setPredicate(matcher.getPredicate());
		return entry;
	}
	
	public static final class ClassificationMatcher<T>
	{
		private ClassifierMatcher<T> builder;
		private String key;
		private Predicate<T> predicate;
		
		private ClassificationMatcher(ClassifierMatcher<T> builder)
        {
            this.builder = builder;
        }
		
		public String getKey() 
		{
			return key;
		}
		
		public ClassificationMatcher<T> setKey(String key) 
		{
			this.key = key;
			return this;
		}
		
		public Predicate<T> getPredicate() 
		{
			return predicate;
		}
		
		public ClassificationMatcher<T> setPredicate(Predicate<T> predicate) 
		{
			this.predicate = predicate;
			return this;
		}
		
		public ClassifierMatcher<T> and()
		{
			return builder;
		}
		
		public Classification<T> build()
		{
			return builder.build();
		}
	}		
}
