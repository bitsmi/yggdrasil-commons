package com.bitsmi.yggdrasil.commons.collection;

import java.util.stream.Stream;

public interface ICollectionView <T extends IQueryableData> 
{
	public Stream<? extends ViewRecord<T>> stream();
	public long size();
}
