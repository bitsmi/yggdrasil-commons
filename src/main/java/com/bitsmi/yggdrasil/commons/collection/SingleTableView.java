package com.bitsmi.yggdrasil.commons.collection;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class SingleTableView <T extends ITable<R>, R extends IQueryableData> implements ICollectionView<R> 
{
    private T sourceTable;
    private Set<String> filteredColumns;
    private Criteria criteria;
    private Long version;
    
    private List<TableViewRecord<R>> filteredData;
    
    SingleTableView <T, R> setSourceTable(T sourceTable)
    {
        this.sourceTable = sourceTable;
        return this;
    }
    
    SingleTableView <T, R> setFilteredColumns(Set<String> filteredColumns)
    {
        this.filteredColumns = filteredColumns;
        return this;
    }
    
    SingleTableView<T, R> setCriteria(Criteria criteria)
    {
        this.criteria = criteria;
        return this;
    }
    
    void refresh() 
    {
        long tableVersion = 0; 
        if(sourceTable instanceof IQueryableVersionSupport) {
            tableVersion = ((IQueryableVersionSupport)sourceTable).getVersion();
        }
        
        if(version==null || version!=tableVersion) {
            version = tableVersion;
            
            Stream<R> stream = sourceTable.stream();
            if(criteria!=null) {
                stream = stream.filter(elem -> criteria.evaluate(elem.getValues()));
            }
            
            filteredData = stream
                .map(elem -> new TableViewRecord<R>(elem, filteredColumns))
                .collect(Collectors.toList());
        }
    }
    
    public long getVersion()
    {
        return version;
    }

    public TableViewRecord<R> get(int index)
    {
        assertInitialized();
        if(filteredData.size()<=Math.abs(index)) {
            throw new IndexOutOfBoundsException("Index out of bounds");
        }
        
        // Refresh view if needed
        refresh();
        
        if(index<0) {
            index = filteredData.size() - index;
        }
        
        return filteredData.get(index);
    }
    
    public Object getValue(int index, String columnName)
    {
        assertInitialized();
        if(filteredColumns!=null && !filteredColumns.contains(columnName)) {
            throw new IllegalArgumentException("Unknown column: " + columnName);
        }
        
        // Refresh view if needed
        refresh();
        
        TableViewRecord<R> rowData = get(index);
        return rowData.getValue(columnName);
    }
    
    @Override
    public Stream<TableViewRecord<R>> stream() 
    {
        assertInitialized();
        
        // Refresh view if needed
        refresh();
        
        return filteredData.stream();
    }

    @Override
    public long size() 
    {
        assertInitialized();
        
        // Refresh view if needed
        refresh();
        
        if(this.filteredData==null) {
            return sourceTable.size();
        }
        return filteredData.size();
    }
    
    private void assertInitialized()
    {
        if(filteredData==null) {
            throw new IllegalStateException("View not initialized");
        }
    }
}
