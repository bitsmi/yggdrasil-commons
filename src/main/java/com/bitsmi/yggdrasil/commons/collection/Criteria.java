package com.bitsmi.yggdrasil.commons.collection;

import java.util.Map;

import com.bitsmi.yggdrasil.commons.exception.MissingValueException;

/**
 * Used as filter in Table Views
 * @deprecated Table & view APIs will be refactored
 */
@Deprecated
public class Criteria 
{
    private AbstractExpression expression;

    void setExpression(AbstractExpression expression)
    {
        this.expression = expression;
    }
    
    public boolean evaluate(Map<String, Object> context)
    {
        if(expression==null) {
            return false;
        }
        
        return expression.evaluate(context);
    }
    
    public CriteriaBuilder isTrue(String expression)
    {
        CriteriaBuilder builder = new CriteriaBuilder(this)
            .isTrue(expression);
        return builder;
    }
    
    public CriteriaBuilder isFalse(String expression)
    {
        CriteriaBuilder builder = new CriteriaBuilder(this)
            .isFalse(expression);
        return builder;
    }
    
    public CriteriaBuilder isNull(String expression)
    {
    	CriteriaBuilder builder = new CriteriaBuilder(this)
        	.isNull(expression);
        return builder;
    }
    
    public CriteriaBuilder isNotNull(String expression)
    {
    	CriteriaBuilder builder = new CriteriaBuilder(this)
    		.isNotNull(expression);
        return builder;
    }
    
    public CriteriaBuilder isEqualsToValue(String expression, Object value)
    {
        CriteriaBuilder builder = new CriteriaBuilder(this)
            .isEqualsToValue(expression, value);
        return builder;
    }
    
    public CriteriaBuilder isEqualsToReference(String expression, String value)
    {
        CriteriaBuilder builder = new CriteriaBuilder(this)
            .isEqualsToReference(expression, value);
        return builder;
    }
    
    public CriteriaBuilder isNotEqualsToValue(String expression, Object value)
    {
        CriteriaBuilder builder = new CriteriaBuilder(this)
            .isNotEqualsToValue(expression, value);
        return builder;
    }
    
    public CriteriaBuilder isNotEqualsToReference(String expression1, String expression2)
    {
        CriteriaBuilder builder = new CriteriaBuilder(this)
            .isNotEqualsToReference(expression1, expression2);
        return builder;
    }
    
    public CriteriaBuilder isGreaterThanValue(String expression, Comparable<?> value)
    {
    	CriteriaBuilder builder = new CriteriaBuilder(this)
    		.isGreaterThanValue(expression, value);
    	return builder;
    }
    
    public CriteriaBuilder isGreaterThanReference(String expression1, String expression2)
    {
    	CriteriaBuilder builder = new CriteriaBuilder(this)
    		.isGreaterThanReference(expression1, expression2);
    	return builder;
    }
    
    public CriteriaBuilder isGreaterOrEqualsToValue(String expression, Comparable<?> value)
    {
    	CriteriaBuilder builder = new CriteriaBuilder(this)
    		.isGreaterOrEqualsToValue(expression, value);
    	return builder;
    }
    
    public CriteriaBuilder isGreaterOrEqualsToReference(String expression1, String expression2)
    {
    	CriteriaBuilder builder = new CriteriaBuilder(this)
    		.isGreaterOrEqualsToReference(expression1, expression2);
    	return builder;
    }
    
    public CriteriaBuilder isLessThanValue(String expression, Comparable<?> value)
    {
    	CriteriaBuilder builder = new CriteriaBuilder(this)
    		.isLessThanValue(expression, value);
    	return builder;
    }
    
    public CriteriaBuilder isLessThanReference(String expression1, String expression2)
    {
    	CriteriaBuilder builder = new CriteriaBuilder(this)
    		.isLessThanReference(expression1, expression2);
    	return builder;
    }
    
    public CriteriaBuilder isLessOrEqualsToValue(String expression, Comparable<?> value)
    {
    	CriteriaBuilder builder = new CriteriaBuilder(this)
    		.isLessOrEqualsToValue(expression, value);
    	return builder;
    }
    
    public CriteriaBuilder isLessOrEqualsToReference(String expression1, String expression2)
    {
    	CriteriaBuilder builder = new CriteriaBuilder(this)
    		.isLessOrEqualsToReference(expression1, expression2);
    	return builder;
    }
    
    @Override
    public String toString()
    {
        if(expression==null) {
            return "[EMPTY]";
        }
        return expression.toString();
    }
    
    /*------------------------------------*
     * HELPER CLASSES
     *------------------------------------*/
    public static class CriteriaBuilder
    {
    	private Criteria filter;
        private AbstractExpression rootExpression;
        private CriteriaBuilder parentBuilder;
        private boolean negateNextExpression = false;
        
        CriteriaBuilder(Criteria filter)
        {
            this.filter = filter;
        }
        
        CriteriaBuilder(CriteriaBuilder parentBuilder)
        {
            this.parentBuilder = parentBuilder;
        }
        
        public Criteria build()
        {
            if(parentBuilder!=null) {
                throw new IllegalStateException("Group not closed");
            }
            
            filter.setExpression(rootExpression);
            return filter;
        }
        
        public CriteriaBuilder not()
        {
        	this.negateNextExpression = true;
        	return this;
        }
        
        public CriteriaBuilder isTrue(String expression)
        {
        	return addUnaryOperation(Operator.TRUE, new Operand(true, expression));
        }
        
        public CriteriaBuilder isFalse(String expression)
        {
        	return addUnaryOperation(Operator.FALSE, new Operand(true, expression));
        }
        
        public CriteriaBuilder isNull(String expression)
        {
        	return isEqualsToValue(expression, null);
        }
        
        public CriteriaBuilder isNotNull(String expression)
        {
        	return isNotEqualsToValue(expression, null);
        }
        
        public CriteriaBuilder isEqualsToValue(String expression, Object value)
        {
        	return addBinaryOperation(Operator.EQ, new Operand(true, expression), new Operand(false, value));
        }
        
        public CriteriaBuilder isEqualsToReference(String expression1, String expression2)
        {
        	return addBinaryOperation(Operator.EQ, new Operand(true, expression1), new Operand(true, expression2));
        }
        
        public CriteriaBuilder isNotEqualsToValue(String expression, Object value)
        {
        	return addBinaryOperation(Operator.NOTEQ, new Operand(true, expression), new Operand(false, value));
        }
        
        public CriteriaBuilder isNotEqualsToReference(String expression1, String expression2)
        {
        	return addBinaryOperation(Operator.NOTEQ, new Operand(true, expression1), new Operand(true, expression2));
        }
        
        public CriteriaBuilder isGreaterThanValue(String expression, Comparable<?> value)
        {
        	return addBinaryOperation(Operator.GT, new Operand(true, expression), new Operand(false, value));
        }
        
        public CriteriaBuilder isGreaterThanReference(String expression1, String expression2)
        {
        	return addBinaryOperation(Operator.GT, new Operand(true, expression1), new Operand(true, expression2));
        }
        
        public CriteriaBuilder isGreaterOrEqualsToValue(String expression, Comparable<?> value)
        {
        	return addBinaryOperation(Operator.GTEQ, new Operand(true, expression), new Operand(false, value));
        }
        
        public CriteriaBuilder isGreaterOrEqualsToReference(String expression1, String expression2)
        {
        	return addBinaryOperation(Operator.GTEQ, new Operand(true, expression1), new Operand(true, expression2));
        }
        
        public CriteriaBuilder isLessThanValue(String expression, Comparable<?> value)
        {
        	return addBinaryOperation(Operator.LT, new Operand(true, expression), new Operand(false, value));
        }
        
        public CriteriaBuilder isLessThanReference(String expression1, String expression2)
        {
        	return addBinaryOperation(Operator.LT, new Operand(true, expression1), new Operand(true, expression2));
        }
        
        public CriteriaBuilder isLessOrEqualsToValue(String expression, Comparable<?> value)
        {
        	return addBinaryOperation(Operator.LTEQ, new Operand(true, expression), new Operand(false, value));
        }
        
        public CriteriaBuilder isLessOrEqualsToReference(String expression1, String expression2)
        {
        	return addBinaryOperation(Operator.LTEQ, new Operand(true, expression1), new Operand(true, expression2));
        }
        
        private CriteriaBuilder addUnaryOperation(Operator operator, Operand operand)
        {
        	if(!operator.isUnaryOperator()) {
        		throw new IllegalArgumentException("Illegal operator for unary operations");
        	}
        	
        	AbstractExpression operationExpression = manageSpecialModifiers(new PlainExpression(operator, operand));
            if(rootExpression==null) {
                rootExpression = new ComposableExpression(operationExpression);
            }
            else if(rootExpression.isComposable()) {
                rootExpression.addOperand(operationExpression);
            }
            else {
                throw new IllegalStateException("Use AND, OR operators to compose expression");
            }
            
            return this;
        }
        
        private CriteriaBuilder addBinaryOperation(Operator operator, Operand op1, Operand op2)
        {
        	if(!operator.isBinaryOperator()) {
        		throw new IllegalArgumentException("Illegal operator for binary operations");
        	}
        	
        	AbstractExpression operationExpression = manageSpecialModifiers(new PlainExpression(operator, op1, op2));
            if(rootExpression==null) {
                rootExpression = new ComposableExpression(operationExpression);
            }
            else if(rootExpression.isComposable()) {
                rootExpression.addOperand(operationExpression);
            }
            else {
                throw new IllegalStateException("Use AND, OR operators to compose expression");
            }
            
            return this;
        }
        
        public CriteriaBuilder and()
        {
            return addOperation(LogicalOperator.AND);
        }
        
        public CriteriaBuilder or()
        {
            return addOperation(LogicalOperator.OR);
        }
        
        public CriteriaBuilder startGroup()
        {
            CriteriaBuilder childBuilder = new CriteriaBuilder(this);
            return childBuilder;
        }
        
        public CriteriaBuilder endGroup()
        {
            if(parentBuilder==null) {
                throw new IllegalStateException("Group nor started");
            }
            
            AbstractExpression groupExpression = parentBuilder.manageSpecialModifiers(new GroupExpression(rootExpression));
            parentBuilder.rootExpression.addOperand(groupExpression);
            
            return parentBuilder;
        }
        
        protected CriteriaBuilder addOperation(LogicalOperator operator)
        {
            if(rootExpression==null) {
                throw new IllegalStateException("Empty expression");
            }
            rootExpression.addOperation(operator);
            
            return this;
        }
        
        protected AbstractExpression manageSpecialModifiers(AbstractExpression operationExpression)
        {
        	if(negateNextExpression) {
            	operationExpression = new NegatedExpression(operationExpression);
            	negateNextExpression = false;
            }
        	return operationExpression;
        }
    }
    
    private static abstract class AbstractExpression
    {
        protected abstract boolean isComposable();
        protected abstract void addOperation(LogicalOperator operator);
        protected abstract void addOperand(AbstractExpression operand);
        protected abstract boolean evaluate(Map<String, Object> context);
    }
    
    private static class PlainExpression extends AbstractExpression
    {
        private Operand leftOperand;
        private Operator operator;
        private Operand rightOperand;
        
        public PlainExpression(Operator operator, Operand operand)
        {
            if(operator.getNumberOfOperands()!=1) {
                throw new IllegalArgumentException("Incorrect operator for unary operations: " + operator);
            }
            this.leftOperand = operand;
            this.operator = operator;
        }
        
        public PlainExpression(Operator operator, Operand leftOperand, Operand rightOperand)
        {
            if(operator.getNumberOfOperands()!=2) {
                throw new IllegalArgumentException("Incorrect operator for binary operations: " + operator);
            }
            this.leftOperand = leftOperand;
            this.rightOperand = rightOperand;
            this.operator = operator;
        }
        
        @Override
        protected boolean isComposable()
        {
            return false;
        }
        
        @Override
        protected void addOperation(LogicalOperator operator)
        {
            throw new UnsupportedOperationException("Plain expression doesn't support operations");
        }
        
        @Override
        protected void addOperand(AbstractExpression operand)
        {
            throw new UnsupportedOperationException("Plain expression doesn't support operands");
        }
        
        @Override
        protected boolean evaluate(Map<String, Object> context)
        {
            Object leftValue = extractOperandValue(context, leftOperand);
            Object rightValue = null;
            if(operator.isBinaryOperator()) {
                rightValue = extractOperandValue(context, rightOperand);
            }
            
            boolean result = false;
            switch(operator) {
                case TRUE:
                    result = Boolean.parseBoolean(leftValue.toString());
                    break;
                case FALSE:
                    result = !Boolean.parseBoolean(leftValue.toString());
                    break;
                case EQ:
                	result = leftValue==null && rightValue==null
            				|| leftValue!=null && leftValue.equals(rightValue)
            				|| rightValue!=null && rightValue.equals(leftValue);
                    break;
                case NOTEQ:
                    result = leftValue==null && rightValue!=null
                    		|| leftValue!=null && !leftValue.equals(rightValue)
                    		|| rightValue!=null && !rightValue.equals(leftValue);
                    break;
                case GT:
                case GTEQ:
                case LT:
                case LTEQ:
                	int comparison = 0;
                	if(leftValue instanceof Comparable
                			&& leftValue.getClass().getName().equals(rightValue.getClass().getName())) {
                		comparison = ((Comparable)leftValue).compareTo(rightValue);
                	}
                	else {
                		throw new IllegalStateException(leftOperand + " cannot be compared with " + rightOperand);
                	}
                	result = Operator.GT==operator && comparison>0
                			|| Operator.GTEQ==operator && comparison>=0
                			|| Operator.LT==operator && comparison<0
                			|| Operator.LTEQ==operator && comparison<=0;
                	break;
            }
            
            return result;
        }
        
        private Object extractOperandValue(Map<String, Object> context, Operand operand)
        {
            Object value = null;
            if(operand.isContextKey()) {
            	if(!context.containsKey(operand.getContextKey())) {
            		throw new MissingValueException("Value not found for key " + operand.toString());
            	}
                value = context.get(operand.getContextKey());
            }
            else {
                value = operand.getValue();
            }
            
            return value;
        }
        
        @Override
        public String toString()
        {
            if(operator.isUnaryOperator()) {
                return operator.getSymbol() +  leftOperand.toString();
            }
            else {
                return leftOperand.toString() + " " + operator.getSymbol() + " " + rightOperand.toString();
            }
        }
    }
    
    private static class ComposableExpression extends AbstractExpression
    {
        private AbstractExpression leftOperand;
        private LogicalOperator operator;
        private AbstractExpression rightOperand;
        // Expressions have only one expandable operand
        private AbstractExpression expandableOperand;
        
        ComposableExpression(AbstractExpression leftOperand)
        {
            this.leftOperand = leftOperand;
        }
        
        @Override
        protected boolean isComposable()
        {
            return true;
        }
        
        @Override
        protected void addOperation(LogicalOperator operator)
        {
            if(rightOperand==null && this.operator!=null) {
                throw new IllegalStateException("Operator already specified");
            }
            else if(rightOperand==null) {
                this.operator = operator;
            }
            else if(expandableOperand!=null) {
                expandableOperand.addOperation(operator);
            }
            else if(LogicalOperator.AND==operator) {
                if(!rightOperand.isComposable()) {
                    rightOperand = new ComposableExpression(rightOperand);
                    expandableOperand = rightOperand;
                }
                rightOperand.addOperation(operator);
            }
            else if(LogicalOperator.OR==operator) {
                // Wrap current expression
                leftOperand = new ComposableExpression(leftOperand);
                leftOperand.addOperation(this.operator);
                leftOperand.addOperand(rightOperand);
                rightOperand = null;
                expandableOperand = null;
                this.operator = operator;
            }
        }
        
        @Override
        protected void addOperand(AbstractExpression operand)
        {
            assertRightOperandReady();
            if(rightOperand==null) {
                this.rightOperand = operand;
            }
            else {
                expandableOperand.addOperand(operand);
            }
        }
        
        void assertRightOperandReady()
        {
            if(leftOperand==null || operator==null) {
                throw new IllegalStateException("Left operand and operator are mandatory to complete the expression");
            }
        }
        
        @Override
        protected boolean evaluate(Map<String, Object> context)
        {
            boolean leftOperandResult = leftOperand.evaluate(context);
            boolean rightOperandResult = rightOperand.evaluate(context);
            if(LogicalOperator.AND==operator) {
                return leftOperandResult && rightOperandResult;
            }
            else if(LogicalOperator.OR==operator) {
                return leftOperandResult || rightOperandResult;
            }
            return false;
        }
        
        @Override
        public String toString()
        {
            StringBuilder builder = new StringBuilder("[")
                .append(leftOperand.toString());
            if(operator!=null) {
                builder.append(" ").append(operator).append(" ");
            }
            if(rightOperand!=null) {
                builder.append(rightOperand);
            }
            builder.append("]");
            
            return builder.toString();
        }
    }

    private static class GroupExpression extends AbstractExpression
    {
        private AbstractExpression expression;
        
        GroupExpression(AbstractExpression expression)
        {
            this.expression = expression;
        }
        
        @Override
        protected boolean isComposable()
        {
            return false;
        }
        
        @Override
        protected void addOperation(LogicalOperator operator)
        {
            throw new UnsupportedOperationException("Group expression doesn't support operations");
        }
        
        @Override
        protected void addOperand(AbstractExpression operand)
        {
            throw new UnsupportedOperationException("Group expression doesn't support operands");
        }
        
        @Override
        protected boolean evaluate(Map<String, Object> context)
        {
            return expression.evaluate(context);
        }
        
        @Override
        public String toString()
        {
            return new StringBuilder("(")
                .append(expression.toString())
                .append(")")
                .toString();
        }
    }
    
    private static class NegatedExpression extends AbstractExpression
    {
    	private AbstractExpression expression;
    	
    	public NegatedExpression(AbstractExpression expression)
    	{
    		this.expression = expression;
    	}
    	
    	@Override
        protected boolean isComposable()
        {
            return false;
        }
        
        @Override
        protected void addOperation(LogicalOperator operator)
        {
            throw new UnsupportedOperationException("Negated expression doesn't support operations");
        }
        
        @Override
        protected void addOperand(AbstractExpression operand)
        {
            throw new UnsupportedOperationException("Negated expression doesn't support operands");
        }
        
        @Override
        protected boolean evaluate(Map<String, Object> context)
        {
            return !expression.evaluate(context);
        }
        
        @Override
        public String toString()
        {
        	return new StringBuilder()
        			.append("!")
        			.append(expression.toString())
        			.toString();
        }
    }
    
    private static class Operand
    {
        private boolean contextKey;
        private Object value;
        
        public Operand(boolean contextKey, Object value)
        {
            this.contextKey = contextKey;
            this.value = value;
        }
        
        public boolean isContextKey()
        {
            return contextKey;
        }
        
        public Object getValue()
        {
            return value;
        }
        
        public String getContextKey()
        {
            return value.toString();
        }
        
        @Override
        public String toString()
        {
            if(contextKey) {
                return "{" + value + "}";
            }
            else {
                return value.toString();
            }
        }
    }
    
    private enum LogicalOperator
    {
        AND, OR;
    }
    
    private enum Operator
    {
        TRUE(1, ""), FALSE(1, "!"), EQ(2, "=="), NOTEQ(2, "!="), GTEQ(2, ">="), GT(2, ">"), LTEQ(2, "<="), LT(2, "<");
        
        private String symbol;
        private int numberOfOperands;
        
        private Operator(int numberOfOperands, String symbol)
        {
            this.numberOfOperands = numberOfOperands;
            this.symbol = symbol;
        }

        public String getSymbol()
        {
            return symbol;
        }
        
        public int getNumberOfOperands()
        {
            return  numberOfOperands;
        }
        
        public boolean isUnaryOperator()
        {
            return numberOfOperands==1;
        }
        
        public boolean isBinaryOperator()
        {
            return numberOfOperands==2;
        }
    }
}
