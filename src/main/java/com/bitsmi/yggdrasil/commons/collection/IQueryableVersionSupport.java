package com.bitsmi.yggdrasil.commons.collection;

public interface IQueryableVersionSupport 
{
	public long getVersion();
	public long incrementAndGetVersion();
}
