package com.bitsmi.yggdrasil.commons.validation;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.function.BinaryOperator;

public class Validation
{
    public static final String EXECUTION_GROUP_DEFAULT = "Default";
    
	private ValidationExecution execution;
	private Result result;
    private List<ConstraintViolation> constraintViolations = new ArrayList<>();
    
    public Validation(ValidationExecution execution)
    {
        this.execution = execution;
    }
    
    public static final Validation validationWithDefaultExecutionGroup()
    {
        return new Validation(new ValidationExecution(true));
    }
    
    public static final Validation validationWithoutExecutionGroups()
    {
        return new Validation(new ValidationExecution(false));
    }
    
    public ValidationExecution getExecution()
    {
    	return execution;
    }
    
    public boolean isSuccess()
    {
        return Result.SUCCEEDED==result;
    }
    
    public boolean isFailed()
    {
        return Result.FAILED==result;
    }
    
    public boolean isSkipped()
    {
    	return Result.SKIPPED==result;
    }
    
    public void success()
    {
        this.result = Result.SUCCEEDED;
    }
    
    public void fail()
    {
    	this.result = Result.FAILED;
    }
    
    public void skip()
    {
    	this.result = Result.SKIPPED;
    }
    
    public List<ConstraintViolation> getConstraintViolations()
    {
        return constraintViolations;
    }
    
    public void addConstraintViolation(ConstraintViolation violation)
    {
    	this.constraintViolations.add(violation);
    }
    
    public static class ValidationExecution
    {
        Map<String, ConstraintGroupExecution> groupExecutions = new LinkedHashMap<>();
        ConstraintGroupExecution selectedGroup = null;
        
        /**
         * Choose if the default validation group must be created or not
         * @param initializeDefaultGroup boolean - true if default group must be created, false otherwise   
         */
        public ValidationExecution(boolean initializeDefaultGroup)
        {
            if(initializeDefaultGroup) {
                selectGroup(EXECUTION_GROUP_DEFAULT);
            }
        }
        
        public List<ConstraintGroupExecution> getAllGroupExecutions()
        {
            return new ArrayList<>(groupExecutions.values());
        }
        
        public ConstraintGroupExecution getGroupExecution(String groupName)
        {
            return groupExecutions.get(groupName);
        }
        
        public ConstraintGroupExecution selectGroup(String groupName)
        {
            if(!groupExecutions.containsKey(groupName)) {
                groupExecutions.put(groupName, new ConstraintGroupExecution(groupName));
            }
            selectedGroup = groupExecutions.get(groupName);
            
            return selectedGroup;
        }
        
        public ConstraintGroupExecution selectedGroup()
        {
            if(selectedGroup==null) {
                throw new IllegalStateException("No group selected");
            }
            
            return selectedGroup;
        }
        
        public ValidationExecution addStatementExecution(ConstraintStatementExecution statementExecution)
        {
            Objects.requireNonNull(statementExecution);
            
            if(selectedGroup==null) {
                throw new IllegalStateException("No group selected");
            }
            
            selectedGroup.addStatementExecution(statementExecution);
            
            return this;
        }
    }
    
    public static enum Result
    {
        SUCCEEDED,
        FAILED,
        SKIPPED;
    }
        
    /*-------------------------------------------*
     * Constraint statements execution info
     *-------------------------------------------*/
    public static class ConstraintGroupExecution
    {
        private String name;
        private Result result;
        private List<ConstraintStatementExecution> statementExecutions = new ArrayList<>();
        
        private ConstraintGroupExecution(String name)
        {
            this.name = name;
        }
        
        public String getName()
        {
            return name;
        }
        
        public List<ConstraintStatementExecution> getStatementExecutions()
        {
            return statementExecutions;
        }
        
        public ConstraintGroupExecution addStatementExecution(ConstraintStatementExecution statement)
        {
            Objects.requireNonNull(statement);
            
            statementExecutions.add(statement);
            
            return this;
        }
        
        public ConstraintGroupExecution setResult(Result result)
        {
            this.result = result;
            
            return this;
        }
        
        public boolean isSuccess()
        {
            return Result.SUCCEEDED==result;
        }
        
        public boolean isFail()
        {
            return Result.FAILED==result;
        }
        
        public boolean isSkipped()
        {
            return Result.SKIPPED==result;
        }
        
        public ConstraintGroupExecution success()
        {
            return setResult(Result.SUCCEEDED);
        }
        
        public ConstraintGroupExecution fail()
        {
            return setResult(Result.FAILED);
        }
        
        public ConstraintGroupExecution skip()
        {
            return setResult(Result.SKIPPED);
        }
    }
        
    public static class ConstraintStatementExecution
    {
        private String name;
        private Object value;
        private AbstractConstraintExecution constraintExecution;
        private Result result;
        
        private ConstraintStatementExecution(String name, Object value, AbstractConstraintExecution constraintExecution)
        {
            Objects.requireNonNull(name);
            Objects.requireNonNull(constraintExecution);
            
            this.name = name;
            this.value = value;
            this.constraintExecution = constraintExecution;
            this.result = constraintExecution.getResult();
        }
        
        private ConstraintStatementExecution(String name, Result result)
        {
            Objects.requireNonNull(name);
            Objects.requireNonNull(result);
            
            this.name = name;
            this.result = result;
        }
        
        public String getName()
        {
            return name;
        }
        
        public Object getValue()
        {
            return value;
        }
        
        public AbstractConstraintExecution getConstraintExecution()
        {
            return constraintExecution;
        }
        
        public Result getResult()
        {
            return result;
        }
        
        public boolean isSuccess()
        {
            return Result.SUCCEEDED==result;
        }
        
        public boolean isFail()
        {
            return Result.FAILED==result;
        }
        
        public boolean isSkipped()
        {
            return Result.SKIPPED==result;
        }
    }
        
    public static ConstraintStatementExecution executedStatement(String name, Object value, AbstractConstraintExecution constraintExecution)
    {
        return new ConstraintStatementExecution(name, value, constraintExecution);
    }
    
    public static ConstraintStatementExecution skippedStatement(String name)
    {
        return new ConstraintStatementExecution(name, Result.SKIPPED);
    }
    
    public static enum ConstraintOperator
    {
        AND {
            public boolean identityValue() 
            {
                return true;
            }
            public boolean isFailFast()
            {
                return true;
            }
        },
        OR {
            public boolean identityValue()
            {
                return false;
            }
            public boolean isFailFast()
            {
                return false;
            }
        };
        
        public abstract boolean identityValue();
        public abstract boolean isFailFast();
    }
    
    static BinaryOperator<Result> getResultOperator(ConstraintOperator operator)
    {
        Objects.requireNonNull(operator);
        
        BinaryOperator<Result> resultOperator = null;
        if(operator==ConstraintOperator.AND) {
            resultOperator = (currentResult, nextResult) -> {
                if((currentResult==null || currentResult==Result.SUCCEEDED) && nextResult==Result.SUCCEEDED) {
                    return Result.SUCCEEDED;
                }
                else {
                    return Result.FAILED;
                }
            };
        }
        // OR
        else {
            resultOperator = (currentResult, nextResult) -> {
                if(currentResult==null || currentResult==Result.SUCCEEDED || nextResult==Result.SUCCEEDED) {
                    return Result.SUCCEEDED;
                }
                else {
                    return Result.FAILED;
                }
            };
        }
        
        return resultOperator;
    }
    
    public static class AbstractConstraintExecution
    {
        protected Object value;
        protected Result result;
        // Display value in toString
        protected boolean valueVisibleInToString = false;
        
        public Result getResult()
        {
            return result;
        }
        
        public Object getValue()
        {
            return value;
        }
        
        public AbstractConstraintExecution setValue(Object value)
        {
            this.value = value;
            return this;
        }
        
        public boolean isValueVisibleInToString()
        {
            return valueVisibleInToString && result!=null && result!=Result.SKIPPED;
        }
        
        public AbstractConstraintExecution setValueVisibleInToString(boolean valueVisibleInToString)
        {
            this.valueVisibleInToString = valueVisibleInToString;
            return this;
        }
        
        public AbstractConstraintExecution setResult(Result result)
        {
            this.result = result;
            return this;
        }
        
        public boolean isSuccess()
        {
            return Result.SUCCEEDED==result;
        }
        
        public boolean isFail()
        {
            return Result.FAILED==result;
        }
        
        public boolean isSkipped()
        {
            return Result.SKIPPED==result;
        }
        
        public AbstractConstraintExecution success()
        {
            return setResult(Result.SUCCEEDED);
        }
        
        public AbstractConstraintExecution fail()
        {
            return setResult(Result.FAILED);
        }
        
        public AbstractConstraintExecution skip()
        {
            return setResult(Result.SKIPPED);
        }
    }
        
    public static class PlainConstraintExecution extends AbstractConstraintExecution
    {
        protected String expression;
        
        private PlainConstraintExecution(String expression)
        {
            this.expression = expression;
        }
        
        @Override
        public PlainConstraintExecution setValue(Object value)
        {
            return (PlainConstraintExecution)super.setValue(value);
        }

        @Override
        public PlainConstraintExecution setValueVisibleInToString(boolean valueVisibleInToString)
        {
            return (PlainConstraintExecution)super.setValueVisibleInToString(valueVisibleInToString);
        }

        @Override
        public PlainConstraintExecution setResult(Result result)
        {
            return (PlainConstraintExecution)super.setResult(result);
        }

        @Override
        public PlainConstraintExecution success()
        {
            return (PlainConstraintExecution)super.success();
        }

        @Override
        public PlainConstraintExecution fail()
        {
            return (PlainConstraintExecution)super.fail();
        }

        @Override
        public PlainConstraintExecution skip()
        {
            return (PlainConstraintExecution)super.skip();
        }

        @Override
        public String toString() 
        {
            StringBuilder builder = new StringBuilder();
            if(isValueVisibleInToString()) {
                builder.append("{Value = ").append(value).append("} --> ");
            }
            
            // Constraint may be already enclosed with "[]" if comes from Validators Constraint
            if(!expression.startsWith("[")) {
                expression = "[" + expression + "]";
            }
            
            builder.append(expression)
                    .append("{").append(result.name()).append("}");
            
            return builder.toString();
        }
    }
    
    public static PlainConstraintExecution executedPlainConstraint(String expression, Object value)
    {
        Objects.requireNonNull(expression);
        
        PlainConstraintExecution constraint = new PlainConstraintExecution(expression);
        constraint.setValue(value);
        
        return constraint;
    }
    
    public static PlainConstraintExecution skippedPlainConstraint(String expression)
    {
        Objects.requireNonNull(expression);
        
        PlainConstraintExecution constraint = new PlainConstraintExecution(expression);
        constraint.skip();
        
        return constraint;
    }
    
    public static class ComposedConstraintExecution extends AbstractConstraintExecution
    {
        protected ConstraintOperator operator;
        protected List<AbstractConstraintExecution> nestedExecutions = new ArrayList<>();
        
        private ComposedConstraintExecution(ConstraintOperator operator)
        {
            Objects.requireNonNull(operator);
            
            this.operator = operator;
        }
        
        public List<AbstractConstraintExecution> getNestedExecutions()
        {
            return this.nestedExecutions;
        }
        
        public void addNestedExecution(AbstractConstraintExecution nestedExecution)
        {
            if(nestedExecution.getResult()==null) {
                throw new IllegalArgumentException("Nested execution result must be defined for (" + nestedExecution.toString() + ")");
            }
            
            nestedExecutions.add(nestedExecution);
            BinaryOperator<Result> resultOperator = getResultOperator(operator);
            Result newResult = resultOperator.apply(this.result, nestedExecution.result);
            this.setResult(newResult);
        }
        
        @Override
        public ComposedConstraintExecution setValue(Object value)
        {
            return (ComposedConstraintExecution)super.setValue(value);
        }

        @Override
        public ComposedConstraintExecution setValueVisibleInToString(boolean valueVisibleInToString)
        {
            return (ComposedConstraintExecution)super.setValueVisibleInToString(valueVisibleInToString);
        }

        @Override
        public ComposedConstraintExecution setResult(Result result)
        {
            return (ComposedConstraintExecution)super.setResult(result);
        }

        @Override
        public ComposedConstraintExecution success()
        {
            return (ComposedConstraintExecution)super.success();
        }

        @Override
        public ComposedConstraintExecution fail()
        {
            return (ComposedConstraintExecution)super.fail();
        }

        @Override
        public ComposedConstraintExecution skip()
        {
            return (ComposedConstraintExecution)super.skip();
        }
        
        @Override
        public String toString()
        {
            StringBuilder builder = new StringBuilder();
            if(isValueVisibleInToString()) {
                builder.append("{Value = ").append(value).append("} --> ");
            }
            
            builder.append("[");
            for(int i=0; i<nestedExecutions.size(); i++) {
                AbstractConstraintExecution c = nestedExecutions.get(i);
                if(i>0) {
                    builder.append(" ").append(operator.name()).append(" ");
                }
                builder.append("(").append(c.toString()).append(")");
            }
            
            return builder
                    .append("]{").append(result.name()).append("}")
                    .toString();
        }
    }
    
    public static ComposedConstraintExecution executedComposedConstraint(ConstraintOperator operator, Object value)
    {
        ComposedConstraintExecution constraint = new ComposedConstraintExecution(operator);
        constraint.setValue(value);
        
        return constraint;
    }
    
    public static ComposedConstraintExecution skippedComposedConstraint(ConstraintOperator operator)
    {
        ComposedConstraintExecution constraint = new ComposedConstraintExecution(operator);
        constraint.skip();
        
        return constraint;
    }
}
