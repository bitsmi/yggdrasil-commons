package com.bitsmi.yggdrasil.commons.validation;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Objects;
import java.util.function.Function;
import java.util.function.Predicate;

import com.bitsmi.yggdrasil.commons.validation.Constraints.IConstraint;
import com.bitsmi.yggdrasil.commons.validation.Validators.ConstraintStatementGroup;
import com.bitsmi.yggdrasil.commons.validation.Validators.ValidationMode;

public class BeanValidator<T> implements IValidator<T>
{
    ValidationMode validationMode;
    Map<String, ConstraintStatementGroup<T>> groups = new LinkedHashMap<>();
    
    BeanValidator(ValidationMode validationMode)
    {
        if(validationMode==null) {
            this.validationMode = ValidationMode.FAIL_FAST;
        }
        else {
            this.validationMode = validationMode;
        }
    }
    
    public static <T> BeanValidatorBuilder<T> builder()
    {
    	return new BeanValidatorBuilder<>();
    }
    
    BeanValidator<T> setGroups(Map<String, ConstraintStatementGroup<T>> groups)
    {
    	this.groups = groups;
    	return this;
    }
    
    @Override
    public Validation validate(T instance)
    {
        // Default execution group will be created group.test() if needed
        Validation validation = Validation.validationWithoutExecutionGroups();
        
        boolean success = true;
        for(String key:groups.keySet()) {
            ConstraintStatementGroup<T> group = groups.get(key);
            if(!success && validationMode==ValidationMode.FAIL_FAST) {
                group.skip(validation);
            }
            else if(group.predicate.test(instance)) {
                success = success && group.test(validation, instance, validationMode);
            }
            else {
                group.skip(validation);
            }
        }
        
        if(success) {
            validation.success();
        }
        else {
            validation.fail();
        }
        
        return validation;
    }
    
    public static class BeanValidatorBuilder<T>
    {
    	ValidationMode validationMode;
        Map<String, ConstraintStatementGroup<T>> groups = new LinkedHashMap<>();
        ConstraintStatementGroup<T> currentGroup = null;
        
        public BeanValidatorBuilder<T> withValidationMode(ValidationMode validationMode)
        {
        	Objects.requireNonNull(validationMode);
        	this.validationMode = validationMode;
        	return this;
        }
        
    	public BeanValidatorBuilder<T> group(String name)
        {
            return group(name, v -> true);
        }
        
        public BeanValidatorBuilder<T> group(String name, Predicate<T> predicate)
        {
            if(groups.containsKey(name)) {
                throw new IllegalArgumentException("Group already registered: " + name);
            }
            
            ConstraintStatementGroup<T> group = new ConstraintStatementGroup<>(name, predicate);
            groups.put(name, group);
            currentGroup = group;
            
            return this;
        }
        
        
        public <R> BeanValidatorBuilder<T> constraint(Function<T, R> extractor, IConstraint<R> constraint)
        {
        	getCurrentGroup()
		            .constraint(extractor, constraint);
        	
        	return this;
        }
        
        public <R> BeanValidatorBuilder<T> constraint(String name, String message, Function<T, R> extractor, IConstraint<R> constraint)
        {
        	getCurrentGroup()
                    .constraint(name, message, extractor, constraint);
        	
        	return this;
        }
        
        ConstraintStatementGroup<T> getCurrentGroup()
        {
        	// Create default group if needed
        	if(currentGroup==null) {
        		group(Validation.EXECUTION_GROUP_DEFAULT, v -> true);
        	}
        	return currentGroup;
        }

        public BeanValidator<T> build()
        {
        	if(validationMode==null) {
        		validationMode = ValidationMode.FAIL_FAST;
        	}
        	
        	return new BeanValidator<T>(validationMode)
        			.setGroups(groups);
        }
    }
}