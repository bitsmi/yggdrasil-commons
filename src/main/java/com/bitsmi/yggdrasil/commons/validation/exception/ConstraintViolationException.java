package com.bitsmi.yggdrasil.commons.validation.exception;

import java.util.List;

import com.bitsmi.yggdrasil.commons.validation.ConstraintViolation;

public class ConstraintViolationException extends RuntimeException  
{
	private static final long serialVersionUID = -3002726008238865851L;

	private List<ConstraintViolation> violations;
	
	public ConstraintViolationException(List<ConstraintViolation> violations) 
	{
		super();
		this.violations = violations;
	}

	public ConstraintViolationException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace, List<ConstraintViolation> violations) 
	{
		super(message, cause, enableSuppression, writableStackTrace);
		this.violations = violations;
	}

	public ConstraintViolationException(String message, Throwable cause, List<ConstraintViolation> violations) 
	{
		super(message, cause);
		this.violations = violations;
	}

	public ConstraintViolationException(String message, List<ConstraintViolation> violations) 
	{
		super(message);
		this.violations = violations;
	}

	public ConstraintViolationException(Throwable cause, List<ConstraintViolation> violations) 
	{
		super(cause);
		this.violations = violations;
	}
	
	public List<ConstraintViolation> getViolations()
	{
		return this.violations;
	}
	
	@Override
	public String getMessage()
	{
		StringBuilder builder = new StringBuilder();
		String message = super.getMessage();
		if(message!=null) {
		    builder.append(super.getMessage()).append(":");
		}
		else {
		    builder.append("Error(s) found:");
		}
		boolean addSeparator = false;
		for(ConstraintViolation violation:violations) {
			if(addSeparator) {
				builder.append("\n");
			}
			else {
				addSeparator = true;
			}
			builder.append(violation.getMessage());
		}
		
		return builder.toString();
	}
}
