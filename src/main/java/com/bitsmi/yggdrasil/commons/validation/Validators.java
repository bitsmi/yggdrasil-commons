package com.bitsmi.yggdrasil.commons.validation;

import java.text.MessageFormat;
import java.util.Objects;
import java.util.function.Function;
import java.util.function.Predicate;

import com.bitsmi.yggdrasil.commons.validation.BeanValidator.BeanValidatorBuilder;
import com.bitsmi.yggdrasil.commons.validation.Constraints.IConstraint;

public class Validators
{
    /**
     * Fail fast bean validator 
     */
    public static <T> BeanValidatorBuilder<T> beanValidator()
    {
    	return BeanValidator.<T>builder()
    			.withValidationMode(ValidationMode.FAIL_FAST);
    }
    
    public static <T> BeanValidatorBuilder<T> beanValidator(ValidationMode validationMode)
    {
    	return BeanValidator.<T>builder()
    			.withValidationMode(validationMode);
    }

    /*-------------------------------*
     * Validation structures
     *-------------------------------*/
    public static enum ValidationMode 
    {
        ALL,
        FAIL_FAST;
    }
    
    public static class ConstraintStatementGroup<T>
    {
        ConstraintStatement<T, ?> statementsHead;
        String name;
        Predicate<T> predicate;
        
        ConstraintStatementGroup(String name, Predicate<T> predicate)
        {
            this.name = name;
            this.predicate = predicate;
        }
        
        public ConstraintStatementGroup<T> group(String name)
        {
        	return group(name, v -> true);
        }
        
        public ConstraintStatementGroup<T> group(String name, Predicate<T> predicate)
        {
        	return new ConstraintStatementGroup<>(name, predicate);
        }
        
        public <R> ConstraintStatementGroup<T> constraint(Function<T, R> extractor, IConstraint<R> constraint)
        {
            String name = new StringBuilder("Constraint '").append(constraint.toString()).append("'")                    
                    .toString();
            String message = new StringBuilder("Validation for [").append(name).append("] failed")
                    .toString();
            return constraint(name, message, extractor, constraint);
        }
        
        public <R> ConstraintStatementGroup<T> constraint(String name, String message, Function<T, R> extractor, IConstraint<R> constraint)
        {
            Objects.requireNonNull(name);
            Objects.requireNonNull(message);
            Objects.requireNonNull(extractor);
            Objects.requireNonNull(constraint);
            
            ConstraintStatement<T, R> statement = new ConstraintStatement<>(name,
                    message, 
                    extractor, 
                    constraint.getRootConstraint());
            if(statementsHead==null) {
                this.statementsHead = statement;
            }
            else {
                this.statementsHead.chain(statement);
            }
            
            return this;
        }
        
        public boolean test(Validation validation, T instance, ValidationMode validationMode)
        {
            validation.getExecution().selectGroup(name);
            boolean success = statementsHead.test(validation, instance, validationMode);
            if(success) {
                validation
                    .getExecution()
                    .selectedGroup()
                    .success();
            }
            else {
                validation
                    .getExecution()
                    .selectedGroup()
                    .fail();
            }
            
            return success;
        }
        
        public void skip(Validation validation)
        {
            validation.getExecution().selectGroup(name);
            statementsHead.skip(validation);
            validation
                .getExecution()
                .selectedGroup()
                .skip();
        }
    }
    
    static class ConstraintStatement<T, R>
    {
        String name;
        String message;
        Function<T, R> extractor;
        IConstraint<R> constraint;
        ConstraintStatement<T, ?> downstream;
        
        ConstraintStatement(String name, String message, Function<T, R> extractor, IConstraint<R> constraint)
        {
            this.name = name;
            this.message = message;
            this.extractor = extractor;
            this.constraint = constraint;
        }
        
        <X> void chain(ConstraintStatement<T, X> downstream)
        {
            if(this.downstream!=null) {
                this.downstream.chain(downstream);
            }
            else {
                this.downstream = downstream;
            }
        }
        
        String getInterpolatedMessage(Object value)
        {
            String interpolatedMessage = MessageFormat.format(message, value);
            return interpolatedMessage;
        }
        
        boolean test(Validation validation, T instance, ValidationMode validationMode)
        {
            R value = extractor.apply(instance);
            Validation.AbstractConstraintExecution constraintExecution = constraint.test(value);
            // Top level constraints must display value in toString
            constraintExecution.setValueVisibleInToString(true);
            
            validation.getExecution()
                    .addStatementExecution(Validation.executedStatement(name, value, constraintExecution));
            
            boolean success = constraintExecution.isSuccess();
            if(!success) {
                String interpolatedMessage = getInterpolatedMessage(value);
                validation.addConstraintViolation(new ConstraintViolation(name, interpolatedMessage));
            }
            
            if(downstream!=null && (success || validationMode==ValidationMode.ALL)) {
                boolean downstreamSuccess = downstream.test(validation, instance, validationMode);
                success = success && downstreamSuccess;
            }
            // Skipped
            else if(downstream!=null) {
                downstream.skip(validation);
            }
            
            return success;
        }
        
        public void skip(Validation validation)
        {
            validation.getExecution()
                    .addStatementExecution(Validation.skippedStatement(name));
            if(downstream!=null) {
                downstream.skip(validation);
            }
        }
    }
}
