package com.bitsmi.yggdrasil.commons.validation;

public class ConstraintViolation 
{
    private String constraintName;
	private String message;
	
	public ConstraintViolation(String constraintName, String message)
	{
	    this.constraintName = constraintName;
		this.message = message;
	}
	
	public String getConstraintName()
	{
	    return constraintName;
	}
	
	public String getMessage()
	{
		return message;
	}

    @Override
    public String toString()
    {
        StringBuilder builder = new StringBuilder();
        builder.append(constraintName)
                .append(" - ").append(message);

        return builder.toString();
    }
}
