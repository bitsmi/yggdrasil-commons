package com.bitsmi.yggdrasil.commons.validation;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.regex.Pattern;

public class Constraints 
{
	/*-------------------------------*
     * Help operations
     *-------------------------------*/
    /**
     * Allow use static import with type infer because call static methods without prepending class name is not allowed <br/>
     * E.G.<br/> <code>
     * validator.constraint(ValidatedBean::getName, &lt;String&gt;map(...)) // Compilation error
     * </code>
     */
    public static <V, X> IConstraint<V> map(Class<V> type, Function<V, X> mapper, IConstraint<X> constraint)
    {
        return map(mapper, constraint);
    }
    
    public static <V, X> IConstraint<V> map(Function<V, X> mapper, IConstraint<X> constraint)
    {
    	return new WrappedConstraint<>(mapper, constraint);
    }
    
    /*-------------------------------*
     * Object constraints
     *-------------------------------*/
    /**
     * Allow use static import with type infer because call static methods without prepending class name is not allowed <br/>
     * E.G.<br/> <code>
     * validator.constraint(ValidatedBean::getName, &lt;String&gt;isNotNull()) // Compilation error
     * </code>
     */
    public static <V> IConstraint<V> isNotNull(Class<V> type)
    {
        return isNotNull();
    }
    
    public static <V> IConstraint<V> isNotNull()
    {
        return new PlainConstraint<V>("Value is not null", 
            v -> v!=null);
    }
    
    /*-------------------------------*
     * String constraints
     *-------------------------------*/
    public static IConstraint<String> isEmail()
    {
        return new PlainConstraint<String>("Value is a valid email", 
                v -> Pattern.matches("^[a-zA-Z0-9_+&*-]+(?:\\.[a-zA-Z0-9_+&*-]+)*@(?:[a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,7}$", v));
    }
    
    /*-------------------------------*
     * Number constraints
     *-------------------------------*/
    public static IConstraint<Integer> isLessThan(int limit)
    {
        return new PlainConstraint<Integer>(String.format("Value is less than (%s)", limit), 
                v -> v < limit);
    }
    
    public static IConstraint<Integer> isGreaterThan(int limit)
    {
        return new PlainConstraint<Integer>(String.format("Value is greater than (%s)", limit), 
                v -> v > limit);
    }
    
    public static IConstraint<Double> isLessThan(double limit)
    {
        return new PlainConstraint<Double>(String.format("Value is less than (%s)", limit), 
                v -> v < limit);
    }
    
    public static IConstraint<Double> isGreaterThan(double limit)
    {
        return new PlainConstraint<Double>(String.format("Value is greater than (%s)", limit), 
            v -> v > limit);
    }
    
    /*-------------------------------*
     * Constraint structures
     *-------------------------------*/
    public static interface IConstraint<V>
    {
        public boolean isComposed();
        
        public IConstraint<V> and(IConstraint<V> other);
        public IConstraint<V> or(IConstraint<V> other);
        
        public default <X> IConstraint<V> and(Function<V, X> mapper, IConstraint<X> other)
        {
            WrappedConstraint<V, X> wrappedConstraint = new WrappedConstraint<>(mapper, other);
            return and(wrappedConstraint);
        }
        
        public default <X> IConstraint<V> or(Function<V, X> mapper, IConstraint<X> other)
        {
            WrappedConstraint<V, X> wrappedConstraint = new WrappedConstraint<>(mapper, other);
            return or(wrappedConstraint);
        }
        
        public ComposedConstraint<V> getParent();
        public void setParent(ComposedConstraint<V> parent);
        
        public Validation.AbstractConstraintExecution test(V value);
        public Validation.AbstractConstraintExecution skip();
        
        public default IConstraint<V> getRootConstraint()
        {
            IConstraint<V> c = this;
            
            if(c.getParent()==null) {
                return c;
            }
            
            do {
                c = c.getParent();
            }
            while(c.getParent()!=null);
            
            return c;
        }
    }
    
    static class PlainConstraint<V> implements IConstraint<V>
    {
        protected String name;
        protected Predicate<V> predicate;
        
        protected ComposedConstraint<V> parent;

        PlainConstraint(String name, Predicate<V> predicate)
        {
            Objects.requireNonNull(name);
            Objects.requireNonNull(predicate);
            
            this.name = name;
            this.predicate = predicate;
        }
        
        @Override
        public boolean isComposed()
        {
            return false;
        }
        
        @Override
        public IConstraint<V> and(IConstraint<V> other)
        {
            ComposedConstraint<V> composableConstraint = new ComposedConstraint<V>(Validation.ConstraintOperator.AND);
            composableConstraint.add(this);
            composableConstraint.add(other);
            
            return composableConstraint;
        }
        
        @Override
        public IConstraint<V> or(IConstraint<V> other)
        {
            ComposedConstraint<V> composableConstraint = new ComposedConstraint<V>(Validation.ConstraintOperator.OR);
            composableConstraint.add(this);
            composableConstraint.add(other);
            
            return composableConstraint;
        }
        
        @Override
        public ComposedConstraint<V> getParent()
        {
            return parent;
        }
        
        @Override
        public void setParent(ComposedConstraint<V> parent)
        {
            this.parent = parent;
        }
        
        @Override
        public Validation.AbstractConstraintExecution test(V value)
        {
            Validation.PlainConstraintExecution execution = Validation.executedPlainConstraint(this.toString(), value);
            boolean success = predicate.test(value);
            if(success) {
                execution.success();
            }
            else {
                execution.fail();
            }
            
            return execution;
        }
        
        @Override
        public Validation.AbstractConstraintExecution skip()
        {
            return Validation.skippedPlainConstraint(this.toString());
        }
        
        @Override
        public String toString()
        {
            StringBuilder builder = new StringBuilder()
                    .append("[").append(name).append("]");
            
            return builder.toString();
        }
    }
    
    static class WrappedConstraint<V, X> implements IConstraint<V>
    {
        protected Function<V, X> mapper;
        protected ComposedConstraint<V> parent;
        protected IConstraint<X> wrappedConstraint;

        WrappedConstraint(Function<V, X> mapper, IConstraint<X> wrappedConstraint)
        {
            Objects.requireNonNull(mapper);
            Objects.requireNonNull(wrappedConstraint);
            
            this.mapper = mapper;
            this.wrappedConstraint = wrappedConstraint;
        }
        
        @Override
        public boolean isComposed()
        {
            return wrappedConstraint.isComposed();
        }
        
        @Override
        public IConstraint<V> and(IConstraint<V> other)
        {
            ComposedConstraint<V> composableConstraint = new ComposedConstraint<V>(Validation.ConstraintOperator.AND);
            composableConstraint.add(this);
            composableConstraint.add(other);
            
            return composableConstraint;
        }
        
        @Override
        public IConstraint<V> or(IConstraint<V> other)
        {
            ComposedConstraint<V> composableConstraint = new ComposedConstraint<V>(Validation.ConstraintOperator.OR);
            composableConstraint.add(this);
            composableConstraint.add(other);
            
            return composableConstraint;
        }
        
        @Override
        public ComposedConstraint<V> getParent()
        {
            return parent;
        }
        
        @Override
        public void setParent(ComposedConstraint<V> parent)
        {
            this.parent = parent;
        }
        
        @Override
        public Validation.AbstractConstraintExecution test(V value)
        {
            X mappedValue = this.mapper.apply(value);
            Validation.AbstractConstraintExecution result = this.wrappedConstraint.test(mappedValue);
            result.setValueVisibleInToString(true);
            
            return result;
        }
        
        @Override
        public Validation.AbstractConstraintExecution skip()
        {
            return wrappedConstraint.skip();
        }
        
        @Override
        public String toString()
        {
            return wrappedConstraint.toString();
        }
    }
    
    static class ComposedConstraint<V> implements IConstraint<V>
    {
        protected List<IConstraint<V>> constraints = new ArrayList<>();
        protected Validation.ConstraintOperator operator;
        
        protected ComposedConstraint<V> parent;
        
        ComposedConstraint(Validation.ConstraintOperator operator)
        {
            Objects.requireNonNull(operator);
            this.operator = operator;
        }
        
        @Override
        public boolean isComposed()
        {
            return true;
        }
        
        @Override
        public IConstraint<V> and(IConstraint<V> other)
        {
            if(this.operator==Validation.ConstraintOperator.AND) {
                add(other);
                return this;
            }
            // OR
            else {
                IConstraint<V> last = constraints.remove(constraints.size()-1);
                ComposedConstraint<V> composableConstraint = new ComposedConstraint<>(Validation.ConstraintOperator.AND);
                add(composableConstraint);
                composableConstraint.add(last);
                composableConstraint.add(other);
                return composableConstraint;
            }
        }
        
        @Override
        public IConstraint<V> or(IConstraint<V> other)
        {
            if(this.operator==Validation.ConstraintOperator.OR) {
                add(other);
                return this;
            }
            // AND
            else {
                // Append to parent's constraint list
                if(this.parent!=null && this.parent.operator==Validation.ConstraintOperator.OR) {
                    this.parent.add(other);
                    return this.parent;
                }
                // Create OR parent
                else {
                    ComposedConstraint<V> composableConstraint = new ComposedConstraint<>(Validation.ConstraintOperator.OR);
                    if(this.parent!=null) {
                        this.parent.add(composableConstraint);
                        this.parent.remove(this);
                    }
                    composableConstraint.add(this);
                    composableConstraint.add(other);
                    return composableConstraint;
                }
            }
        }
        
        @Override
        public ComposedConstraint<V> getParent()
        {
            return parent;
        }
        
        @Override
        public void setParent(ComposedConstraint<V> parent)
        {
            this.parent = parent;
        }
        
        @Override
        public Validation.AbstractConstraintExecution test(V value)
        {
            Validation.ComposedConstraintExecution execution = Validation.executedComposedConstraint(operator, value);;
            
            // Identity value: AND -> true; OR -> false
            boolean success = this.operator.identityValue();
            boolean skip = false;
            for(int i=0; i<constraints.size(); i++) {
                IConstraint<V> c = constraints.get(i);
                Validation.AbstractConstraintExecution nestedExecution = null;
                if(!skip) {
                    nestedExecution = c.test(value);
                    
                    if(this.operator==Validation.ConstraintOperator.AND) {
                        success = success && nestedExecution.isSuccess();
                    }
                    // OR
                    else {
                        success = success || nestedExecution.isSuccess();
                    }
                    
                    skip = this.operator.isFailFast() && !success;
                }
                else {
                    nestedExecution = c.skip();
                }
                execution.addNestedExecution(nestedExecution);
            }
            
            if(success) {
                execution.success();
            }
            else {
                execution.fail();
            }
            
            return execution;
        }
        
        @Override
        public Validation.AbstractConstraintExecution skip()
        {
            Validation.ComposedConstraintExecution execution = Validation.skippedComposedConstraint(operator);
            
            for(int i=0; i<constraints.size(); i++) {
                IConstraint<V> c = constraints.get(i);
                Validation.AbstractConstraintExecution nestedExecution = c.skip();
                execution.addNestedExecution(nestedExecution);
            }
            
            return execution;
        }
        
        void add(IConstraint<V> other)
        {
            this.constraints.add(other);
            other.setParent(this);
        }
        
        void remove(IConstraint<V> c)
        {
            this.constraints.remove(c);
            // Detach
            c.setParent(null);
        }
        
        @Override
        public String toString()
        {
            StringBuilder builder = new StringBuilder();
            for(int i=0; i<constraints.size(); i++) {
                IConstraint<V> c = constraints.get(i);
                if(i>0) {
                    builder.append(" ").append(operator).append(" ");
                }
                builder.append("(").append(c.toString()).append(")");
            }
            
            return builder.toString();
        }
    }
    
    public static String explain(IConstraint<?> constraint)
    {
        if(constraint.isComposed()) {
            IConstraint<?> root = constraint.getRootConstraint();
            return root.toString();
        }
        else {
            return constraint.toString();
        }
    }
}
