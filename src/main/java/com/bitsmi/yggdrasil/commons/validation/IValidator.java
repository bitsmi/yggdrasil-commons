package com.bitsmi.yggdrasil.commons.validation;

public interface IValidator<T> 
{
	public Validation validate(T instance);	
}
