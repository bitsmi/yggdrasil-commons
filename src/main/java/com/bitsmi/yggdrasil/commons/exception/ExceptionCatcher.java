package com.bitsmi.yggdrasil.commons.exception;

import java.util.Optional;
import java.util.function.Consumer;
import java.util.function.Function;

import com.bitsmi.yggdrasil.commons.util.UncheckedFunctionals;
import com.bitsmi.yggdrasil.commons.util.UncheckedFunctionals.ThrowingCallable;
import com.bitsmi.yggdrasil.commons.util.UncheckedFunctionals.ThrowingSupplier;

/**
 * Exception catching utils.
 * @since 1.1.0
 */
public class ExceptionCatcher <T>
{
	private Exception exception;
	private T result;
	
	private ExceptionCatcher() { }
	
	/**
	 * Execute a block of code that may throw an exception. The exception thrown (if any) can be managed through {@link ExceptionCatcher}'s API methods. 
	 * @param callable {@link UncheckedFunctionals.ThrowingCallable} - Code to be executed
	 * @return {@link ExceptionCatcher} - Catcher instance, allowing method chaining
	 */
	public static ExceptionCatcher<Void> execute(UncheckedFunctionals.ThrowingCallable<Exception> callable)
	{
		ExceptionCatcher<Void> catcher = new ExceptionCatcher<>();
		try {
			callable.call();
		}
		catch(Exception e) {
			catcher.exception = e;
		}
		
		return catcher;
	}
	
	/**
	 * Execute a block of code that may throw an exception. The exception thrown (if any) can be managed through {@link ExceptionCatcher}'s API methods. 
	 * @param supplier {@link UncheckedFunctionals.ThrowingSupplier} - Code to be executed. May return a value. Use {@link ExceptionCatcher#getResult()} to obtain it.
	 * @return {@link ExceptionCatcher} - Catcher instance, allowing method chaining
	 * @since 1.3.0
	 */
	public static <U> ExceptionCatcher<U> execute(UncheckedFunctionals.ThrowingSupplier<U, Exception> supplier)
	{
		ExceptionCatcher<U> catcher = new ExceptionCatcher<>();
		try {
			catcher.result = supplier.get();
		}
		catch(Exception e) {
			catcher.exception = e;
		}
		
		return catcher;
	}
	
	/**
	 * Checks if an exception was thrown by the executed code block
	 * @return boolean - True if and exception was thrown. False otherwise
	 */
	public boolean hasException()
	{
		return exception!=null;
	}
	
	/**
	 * If executing a supplier, returns the resulting value
	 * @return {@link Optional}&lt;T<&gt; - Optional containing the resulting value. 
	 * 		If executing a callable it will result in an empty Optional 
	 * @since 1.3.0
	 */
	public Optional<T> getResult()
	{
		return Optional.ofNullable(result);
	}
	
	/*---------------------------*
	 * RETHROW EXCEPTIONS
	 *---------------------------*/
	/**
	 * If an exception is thrown by the executed code block, rethrow it. 
	 * @throws {@link Exception} - Exception thrown by the executed code block. It's rethrowed as a generic {@link Exception}.
	 * @return {@link ExceptionCatcher} - Catcher instance, allowing method chaining
	 */
	public ExceptionCatcher<T> ifExceptionRethrow() throws Exception
	{
		if(exception!=null) {
			throw exception;
		}
		
		return this;
	}
	
	/**
	 * If an exception is thrown by the executed code block, rethrow it. 
	 * Unlike the no parameters version of this method, a cast will be performed to the specified Exception.
	 * If catched exception's class is different from the one specified bu this method's parameter, and can't be cast to it, no exception will be rethrown. 
	 * @param {@link Class} - Class of the exception to be rethrown
	 * @throws {@link Exception} - Exception thrown by the executed code block. It's rethrowed as a generic {@link Exception}.
	 * @return {@link ExceptionCatcher} - Catcher instance, allowing method chaining
	 */
	public <E extends Exception> ExceptionCatcher<T> ifExceptionRethrow(Class<E> exceptionClass) throws E
	{
		if(exception!=null
				&& exceptionClass.isAssignableFrom(exception.getClass())) {
			throw exceptionClass.cast(exception);
		}
		
		return this;
	}
	
	/**
	 * If an exception is thrown by the executed code block, the specified map function will be executed and it's result will be rethrown. 
	 * This method serves as conversion mechanism between exception types.
	 * @param function - {@link Function} - Exception map function
	 * @throws E - Exception thrown by the executed code block. 
	 * @return {@link ExceptionCatcher} - Catcher instance, allowing method chaining
	 */
	public <E extends Exception> ExceptionCatcher<T> ifExceptionRethrow(Function<Exception, E> function) throws E
	{
		if(exception!=null) {
			throw function.apply(exception);
		}
		
		return this;
	}
	
	/**
	 * If an exception is thrown by the executed code block, convert to unchecked exception and rethrow it.
	 * Relies on generic's type erasure to convert checked exceptions into unchecked. The runtime type of the 
	 * thrown exception will be checked.
	 * @return {@link ExceptionCatcher} - Catcher instance, allowing method chaining 
	 */
    public ExceptionCatcher<T> ifExceptionRethrowAsUnchecked()
	{
	    if(exception!=null) {
	        ExceptionCatcher.<RuntimeException>rethrow(exception);
	    }	    
		
		return this;
	}
	
	/*---------------------------*
	 * EXCEPTION CONSUMERS
	 *---------------------------*/
	/**
	 * Perform an operation on catched exception (if any). This method will not rethrow the catched exception by is own.
	 * This must be done inside consumer's implementation. 
	 * @param consumer {@link Consumer} - Consumer instance that will be executed if an exception was catched
	 * @return {@link ExceptionCatcher} - Catcher instance, allowing method chaining
	 */
	public ExceptionCatcher<T> ifException(Consumer<Exception> consumer)
	{
		if(exception!=null) {
			consumer.accept(exception);
		}
		return this;
	}
	
	/**
	 * Perform an operation on catched exception (if any) if it's an instance of the specified exception class or one of it's subclasses. 
	 * This method will not rethrow the catched exception by is own. This must be done inside consumer's implementation. 
	 * @param exceptionClass {@link Class} - Exception class to filter with
	 * @param consumer {@link Consumer} - Consumer instance that will be executed if an exception was catched
	 * @return {@link ExceptionCatcher} - Catcher instance, allowing method chaining
	 */
	public <E extends Exception> ExceptionCatcher<T> ifException(Class<E> exceptionClass, Consumer<Exception> consumer)
	{
		if(exception!=null
				&& exceptionClass.isAssignableFrom(exception.getClass())) {
			ifException(consumer);
		}
		
		return this;
	}
	
	/**
	 * Util method that allows converting a checked exception into unchecked. 
	 * It relies on java's generics type erasure in order to achieve that.
	 * @param throwable
	 * @throws E
	 */
	@SuppressWarnings("unchecked")
    static <E extends Throwable> void rethrow(Throwable throwable) throws E
    {
        throw (E)throwable;
    }
}
