package com.bitsmi.yggdrasil.commons.exception;

public class CodedMessage 
{
	private String code;
	private String message;
	
	public String getCode() 
	{
		return code;
	}
	
	public CodedMessage setCode(String code) 
	{
		this.code = code;
		return this;
	}
	
	public String getMessage() 
	{
		return message;
	}
	
	public CodedMessage setMessage(String message)
	{
		this.message = message;
		return this;
	}
	
	public CodedMessage setMessage(String message, Object... args)
	{
		String formattedMessage = String.format(message, args);
		setMessage(formattedMessage);
		
		return this;
	}

	public static CodedMessage of(String code, String message)
	{
	    return new CodedMessage()
            .setCode(code)
            .setMessage(message);
	}
	
	public static CodedMessage of(String code, String message, Object... args)
	{
	    return new CodedMessage()
	        .setCode(code)
	        .setMessage(message, args);
	}
	
	@Override
	public int hashCode() 
	{
		final int prime = 31;
		int result = 1;
		result = prime * result + ((code == null) ? 0 : code.hashCode());
		result = prime * result + ((message == null) ? 0 : message.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) 
	{
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		CodedMessage other = (CodedMessage) obj;
		if (code == null) {
			if (other.code != null) {
				return false;
			}
		} else if (!code.equals(other.code)) {
			return false;
		}
		if (message == null) {
			if (other.message != null) {
				return false;
			}
		} else if (!message.equals(other.message)) {
			return false;
		}
		return true;
	}
}
