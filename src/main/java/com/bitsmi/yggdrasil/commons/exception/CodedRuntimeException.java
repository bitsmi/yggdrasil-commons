package com.bitsmi.yggdrasil.commons.exception;

public class CodedRuntimeException extends RuntimeException
{
	private static final long serialVersionUID = -5400490560651383673L;
	
	protected String errorCode;
	
	public CodedRuntimeException(String errorCode) 
	{
		super();
		this.errorCode = errorCode;
	}
	
	public CodedRuntimeException(String errorCode, Throwable cause) 
	{
		super(cause);
		this.errorCode = errorCode;
	}

	public CodedRuntimeException(CodedMessage error) 
	{
		super(error.getMessage());
		this.errorCode = error.getCode();
	}
	

	public CodedRuntimeException(CodedMessage error, Throwable cause) 
	{
		super(error.getMessage(), cause);
		this.errorCode = error.getCode();
	}

	public CodedRuntimeException(CodedMessage error, Throwable cause, boolean enableSuppression, boolean writableStackTrace) 
	{
		super(error.getMessage(), cause, enableSuppression, writableStackTrace);
		this.errorCode = error.getCode();
	}

	public String getErrorCode() 
	{
		return errorCode;
	}

	public void setErrorCode(String errorCode) 
	{
		this.errorCode = errorCode;
	}
}
