package com.bitsmi.yggdrasil.commons.exception;

public class CodedException extends Exception
{
	private static final long serialVersionUID = 8104300710094371253L;
	
	protected String errorCode;
	
	public CodedException(String errorCode) 
	{
		super();
		this.errorCode = errorCode;
	}
	
	public CodedException(String errorCode, Throwable cause) 
	{
		super(cause);
		this.errorCode = errorCode;
	}

	public CodedException(CodedMessage error) 
	{
		super(error.getMessage());
		this.errorCode = error.getCode();
	}
	

	public CodedException(CodedMessage error, Throwable cause) 
	{
		super(error.getMessage(), cause);
		this.errorCode = error.getCode();
	}

	public CodedException(CodedMessage error, Throwable cause, boolean enableSuppression, boolean writableStackTrace) 
	{
		super(error.getMessage(), cause, enableSuppression, writableStackTrace);
		this.errorCode = error.getCode();
	}

	public String getErrorCode() 
	{
		return errorCode;
	}

	public void setErrorCode(String errorCode) 
	{
		this.errorCode = errorCode;
	}
}
