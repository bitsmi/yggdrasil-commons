package com.bitsmi.yggdrasil.commons.exception;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Checked exception class that wraps several coded errors.
 * It's purpose is to be used as a {@link CodedException} alternative when there are multiple issues to inform.
 * @see CodedMessage 
 * @since 1.2.0
 */
public class CompositeCodedException extends Exception 
{
	private static final long serialVersionUID = 8462931211794757174L;
	
	private List<CodedMessage> errors;
	
	public List<CodedMessage> getErrors()
	{
		if(errors==null){
			return Collections.emptyList();
		}
		
		return errors;
	}
	
	public CompositeCodedException setErrors(List<CodedMessage> errors)
	{
		this.errors = errors;
		return this;
	}
	
	public CompositeCodedException addError(CodedMessage error)
	{
		if(this.errors==null){
			this.errors = new ArrayList<>();
		}
		
		this.errors.add(error);
		return this;
	}
	
	public CompositeCodedException addErrors(List<CodedMessage> errors)
	{
		if(this.errors==null){
			this.errors = new ArrayList<>();
		}
		
		this.errors.addAll(errors);
		return this;
	}
	
	@Override
	public String getMessage()
	{
		StringBuilder messageBuilder = new StringBuilder();
		for(CodedMessage error:getErrors()){
			if(messageBuilder.length()>0){
				messageBuilder.append(", ");
			}
			messageBuilder.append(error.getMessage());
		}
		
		return messageBuilder.toString();
	}
}
